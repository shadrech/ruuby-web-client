const moment = require("moment");

module.exports = [
  {
    path: "/",
    priority: "0.9",
  },
  {
    path: "category",
    priority: "0.6",
    changeFreq: "monthly"
  },
  {
    path: "search",
    priority: "0.8",
  },
  {
    path: "massage",
    priority: "0.8"
  },
  {
    path: "eyes",
    priority: "0.8"
  },
  {
    path: "nails",
    priority: "0.8"
  },
  {
    path: "wellness",
    priority: "0.8"
  },
  {
    path: "waxing",
    priority: "0.8"
  },
  {
    path: "cosmetic-injectables",
    priority: "0.8"
  },
  {
    path: "ruuby-men",
    priority: "0.8"
  },
  {
    path: "tanning",
    priority: "0.8"
  },
  {
    path: "facial",
    priority: "0.8"
  },
  {
    path: "makeup",
    priority: "0.8"
  },
  {
    path: "hair",
    priority: "0.8"
  },
  {
    path: "about",
    priority: "0.7",
    changeFreq: "monthly"
  },
  {
    path: "price-list",
    priority: "0.7",
    changeFreq: "monthly"
  },
  {
    path: "magazine",
    priority: "0.7"
  },
  {
    path: "join",
    priority: "0.8"
  },
  {
    path: "login",
    priority: "0.7",
    changeFreq: "monthly"
  },
  {
    path: "register",
    priority: "0.7",
    changeFreq: "monthly"
  },
  {
    path: "provider",
    priority: "0.7",
    changeFreq: "monthly"
  }
]