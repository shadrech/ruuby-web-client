import { resolve } from "path";
import * as webpack from "webpack";
import SitemapPlugin from "sitemap-webpack-plugin";

const p = require("../package.json");
const sitemapPaths = require("./sitemap.js");

const SriPlugin = require("webpack-subresource-integrity");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const config: webpack.Configuration = {
  context: resolve(__dirname, ".."),
  entry: {
    app: [ resolve(__dirname, "../src/index.tsx") ],
    vendor: [
      "react",
      "react-dom",
      "axios",
      "redux",
      "react-redux",
      "redux-thunk",
      "react-router",
      "gsap",
      "redux-form",
      "mobile-detect"
    ]
  },
  output: {
    crossOriginLoading: "anonymous",
    path: resolve(__dirname, "../build/"),
    filename: "[name]-[chunkhash].js",
    sourceMapFilename: "[name]-[chunkhash].js.map",
    publicPath: "/",
  },
  resolve: {
    extensions: ["*", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx", ".json"]
  },
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loaders: ["react-hot-loader/webpack", "ts-loader"]
      },
      {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.css$/,
        loaders: ["style-loader", "css-loader"]
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png$|\.jpg$|\.gif$/,
        loader: "file-loader"
      },
      {
        test: /\.js$/,
        loader: "imports-loader?define=>false"
      }
    ]
  },
  devtool: "source-map",
  plugins: [
    new webpack.DefinePlugin({
      DEVELOPMENT: JSON.stringify(false),
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      }
    }),
    new webpack.optimize.UglifyJsPlugin({ comments: false, sourceMap: true }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "vendor-[hash].js",
      chunks: ["vendor", "app"]
    }),
    new HtmlWebpackPlugin({
      filename: "index.html",
      hash: true,
      template: resolve(__dirname, "../src", "index-prod.tpl.html"),
      version: p.version
    }),
    new SriPlugin({
      hashFuncNames: ["sha256", "sha384"],
    }),
    new SitemapPlugin("https://www.ruuby.com", sitemapPaths, {
      lastMod: true,
      changeFreq: "monthly"
    })
  ]
};

export default config;
