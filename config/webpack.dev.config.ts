import { resolve, join } from "path";
import * as webpack from "webpack";

const HtmlWebpackPlugin = require("html-webpack-plugin");

const config: webpack.Configuration = {
  context: resolve(__dirname, ".."),
  entry: [
    "babel-plugin-transform-async-to-generator",
    resolve(__dirname, "../src/index.tsx")
  ],
  output: {
    path: resolve(__dirname, "../public"),
    publicPath: "/",
    filename: "bundle.js"
  },
  devtool: "cheap-module-eval-source-map",
  resolve: {
    extensions: ["*", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx", ".json"]
  },
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        loaders: ["react-hot-loader/webpack", "ts-loader"],
        exclude: resolve(__dirname, "../node_modules")
      },
      {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.css$/,
        loaders: ["style-loader", "css-loader"]
      },
      {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png$|\.jpg$|\.gif$/,
        loader: "file-loader"
      },
      {
        test: /\.js$/,
        loader: "imports-loader?define=>false"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      DEVELOPMENT: JSON.stringify(true)
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: "index.html",
      hash: true,
      template: resolve(__dirname, "../src", "index-dev.tpl.html")
    }),
  ],
  devServer: {
    contentBase: join(__dirname, "../public"),
    port: 3000,
    historyApiFallback: true,
    hot: true,
    host: "0.0.0.0",
    disableHostCheck: true
  }
};

export default config;
