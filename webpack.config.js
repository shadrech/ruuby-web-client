require("ts-node").register({
  project: "./tsconfig.json"
});

module.exports = (process.env.NODE_ENV === "production") ? require("./config/webpack.prod.config").default : require("./config/webpack.dev.config");
