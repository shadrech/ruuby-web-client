#!/bin/bash

set -e

rm -rf node_modules
yarn

cp config.original.json config.json
yarn build
