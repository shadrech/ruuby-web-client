#!/bin/bash

set -e

export AWS_ACCESS_KEY_ID=${STAGING_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${STAGING_AWS_SECRET_ACCESS_KEY}
DISTRIBUTION_ID=EHDWO6RR4D648
BUCKET=ruuby-main-website-staging
CONFIG_BUCKET=ruuby-test-configuration
TAG=$(git tag -l --contains HEAD)
CACHE_AGE=3600
PACKAGE_NAME=$(node -e 'const f = fs.readFileSync("package.json"); const j = JSON.parse(f); console.log(j.name)')

rm -rf node_modules
yarn

# check if staging job or production and tagged
if [ $TAG ] && [ "${BRANCH_NAME}" == "master" ]; then
  aws s3 cp s3://${CONFIG_BUCKET}/ruuby-website/config.json ./

  yarn build

  # upload
  aws s3 sync build/ s3://${BUCKET}/

  # set cache control header on index.html
  aws s3api copy-object --bucket ${BUCKET} \
    --cache-control "max-age=${CACHE_AGE}" \
    --content-type "text/html" \
    --copy-source ${BUCKET}/index.html --key index.html \
    --metadata-directive "REPLACE"

  # create cache invalidation
  aws configure set preview.cloudfront true

  aws cloudfront create-invalidation --distribution-id ${DISTRIBUTION_ID} \
    --paths / /index.html
else
  echo "You can only deploy a tagged commit from master"
  exit 1
fi
