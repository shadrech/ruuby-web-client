import * as moment from "moment";

interface Config {
  gaId: string;
  version: string;
  api: {
    host: string;
    credentials: {
      id: string;
      secret: string;
    };
  };
  appApi: {
    host: string;
    credentials: {
      deviceId: string;
    }
  };
  magazineApi: {
    path: string;
  };
  timezone: string;
  jwtExpiration: Date;
}

const config: Config = require("../config.json");
const packageInfo = require("../package.json");

config.jwtExpiration = moment().add(12, "h").toDate();
config.version = packageInfo.version;

export {config};
