import * as Immutable from "immutable";
import {ActionType} from "../providers/reducer";
import * as types from "./types";
import {ApiCard} from "../../api/card";
import {FETCH_CUSTOMER_PROFILE_SUCCESS} from "../customer/types";

export interface State {
  isBusy: boolean;
  paymentGateway: {
    token: string;
  };
  cards: ApiCard[];
  newCardOpen: boolean;
  error: any;
}

const initialState = Immutable.fromJS({
  isBusy: false,
  paymentGateway: {},
  cards: [],
  newCardOpen: false,
  error: null
});

export default function(state: Immutable.Map<any, any> = initialState, action: ActionType) {
  switch (action.type) {
    case types.CREATE_CARD_REQUEST:
      return state.set("isBusy", true);

    case types.SET_PAYMENT_ERROR:
      return state.set("error", action.payload.error)
        .set("isBusy", false);

    case types.GET_BRAINTREE_TOKEN_SUCCESS:
      return state.set("paymentGateway", {token: action.payload.token});

    case types.FETCH_CARDS_SUCCESS:
      return state.set("cards", action.payload.cards)
        .set("isBusy", false);

    case types.NEW_CARD_OPEN:
      return state.set("newCardOpen", true);

    case types.NEW_CARD_CLOSE:
      return state.set("newCardOpen", false);

    case FETCH_CUSTOMER_PROFILE_SUCCESS:
      return state.set("cards", Immutable.fromJS(action.payload.cards));

    default:
      return state;
  }
}