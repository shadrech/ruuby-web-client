import {Dispatch} from "redux";
import * as api from "../../api/card";
import * as types from "./types";
// import {setCustomerError} from "../../reducers/customer/actions";

export function setPaymentBusyness() {
  return {
    type: types.CREATE_CARD_REQUEST
  };
}

export function setPaymentError(error: string) {
  return {
    type: types.SET_PAYMENT_ERROR,
    payload: { error }
  };
}

export function getBraintreeTokenSuccess(token: string) {
  return {
    type: types.GET_BRAINTREE_TOKEN_SUCCESS,
    payload: { token }
  };
}

export function fetchBraintreeeToken() {
  return async (dispatch: Dispatch<any>) => {
    try {
      const braintreeData = await api.getBraintreeToken();
      dispatch(getBraintreeTokenSuccess(braintreeData.token));
    } catch (error) {
      console.error("FETCH BRAINTREE CLIENT TOKEN ERROR", error);

      const mssg = JSON.parse(error.request.response).error.message;
      dispatch(setPaymentError(mssg));
    }
  };
}

export function fetchCardsSuccess(cards: api.ApiCard[]) {
  return {
    type: types.FETCH_CARDS_SUCCESS,
    payload: { cards }
  };
}

export function createCard(nonce: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(setPaymentBusyness());

    try {
      let data = await api.createCard(nonce);
      if (data.isInserted)
        data = await api.fetchCards();

      dispatch(fetchCardsSuccess(data));
      dispatch(closeNewCardForm());
    } catch (error) {
      console.error("CREATE CARD ERROR", error.response);
      const mssg = error.response.data.error.message;
      dispatch(setPaymentError(mssg));
    }
  };
}

export function openNewCardForm() {
  return {
    type: types.NEW_CARD_OPEN
  };
}

export function closeNewCardForm() {
  return {
    type: types.NEW_CARD_CLOSE
  };
}
