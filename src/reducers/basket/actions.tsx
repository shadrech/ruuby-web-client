import * as types from "./types";
import {ApiTherapistService} from "../../api/provider";

export function addTreatmentToBasket(id: string, treatment: ApiTherapistService) {
  return {
    type: types.ADD_TREATMENT_TO_BASKET,
    payload: { id, treatment }
  };
}

export function removeTreatmentFromBasket(id: string, treatmentIndex: any) {
  return {
    type: types.REMOVE_TREATMENT_FROM_BASKET,
    payload: { id, treatmentIndex }
  };
}

export function clearBasket(id: string) {
  return {
    type: types.CLEAR_BASKET,
    payload: { id }
  };
}

export function clearWholeBasket() {
  return {
    type: types.CLEAR_WHOLE_BASKET
  };
}
