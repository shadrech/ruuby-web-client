import * as Immutable from "immutable";
import * as types from "./types";
import {ActionType} from "../providers/reducer";
import {ApiTherapistService} from "../../api/provider";
import * as Cookie from "js-cookie";
import * as moment from "moment";

export interface State {
  items: {
    [urn: string]: ApiTherapistService[];
  };
}

const defaultBasket = Immutable.fromJS({
  items: Immutable.fromJS({})
});

export default function(state: any = defaultBasket, action: ActionType) {
  switch (action.type) {
    case types.ADD_TREATMENT_TO_BASKET:
      const exists = state.getIn(["items", action.payload.id]);
      let newState: Immutable.Map<any, any>;
      if (!exists) {
        const initialBasket = state.get("items").setIn([action.payload.id], Immutable.fromJS([action.payload.treatment]));
        newState = state.set("items", initialBasket);
      } else {
        newState = state.updateIn(["items", action.payload.id], items => items.push(action.payload.treatment));
      }
      Cookie.set(types.BASKET, newState.get("items").toJS(), {expires: moment().add(1, "day").toDate(), path: "/"});
      return newState;

    case types.REMOVE_TREATMENT_FROM_BASKET:
      newState = state.updateIn(["items", action.payload.id], (basket: any) => basket.delete(action.payload.treatmentIndex));
      Cookie.set(types.BASKET, newState.get("items").toJS(), {path: "/"});
      return newState;

    case types.CLEAR_BASKET:
      newState = state.set("items", Immutable.fromJS({}));
      Cookie.set(types.BASKET, newState.get("items").toJS(), {path: "/"});
      return newState;

    case types.CLEAR_WHOLE_BASKET:
      return defaultBasket;

    default:
      return state;
  }
}
