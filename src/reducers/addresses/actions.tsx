import {Dispatch} from "redux";
import * as types from "./types";
import * as api from "../../api/address";

function createAddressAttempt() {
  return {
    type: types.CREATE_ADDRESS_ATTEMPT
  };
}

export function openNewAddressForm() {
  return {
    type: types.NEW_ADDRESS_OPEN
  };
}

export function closeNewAddressForm() {
  return {
    type: types.NEW_ADDRESS_CLOSE
  };
}

function setAddressError(error: string) {
  return {
    type: types.NEW_ADDRESS_ERROR,
    payload: { error }
  };
}

function createAddressSuccess(address: api.ApiAddress) {
  return {
    type: types.CREATE_ADDRESS_SUCCESS,
    payload: { address }
  };
}

export function createAddress(data: any) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(createAddressAttempt());

    try {
      if (!data.address2) data.address2 = "";
      const newAddress = await api.createAddress(data);
      dispatch(createAddressSuccess(newAddress));
    } catch (err) {
      console.error("ERROR CREATING ADDRESS", err.response.data.error.message);
      dispatch(setAddressError(err.response.data.error.message));
    }
  };
}
