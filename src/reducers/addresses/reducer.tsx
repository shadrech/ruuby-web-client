import * as Immutable from "immutable";
import * as types from "./types";
import {
  ApiAddress
} from "../../api/address";
import {ActionType} from "../providers/reducer";
import {FETCH_CUSTOMER_PROFILE_SUCCESS} from "../customer/types";

export interface State {
  isBusy: boolean;
  addresses: ApiAddress[];
  newAddressOpen: boolean;
  error: any;
}

const initialState = Immutable.fromJS({
  isBusy: false,
  newAddressOpen: false,
  addresses: [],
  error: null
});

export default function(state: Immutable.Map<any, any> = initialState, action: ActionType) {
  switch (action.type) {
    case types.CREATE_ADDRESS_ATTEMPT:
      return state.set("isBusy", true);

    case types.NEW_ADDRESS_ERROR:
      return state.set("error", action.payload.error)
        .set("isBusy", false);

    case types.NEW_ADDRESS_OPEN:
      return state.set("newAddressOpen", true);

    case types.NEW_ADDRESS_CLOSE:
      return state.set("newAddressOpen", false);

    case types.CREATE_ADDRESS_SUCCESS:
      return state.update("addresses", addresses => addresses.push(action.payload.address))
        .set("newAddressOpen", false)
        .set("isBusy", false)
        .set("error", null);

    case FETCH_CUSTOMER_PROFILE_SUCCESS:
      return state.set("addresses", Immutable.fromJS(action.payload.addresses))
        .set("error", null);

    default:
      return state;
  }
}
