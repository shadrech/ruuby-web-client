import * as Immutable from "immutable";
import * as types from "./types";
import {
  ApiProvider,
  ApiAvailability
} from "../../api/provider";

export interface State {
  isBusy: boolean;
  isAvailabilityBusy: boolean;
  items: ApiProvider[];
  availability: ApiAvailability[];
  errors: any;
}

const defaultProviders = Immutable.fromJS({
  isBusy: false,
  isAvailabilityBusy: false,
  items: Immutable.fromJS([]),
  errors: Immutable.fromJS([]),
  availability: Immutable.fromJS([])
});

export interface ActionType {
  type: string;
  payload?: any;
};

export default function(state: Immutable.Map<any, any> = defaultProviders, action: ActionType) {
  switch (action.type) {
    case types.FETCH_PROVIDERS_REQUEST:
      return state.set("items", Immutable.fromJS([]))
        .set("errors", Immutable.fromJS([]))
        .set("isBusy", true);

    case types.FETCH_PROVIDERS_SUCCESS:
      return state.set("errors", Immutable.fromJS([]))
        .set("items", Immutable.fromJS(action.payload.providers))
        .set("isBusy", false);

    case types.FETCH_PROVIDERS_FAILURE:
      return state.set("errors", Immutable.fromJS(action.payload.errors))
        .set("isBusy", false);

    case types.FETCH_AVAILABILITY_ATTEMPT:
      return state.set("isAvailabilityBusy", true);

    case types.SET_AVAILABILITY:
      return state.set("availability", Immutable.fromJS(action.payload.availability))
        .set("isAvailabilityBusy", false);

    case types.CLEAR_AVAILABILITY:
      return state.set("availability", Immutable.fromJS([]));

    case types.PROVIDER_IS_BUSY:
      return state.set("isBusy", true);

    case types.PROVIDER_NOT_BUSY:
      return state.set("isBusy", false);

    default:
      return state;
  }
}
