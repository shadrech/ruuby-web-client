import {Dispatch} from "redux";
import * as types from "./types";
import * as api from "../../api/provider";
import {ApiTherapistService} from "../../api/provider";
import {actionCreators} from "../../reducers/booking/actions";

export function setBusyness() {
  return {
    type: types.PROVIDER_IS_BUSY
  };
}

export function setUnbusyness() {
  return {
    type: types.PROVIDER_NOT_BUSY
  };
}

function fetchProvidersSuccess(providers: api.ApiProvider[]) {
  return {
    type: types.FETCH_PROVIDERS_SUCCESS,
    payload: { providers }
  };
}

function fetchProvidersFailure(errors: any) {
  return {
    type: types.FETCH_PROVIDERS_FAILURE,
    payload: { errors }
  };
}

export function fetchProviders(category: string, slot: string, date: string, postcode: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch({type: types.FETCH_PROVIDERS_REQUEST});

    try {
      const providers = await api.fetchProviders(category, slot, date, postcode);
      dispatch(fetchProvidersSuccess(providers));
      dispatch(actionCreators.setSelectedPostcode(postcode));
    } catch (err) {
      console.error("FETCH PROVIDERS ERROR", err);
      dispatch(fetchProvidersFailure([err.message]));
    }
  };
}

export function fetchProvider(providerUrn: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch({type: types.FETCH_PROVIDERS_REQUEST});

    try {
      const providers = await api.fetchProvider(providerUrn);
      dispatch(fetchProvidersSuccess([providers]));
    } catch (err) {
      console.log("FETCH PROVICERS ERROR", err);
      dispatch(fetchProvidersFailure([err.message]));
    }
  };
}

export function clearAvailability() {
  return {
    type: types.CLEAR_AVAILABILITY
  };
}

export function setAvailability(availability: any) {
  return {
    type: types.SET_AVAILABILITY,
    payload: {availability}
  };
}

export function fetchAvailability(providerUrn: string) {
  return async (dispatch: Dispatch<any>, getState: any) => {
    dispatch(clearAvailability());
    dispatch({type: types.FETCH_AVAILABILITY_ATTEMPT});

    const serviceUrns = getState().basketState.getIn(["items", providerUrn]).toJS().map((service: ApiTherapistService) => service.urn);

    try {
      const availability = await api.fetchAvailability(providerUrn, serviceUrns);
      dispatch(setAvailability(availability));
    } catch (error) {
      console.error("FETCH AVAILABILITY ERROR", error);
    }
  };
}
