import * as moment from "moment";
import {browserHistory} from "react-router";
import * as types from "./types";
import {Dispatch} from "redux";
import * as api from "../../api/booking";
import {ApiAddress} from "../../api/address";
import {ApiCard} from "../../api/card";
import {ApiPromo} from "../../api/booking";
import {ApiBooking} from "../../api/booking";
import { slotTypes } from "./reducer";

export const actionCreators = {
  setBookingError: (error: string) => ({
    type: types.SET_BOOKING_ERROR,
    payload: { error }
  }),
  clearBooking: () => ({
    type: types.CLEAR_BOOKING
  }),
  /* -------------- DATE STUFF -------------- */
  setSelectedDate: (date: moment.Moment) => ({
    type: types.SET_SELECTED_DATE,
    payload: { date }
  }),
  clearSelectedDate: () => ({
    type: types.CLEAR_SELECTED_DATE
  }),
  setSelectedSlot: (slot: slotTypes) => ({
    type: types.SET_SELECTED_SLOT,
    payload: { slot }
  }),
  clearSelectedSlot: () => ({
    type: types.CLEAR_SELECTED_SLOT
  }),
  setSelectedTime: (time: moment.Moment) => ({
    type: types.SET_SELECTED_TIME,
    payload: { time }
  }),
  clearSelectedTime: () => ({
    type: types.CLEAR_SELECTED_TIME
  }),
  /* -------------- ADDRESS STUFF -------------- */
  selectAddress: (address: ApiAddress) => ({
    type: types.SELECT_ADDRESS,
    payload: { address }
  }),
  removeSelectedAddress: () => ({
    type: types.REMOVE_SELECTED_ADDRESS
  }),
  setSelectedPostcode: (postcode: string) => ({
    type: types.SELECT_POSTCODE,
    payload: { postcode }
  }),
  clearSelectedPostcode: () => ({
    type: types.CLEAR_SELECTED_POSTCODE
  }),
  /* -------------- PROMO STUFF -------------- */
  verifyPromoSuccess: (promo: ApiPromo) => ({
    type: types.VERIFY_PROMO_SUCCESS,
    payload: { promo }
  }),
  setBookingPromoError: (error: string) => ({
    type: types.SET_BOOKING_PROMO_ERROR,
    payload: { error }
  }),
  setPromoCode: (code: string) => ({
    type: types.SET_PROMO_CODE,
    payload: { code }
  }),
  clearPromoCode: () => ({
    type: types.CLEAR_PROMO_CODE
  }),
  verifyPromoRequest: () => ({
    type: types.VERIFY_PROMO_REQUEST
  }),
  /* -------------- CARD STUFF -------------- */
  selectCard: (card: ApiCard) => ({
    type: types.SELECT_CARD,
    payload: { card }
  }),
  removeSelectedCard: () => ({
    type: types.REMOVE_SELECTED_CARD
  }),
  /* -------------- NOTES STUFF -------------- */
  setSelectedNotes: (notes: string) => ({
    type: types.SET_SELECTED_NOTES,
    payload: { notes }
  }),
  /* -------------- FINALISING BOOKING STUFF -------------- */
  createBookingAttempt: () => ({
    type: types.CREATE_BOOKING_REQUEST
  }),
  createBookingSuccess: (booking: api.ApiBooking) => ({
    type: types.CREATE_BOOKING_SUCCESS,
    payload: { booking }
  }),
  /* -------------- FINALISING BOOKING STUFF -------------- */
  setSelectedCategory: (category: string) => ({
    type: types.SET_SELECTED_CATEGORY,
    payload: { category }
  }),
};

export function verifyPromo(code: string, urn: string) {
  return async (dispatch: Dispatch<any>, getState: any) => {
    dispatch(actionCreators.verifyPromoRequest());

    const serviceUrns = getState().basketState.getIn(["items", urn]).toJS().map(treatment => treatment.urn);

    try {
      const promo = await api.verifyPromo(code, serviceUrns);
      promo.code = code;
      return dispatch(actionCreators.verifyPromoSuccess(promo));
    } catch (err) {
      console.error("VERIFY PROMO ERROR", err);
      dispatch(actionCreators.setBookingPromoError(err.response.data.error.message));
    }
  };
}

export function createBooking(urn: string) {
  return async (dispatch: Dispatch<any>, getState: any) => {
    dispatch(actionCreators.createBookingAttempt());

    try {
      const booking: ApiBooking = await api.createBooking(getState(), urn);
      dispatch(actionCreators.createBookingSuccess(booking));

      browserHistory.push("/booking-confirmation");
    } catch (err) {
      console.error("CREATE BOOKING ERROR", err);
      dispatch(actionCreators.setBookingError(err.response.data.error.message));
    }
  };
}
