import * as Immutable from "seamless-immutable";
import { returntypeof } from "react-redux-typescript";
import {ApiAddress} from "../../api/address";
import {ApiCard} from "../../api/card";
import * as types from "./types";
import {ApiPromo, ApiBooking} from "../../api/booking";
import { actionCreators } from "./actions";

export type SLOT_MORNING = "Morning";
export const SLOT_MORNING: SLOT_MORNING = "Morning";
export type SLOT_AFTERNOON = "Afternoon";
export const SLOT_AFTERNOON: SLOT_AFTERNOON = "Afternoon";
export type SLOT_EVENING = "Evening";
export const SLOT_EVENING: SLOT_EVENING = "Evening";
export type SLOT_ALL_DAY = "All-Day";
export const SLOT_ALL_DAY: SLOT_ALL_DAY = "All-Day";
export type slotTypes = SLOT_MORNING | SLOT_AFTERNOON | SLOT_EVENING | SLOT_ALL_DAY | "";

export const actions = Object.values(actionCreators).map(returntypeof);
export type Action = typeof actions[number];

export interface State {
  isBusy: boolean;
  promoIsBusy: boolean;
  booking: ApiBooking;
  selectedCategory: string;
  selectedDate: string;
  selectedTime: string;
  selectedAddress: ApiAddress;
  selectedPostcode: string;
  selectedCard: ApiCard;
  selectedPromo: ApiPromo;
  selectedNotes: string;
  selectedSlot: slotTypes;
  error: any;
  promoError: any;
}

const _defaultPromo: ApiPromo = {code: "", isValid: false, totalAmount: null, totalDuration: null, treatmentPrices: {}};
const defaultPromo = Immutable.from(_defaultPromo);

const initialState = Immutable.from({
  isBusy: false,
  promoIsBusy: false,
  booking: null,
  selectedCategory: null,
  selectedPostcode: null,
  selectedAddress: null,
  selectedDate: null,
  selectedTime: null,
  selectedCard: null,
  selectedPromo: defaultPromo,
  selectedNotes: "",
  selectedSlot: null,
  error: null,
  promoError: null
});

export default function(state: Immutable.ImmutableObject<State> = initialState, action: Action) {
  switch (action.type) {
    case types.CLEAR_BOOKING:
      return initialState;

    case types.CREATE_BOOKING_REQUEST:
      return state.set("isBusy", true);

    case types.SET_BOOKING_ERROR:
      return state.set("isBusy", false)
        .set("error", action.payload.error);

    case types.SET_BOOKING_PROMO_ERROR:
      return state.set("promoIsBusy", false)
        .set("promoError", action.payload.error)
        .set("selectedPromo", defaultPromo);

    case types.CLEAR_SELECTED_DATE:
      return state.set("selectedDate", null);

    case types.SET_SELECTED_DATE:
      return state.set("selectedDate", action.payload.date.toISOString());

    case types.SET_SELECTED_TIME:
      return state.set("selectedTime", action.payload.time.toISOString())
        .set("selectedDate", action.payload.time.toISOString());

    case types.CLEAR_SELECTED_TIME:
      return state.set("selectedTime", null);

    case types.SET_PROMO_CODE:
      return state.setIn(["selectedPromo", "code"], action.payload.code)
        .set("promoError", null);

    case types.CLEAR_PROMO_CODE:
      return state.set("selectedPromo", defaultPromo)
        .set("promoError", null)
        .set("promoIsBusy", false);

    case types.VERIFY_PROMO_REQUEST:
      return state.set("promoIsBusy", true);

    case types.VERIFY_PROMO_SUCCESS:
      return state.set("selectedPromo", Immutable.from(action.payload.promo))
        .set("promoIsBusy", false)
        .set("promoError", null);

    case types.SELECT_CARD:
      return state.set("selectedCard", Immutable.from(action.payload.card));

    case types.REMOVE_SELECTED_CARD:
      return state.set("selectedCard", null);

    case types.SELECT_ADDRESS:
      return state.set("selectedAddress", Immutable.from(action.payload.address));

    case types.REMOVE_SELECTED_ADDRESS:
      return state.set("selectedAddress", null);

    case types.SELECT_POSTCODE:
      return state.set("selectedPostcode", action.payload.postcode);

    case types.CLEAR_SELECTED_POSTCODE:
      return state.set("selectedPostcode", null);

    case types.SET_SELECTED_NOTES:
      return state.set("selectedNotes", action.payload.notes);

    case types.CREATE_BOOKING_SUCCESS:
      return state.set("booking", action.payload.booking);

    case types.SET_SELECTED_CATEGORY:
      return state.set("selectedCategory", action.payload.category);

    case types.SET_SELECTED_SLOT:
      return state.set("selectedSlot", action.payload.slot);

    default:
      return state;
  }
}
