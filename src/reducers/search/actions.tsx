import { Dispatch } from "react-redux";
import axios, { AxiosResponse } from "axios";
import {config} from "../../config";
import * as types from "./types";

export function searchFilterRequest() {
  return {
    type: types.SEARCH_FILTER_REQUEST
  };
}
export function searchFilterRequested() {
  return {
    type: types.SEARCH_FILTER_REQUESTED
  };
}

function fetchCategoriesSuccess(categories: any) {
  return {
    type: types.FETCH_CATEGORIES_SUCCESS,
    payload: { categories }
  };
}

export function fetchCategories() {
  return (dispatch: Dispatch<any>) => {
    const url =  config.api.host + "/api/v1/salon/categories";

    axios.get(url)
      .then((response: AxiosResponse) => {
        dispatch(fetchCategoriesSuccess(response.data.categories));
      })
      .catch(err => console.error("FETCH CATEGORIES ERROR", err));
  };
}

function stateUpdateSelectedCategory(category: any) {
  return {
    type: types.UPDATE_SELECTED_CATEGORY,
    payload: { category }
  };
}

export function updateSelectedCategory(category: any) {
  return (dispatch: Dispatch<any>) => {
    dispatch(searchFilterRequested());
    dispatch(stateUpdateSelectedCategory(category));
  };
}

function stateUpdatePostcode(postcode: string) {
  return {
    type: types.UPDATE_POSTCODE,
    payload: { postcode }
  };
}

export function updatePostcode(postcode: string) {
  return (dispatch: Dispatch<any>) => {
    dispatch(searchFilterRequested());
    dispatch(stateUpdatePostcode(postcode));
  };
}

function updateSearchState(key: string, value: string) {
  return {
    type: types.UPDATE_SEARCH_STATE,
    payload: { key, value }
  };
}

function constructUpdateUrl(state: any) {
  console.log("STATE: ", state);
}

export function updateSearchResults(key: string, value: string) {
  return (dispatch: Dispatch<any>, getState: any) => {
    dispatch(updateSearchState(key, value));
    constructUpdateUrl(getState);
    // axios.get()
  };
}
