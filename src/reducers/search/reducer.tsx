import * as Immutable from "immutable";
const {fromJS} = Immutable;
import * as moment from "moment";
import * as types from "./types";
import { ApiCategory } from "../../api/categories";

export interface State {
  isBusy: boolean;
  postcode: string;
  type: string;
  category: {
    id: number;
    name: string;
    code: string;
  };
  categories: ApiCategory[];
  subCategories: any;
  startDate: moment.Moment;
}

const initialState = fromJS({
  isBusy: false,
  postcode: "SW100JZ",
  type: "at-home",
  category: {
    id: 5,
    name: "Nails",
    code: "nails"
  },
  categories: {},
  subCategories: {},
  startDate: moment()
});

export default function(state: Immutable.Map<{}, {}> = initialState, action: any) {
  switch (action.type) {
    case types.SEARCH_FILTER_REQUEST:
      return state;
    case types.FETCH_CATEGORIES_SUCCESS:
      return state.set("categories", fromJS(filterCategories(action.payload.categories)))
        .set("subCategories", fromJS(filterSubcategories(action.payload.categories)));
    case types.UPDATE_SEARCH_STATE:
      const key = action.payload.key;
      const value = action.payload.value;
      return state.set(key, fromJS(value));
    default:
      return state;
  }
}

function filterCategories(categories: any) {
  return categories.reduce((obj: any, cat: any) => {
    obj[cat.id] = {
      id: cat.id,
      name: cat.name,
      code: cat.code
    };
    return obj;
  }, {});
}

function filterSubcategories(categories: any) {
  return categories.reduce((obj: any, cat: any) => {
    obj[cat.id] = cat.sub_categories;
    return obj;
  }, {});
}
