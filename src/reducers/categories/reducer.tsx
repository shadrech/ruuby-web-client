import * as Immutable from "immutable";
import * as types from "./types";
import {ApiCategory} from "../../api/categories";
import {ActionType} from "../../reducers/providers/reducer";

interface CategoryInfoFeatured {
  id: string;
  name: string;
  treatment: {
    id: number;
    name: string;
    description: string;
  };
}
export interface CategoryInfo {
  urn: string;
  name: string;
  description: string;
  featured: CategoryInfoFeatured[];
}
export interface CategoryInfos {
  [urn: string]: CategoryInfo;
}

export interface State {
  isBusy: boolean;
  error: any;
  items: ApiCategory[];
  info: ApiCategory;
}

const initialState = Immutable.fromJS({
  isBusy: false,
  items: [],
  errors: null,
  info: require("../../components/categories/category-modal-info.js").default
});

export default function(state: Immutable.Map<any, any> = initialState, action: ActionType) {
  switch (action.type) {
    case types.FETCH_CATEGORIES_REQUEST:
      return state.set("isBusy", true);

    case types.FETCH_CATEGORIES_SUCCESS:
      return state.set("items", action.payload.categories)
        .set("isBusy", false)
        .set("error", null);

    default:
      return state;
  }
}
