import * as types from "./types";
import {Dispatch} from "redux";
import * as api from "../../api/categories";

export function fetchCategories() {
  return async (dispatch: Dispatch<any>) => {
    dispatch({type: types.FETCH_CATEGORIES_REQUEST});

    try {
      const categories = await api.fetchCategories();
      dispatch(fetchCategoriesSuccess(categories));
    } catch (err) {
      console.error("err", err);
      dispatch(fetchCategoriesFailure(err));
    }
  };
}

function fetchCategoriesFailure(errors: any) {
  return {
    type: types.FETCH_CATEGORIES_FAILURE,
    payload: { errors }
  };
}

function fetchCategoriesSuccess(categories: api.ApiCategory[]) {
  return {
    type: types.FETCH_CATEGORIES_SUCCESS,
    payload: { categories }
  };
}
