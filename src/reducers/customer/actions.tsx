import { Dispatch } from "react-redux";
import * as customerApi from "../../api/customer";
import * as cardApi from "../../api/card";
import {getBraintreeTokenSuccess} from "../payments/actions";
import { browserHistory } from "react-router";
import * as types from "./types";
import { constants } from "../../utils";
import {
  ApiCustomer,
  ApiCustomerProfile,
  ApiBookingMapping,
} from "../../api/customer";
import {ApiProvider} from "../../api/provider";
import {fetchBraintreeeToken} from "../../reducers/payments/actions";
import * as Cookie from "js-cookie";
import { ApiCard } from "../../api/card";
import { ApiBookingTransaction } from "../../api/booking";

export interface LoginParams {
  email: string;
  password: string;
}
export interface RegisterParams {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password: string;
  passwordConfirm: string;
}

function handleErrors(err: any, dispatch: Dispatch<any>): string {
  console.error("ERRORS", err);
  const code = JSON.parse(err.request.response).error.code;
  const errorMessage = code === constants.ERROR_TOKEN ? "Session expired. Please login again" : JSON.parse(err.request.response).error.message;
  dispatch(setCustomerError(errorMessage));

  return code;
}

function setCustomerBusy() {
  return {
    type: types.SET_CUSTOMER_BUSY
  };
}

export function setCustomerNotBusy() {
  return {
    type: types.SET_CUSTOMER_NOT_BUSY
  };
}

export function toggleEditingFlag(key: string) {
  return {
    type: types.TOGGLE_EDITING_FLAG,
    payload: { key }
  };
}

function submitLoginFormSuccess(token: string, data: ApiCustomer) {
  return {
    type: types.LOGIN_SUCCESS,
    payload: { data, token }
  };
}

export function setCustomerError(error: string) {
  return {
    type: types.CUSTOMER_ERROR,
    payload: { error }
  };
}

// function fetchCustomerSuccess(profile: ApiCustomerProfile, token) {
//   return {
//     type: types.FETCH_CUSTOMER_SUCCESS,
//     payload: { profile, token }
//   };
// }

function fetchCustomerProfileSuccess(user: ApiCustomerProfile, token: string) {
  return {
    type: types.FETCH_CUSTOMER_PROFILE_SUCCESS,
    payload: { ...user, token }
  };
}

export function fetchCustomer() {
  return async (dispatch: Dispatch<any>) => {
    try {
      const data = await customerApi.fetchCustomerProfile();
      // dispatch(fetchCustomerSuccess(data.profile, data.token));
      dispatch(fetchCustomerProfileSuccess(data.profile, data.token));

      const braintreeData = await cardApi.getBraintreeToken();
      dispatch(getBraintreeTokenSuccess(braintreeData.token));
    } catch (error) {
      handleErrors(error, dispatch);
      browserHistory.push("/login");
    }
  };
}

/**
 * Fetch customer, addresses and cards object.
 * User must be already authenticated (JWT token created)
 */
export function fetchCustomerProfile() {
  return async (dispatch: Dispatch<any>) => {
    try {
      const data = await customerApi.fetchCustomerProfile();
      dispatch(fetchCustomerProfileSuccess(data.profile, data.token));
    } catch (error) {
      console.error("FETCH CUSTOMER PROFILE ERROR", error);
    }
  };
}

export function submitLoginForm(data: LoginParams, redirect: boolean = false) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(setCustomerBusy());

    try {
      await customerApi.submitLoginForm(data);
      const customer = await customerApi.fetchCustomerProfile();
      dispatch(fetchCustomerProfileSuccess(customer.profile, customer.token));
      const braintreeData = await cardApi.getBraintreeToken();
      dispatch(getBraintreeTokenSuccess(braintreeData.token));

      if (redirect) browserHistory.push("/profile");
      dispatch(setCustomerNotBusy());
    } catch (err) {
      console.error("SUBMIT LOGIN ERROR", err);

      const mssg = JSON.parse(err.request.response).error.message;
      dispatch(setCustomerError(mssg));
    }
  };
}

export function updateCustomerAttempt(key: string) {
  return {
    type: types.UPDATE_CUSTOMER_ATTEMPT,
    payload: { key }
  };
}

export function updateCustomer(params: any) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(updateCustomerAttempt(Object.keys(params)[0]));

    try {
      await customerApi.updateCustomer(params);

      const data = await customerApi.fetchCustomerProfile();
      dispatch(fetchCustomerProfileSuccess(data.profile, data.token));
      dispatch(setCustomerNotBusy());
    } catch (err) {
      const code = handleErrors(err, dispatch);

      if (code === constants.ERROR_TOKEN)
        browserHistory.push("/login");
    }
  };
}

export function submitRegisterForm(data: RegisterParams, redirect: boolean = false) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(setCustomerBusy());

    try {
      const params = Object.assign(data);
      delete params.passwordConfirm;
      const user = await customerApi.submitRegisterForm(params);
      dispatch(submitLoginFormSuccess(user.token, user.customer));
      dispatch(fetchBraintreeeToken());

      if (redirect) browserHistory.push("/profile");
      dispatch(setCustomerNotBusy());
    } catch (err) {
      console.error(err);
      const mssg = JSON.parse(err.request.response).error.message;
      dispatch(setCustomerError(mssg));
    }
  };
}

export function setUserCredentials(credentials: any) {
  return {
    type: types.SET_CUSTOMER_CREDENTIALS,
    payload: { credentials }
  };
}

function fetchFavouritesSuccess(providers: ApiProvider[]) {
  return {
    type: types.FETCH_FAVOURITES_SUCCESS,
    payload: { providers }
  };
}

export function fetchFavourites() {
  return async (dispatch: Dispatch<any>, _getState: any) => {
    try {
      const favs = await customerApi.fetchFavourites();
      dispatch(fetchFavouritesSuccess(favs));
    } catch (error) {
      console.error("ERROR FETCHING FAVOURITES", error);
    }
  };
}

function fetchBookingsSuccess(bookings: ApiBookingMapping) {
  return {
    type: types.FETCH_CUSTOMER_BOOKINGS_SUCCESS,
    payload: { bookings }
  };
}

export function fetchBookings() {
  return async (dispatch: Dispatch<any>) => {
    try {
      const bookings = await customerApi.fetchBookings();
      dispatch(fetchBookingsSuccess(bookings));
    } catch (error) {
      console.error("ERROR FETCHING CUSTOMER BOOKINGS", error);
    }
  };
}

export function logout() {
  return dispatch => {
    Cookie.remove("token");
    dispatch({type: types.CUSTOMER_LOGOUT});

    browserHistory.push("/");
  };
}

const fetchTransactionsAttempt = () => ({
  type: types.FETCH_TRANSACTIONS_ATTEMPT
});
const fetchTransactionsSuccess = (urn: string, transactions: ApiBookingTransaction, cards: ApiCard[]) => ({
  type: types.FETCH_TRANSACTIONS_SUCCESS,
  payload: { transactions, cards, urn }
});

export function fetchTransactions(bookingUrn: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(fetchTransactionsAttempt());

    try {
      const response = await customerApi.fetchTransactionsByBookingUrn(bookingUrn);
      dispatch(fetchTransactionsSuccess(bookingUrn, response.transactions, response.cards));
    } catch (error) {
      console.error("FETCH PAYMENT METHODS ERROR", error);
    }
  };
}

const cancelBookingSuccess = (urn: string) => ({
  type: types.CANCEL_BOOKING_SUCCESS,
  payload: { urn }
});

export function cancelBooking(urn: string) {
  return async dispatch => {
    dispatch(updateCustomerAttempt("status"));

    try {
      await customerApi.cancelBooking(urn);
      dispatch(cancelBookingSuccess(urn));
    } catch (error) {
      console.error("CANCEL BOOKING", error);
    }
  };
}
