import * as Immutable from "immutable";
// import * as moment from "moment";
import * as types from "./types";
import {
  ApiCustomer, ApiBookingMapping,
} from "../../api/customer";
import {ApiProvider} from "../../api/provider";

export interface UpdatesStates {
  names: boolean;
  email: boolean;
  phone: boolean;
  status: boolean;
}
const initialUpdatesState: UpdatesStates = {
  names: false,
  email: false,
  phone: false,
  status: false
};
export interface FetchingState {
  transactions: boolean;
}
const initialFetchingState: FetchingState = {
  transactions: false
};

export interface State {
  isBusy: boolean;
  customer: ApiCustomer;
  error: any;
  updating: UpdatesStates;
  editing: UpdatesStates;
  fetching: FetchingState;
  credentials: {
    access_token: string;
  };
  favourites: ApiProvider[];
  bookings: ApiBookingMapping;
}

const initialState = Immutable.fromJS({
  isBusy: false,
  customer: null,
  error: null,
  updating: Immutable.fromJS(initialUpdatesState),
  editing: Immutable.fromJS(initialUpdatesState),
  fetching: Immutable.fromJS(initialFetchingState),
  credentials: {},
  favourites: null,
  bookings: null
});

export default function(state: Immutable.Map<any, any> = initialState, action: any) {
  switch (action.type) {
    case types.SET_CUSTOMER_BUSY:
      return state.set("isBusy", true);

    case types.SET_CUSTOMER_NOT_BUSY:
      return state.set("isBusy", false);

    case types.LOGIN_SUCCESS:
      return state.set("customer", Immutable.fromJS(action.payload.data))
        .set("credentials", {access_token: action.payload.token})
        .set("isBusy", false)
        .set("error", null)
        .set("updating", Immutable.fromJS(initialUpdatesState))
        .set("editing", Immutable.fromJS(initialUpdatesState));

    case types.FETCH_CUSTOMER_SUCCESS:
      return state.set("customer", Immutable.fromJS(action.payload.customer))
        .set("credentials", {access_token: action.payload.token})
        .set("error", null)
        .set("updating", Immutable.fromJS(initialUpdatesState))
        .set("editing", Immutable.fromJS(initialUpdatesState));

    case types.FETCH_CUSTOMER_PROFILE_SUCCESS:
      return state.set("customer", Immutable.fromJS(action.payload.customer))
        .set("credentials", {access_token: action.payload.token})
        .set("updating", Immutable.fromJS(initialUpdatesState))
        .set("editing", Immutable.fromJS(initialUpdatesState));

    case types.UPDATE_CUSTOMER_ATTEMPT:
      switch (action.payload.key) {
        case "card":
          return state.setIn(["updating", "card"], true);
        case "email":
          return state.setIn(["updating", "email"], true);
        case "phone":
          return state.setIn(["updating", "phone"], true);
        case "status":
          return state.setIn(["updating", "status"], true);
        default:
          return state.setIn(["updating", "names"], true);
      }

    case types.TOGGLE_UPDATE_FLAG:
      switch (action.payload.key) {
        case "card":
          return state.updateIn(["updating", "card"], flag => !flag);
        case "email":
          return state.updateIn(["updating", "email"], flag => !flag);
        case "phone":
          return state.updateIn(["updating", "phone"], flag => !flag);
        default:
          return state.updateIn(["updating", "names"], flag => !flag);
      }

    case types.CUSTOMER_ERROR:
      return state.set("error", action.payload.error)
        .set("updating", Immutable.fromJS(initialUpdatesState))
        .set("isBusy", false);

    case types.TOGGLE_EDITING_FLAG:
      switch (action.payload.key) {
        case "email":
          return state.updateIn(["editing", "email"], flag => !flag)
            .set("error", null);
        case "phone":
          return state.updateIn(["editing", "phone"], flag => !flag)
            .set("error", null);
        default:
          return state.updateIn(["editing", "names"], flag => !flag)
            .set("error", null);
      }

    case types.FETCH_FAVOURITES_SUCCESS:
      return state.set("favourites", action.payload.providers);

    case types.CUSTOMER_LOGOUT:
      return initialState;

    case types.FETCH_CUSTOMER_BOOKINGS_SUCCESS:
      return state.set("bookings", Immutable.fromJS(action.payload.bookings));

    case types.FETCH_TRANSACTIONS_ATTEMPT:
      return state.setIn(["fetching", "transactions"], true);

    case types.FETCH_TRANSACTIONS_SUCCESS:
      let b = state.getIn(["bookings", action.payload.urn]).toJS();
      b.cards = action.payload.cards;
      b.transactions = action.payload.transactions;
      return state.setIn(["bookings", action.payload.urn], b)
        .setIn(["fetching", "transactions"], false);

    case types.CANCEL_BOOKING_SUCCESS:
      b = state.getIn(["bookings", action.payload.urn]);
      b.status = "CANCELLED";
      return state.setIn(["bookings", action.payload.urn], b)
        .setIn(["updating", "status"], false);

    default:
      return state;
  }
}
