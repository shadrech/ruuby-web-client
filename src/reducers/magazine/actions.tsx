import { Dispatch } from "redux";

import * as types from "./types";
import {fetchPosts, Post} from "../../api/magazine";

export function fetchMagaineContentSuccess(posts: Post[]) {
  return {
    type: types.FETCH_MAGAZINE_CONTENT_SUCCESS,
    payload: { posts }
  };
}

export function fetchMagazinePosts() {
  return async (dispatch: Dispatch<any>) => {
    dispatch({type: types.FETCH_MAGAZINE_CONTENT_ATTEMPT});

    const posts = await fetchPosts();
    dispatch(fetchMagaineContentSuccess(posts));
  };
}
