import * as types from "./types";
import { fromJS } from "immutable";

export interface State {
  isBusy: boolean;
  posts: any[];
}

const initialMagazineState = fromJS({
  isBusy: false,
  posts: []
});

export default function(state: any = initialMagazineState, action: any) {
  switch (action.type) {
    case types.FETCH_MAGAZINE_CONTENT_ATTEMPT: {
      return state
        .set("isBusy", true)
        .set("posts", fromJS([]));
    }

    case types.FETCH_MAGAZINE_CONTENT_SUCCESS: {
      return state
        .set("isBusy", false)
        .set("posts", fromJS(action.payload.posts));
    }

    default:
      return state;
  }
}
