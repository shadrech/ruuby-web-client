import * as Immutable from "immutable";
import * as types from "./types";

export interface State {
  isBusy: boolean;
  data: any;
}

const initialState = Immutable.fromJS({
  isBusy: false,
  data: null
});

export default function(state: any = initialState, action: any) {
  switch (action.type) {
    case types.FETCH_INSTA_DATA_SUCCESS:
      return state.set("isBusy", false)
                  .set("data", action.payload.data);
    default:
      return state;
  }
}
