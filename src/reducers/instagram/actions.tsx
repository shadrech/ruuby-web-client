import { Dispatch } from "react-redux";
import * as types from "./types";
import * as $ from "jquery";

const access_token = "1028516166.1677ed0.0ad0d653f8664bda87ce91ab9b042540";
const URL = `https://api.instagram.com/v1/users/1028516166/media/recent/?access_token=${access_token}&count=6&callback=?`;

function fetchInstaDataSuccess(data: any) {
  return {
    type: types.FETCH_INSTA_DATA_SUCCESS,
    payload: { data }
  };
}

export function fetchInstaData() {
  return (dispatch: Dispatch<any>) => {
    $.getJSON(URL, function(json, _textStatus) {
      dispatch(fetchInstaDataSuccess(json.data));
    });
  };
}
