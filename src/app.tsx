import * as React from "react";
import {bindActionCreators} from "redux";
import { connect, Dispatch } from "react-redux";
import Header from "./components/header";
import HeaderFixed from "./components/header-fixed";
import Footer from "./components/footer";
import {ApiCustomer} from "./api/customer";
import {ApiCategory} from "./api/categories";
import {fetchCategories} from "./reducers/categories/actions";
import { actionCreators } from "./reducers/booking/actions";
import { slotTypes } from "./reducers/booking/reducer";

interface AppProps {
  location?: any;
  customer: ApiCustomer;
  categories: ApiCategory[];
  category: string;
  slot: slotTypes;
  postcode: string;
  date: string;

  fetchCategories: () => void;
  setSelectedCategory: (category: string) => void;
}

class App extends React.Component<AppProps, null> {
  render() {
    const {pathname} = this.props.location;
    const location = {
      category: this.props.category,
      slot: this.props.slot,
      postcode: this.props.postcode,
      date: this.props.date
    };

    return (
      <div id="wrapper" className="page-wrapper">
        {pathname === "/" ?
          <Header path={pathname} customer={this.props.customer} categories={this.props.categories} location={location} setSelectedCategory={this.props.setSelectedCategory} />
        :
          <HeaderFixed customer={this.props.customer} categories={this.props.categories} location={location} fetchCategories={this.props.fetchCategories} setSelectedCategory={this.props.setSelectedCategory} />}
        {this.props.children}
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    customer: state.customerState.get("customer"),
    categories: state.categoriesState.get("items"),
    category: ownProps.params.category,
    postcode: ownProps.params.postcode,
    date: ownProps.params.date,
    slot: ownProps.params.slot
  };
};

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    fetchCategories,
    setSelectedCategory: actionCreators.setSelectedCategory
  }, dispatch) as any;
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
