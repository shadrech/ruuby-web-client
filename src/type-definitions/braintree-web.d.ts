declare namespace BraintreeWeb {
  interface BraintreeClient {
    create: (args: any, callback: any) => void;
  }

  interface BraintreeHostedFields {
    create: (args: any, callback: any) => void;
  }

  interface BraintreeInstance {
    client: BraintreeClient;
    hostedFields: BraintreeHostedFields;
  }
}

declare var braintree: BraintreeWeb.BraintreeInstance;

declare module "braintree-web" {
  export = braintree;
}
