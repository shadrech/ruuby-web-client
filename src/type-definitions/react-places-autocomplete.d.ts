declare module "react-places-autocomplete" {
  interface PlacesAutocompleteProps {
    inputProps: any;
    styles?: any;
    options?: any;
    autocompleteItem?: () => void;
  }

  const PlacesAutocomplete: React.ClassicComponentClass<PlacesAutocompleteProps>;
  export default PlacesAutocomplete;
}
