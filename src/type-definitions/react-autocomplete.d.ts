declare module "react-autocomplete" {
  interface AutocompleteProps {
    inputProps: any;
    value: any;
    items: any;
    getItemValue: (item: any) => string;
    renderItem: (item: any, isHighlighted?: boolean) =>  any;
  }

  const Autocomplete: React.ClassicComponentClass<AutocompleteProps>;
  export default Autocomplete;
}
