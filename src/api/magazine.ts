import axios, { AxiosResponse } from "axios";

import {config} from "../config";

interface ApiPost {
  id: number;
  date: string;
  link: string;
  title: {
    rendered: string;
  };
  excerpt: {
    rendered: string;
  };
  featured_media: number;
};

interface ApiMedia {
  id: number;
  source_url: string;
}

export interface Post {
  id: number;
  date: string;
  link: string;
  title: string;
  excerpt: string;
  image: string;
};

export function mapPost(post: ApiPost, media: ApiMedia): Post {
  return {
    id: post.id,
    date: post.date,
    link: post.link,
    title: post.title.rendered,
    excerpt: post.excerpt.rendered,
    image: media ? media.source_url : "",
  };
}

export async function fetchPosts(): Promise<Post[]> {
  const url = `${config.magazineApi.path}/posts`;
  const response: AxiosResponse = await axios.get(url);

  const medias = await Promise.all(
    response.data
      .filter(post => post.featured_media > 0)
      .map(post => {
        return axios.get(`${config.magazineApi.path}/media/${post.featured_media}`);
      })
  );

  return response.data.map(post => {
    const media: any = medias.find((media: any) => media.data.id === post.featured_media);
    return mapPost(post, media ? media.data : null);
  });
}
