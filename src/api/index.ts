import axios, {AxiosResponse, AxiosError} from "axios";
import * as Cookie from "js-cookie";
import {config} from "../config";

export function createQueryParams(params: object): string {
  return Object.keys(params).reduce((prevStr, currParam) => {
    return prevStr + currParam + "=" + params[currParam] + "&";
  }, "");
}

const axiosInstance = axios.create({
  baseURL: config.appApi.host,
});
const headers = {
  "device-id": config.appApi.credentials.deviceId,
  "Content-Type": "application/json"
};

export const get = (path: string) => {
  if (Cookie.get("token")) headers["authorization"] = Cookie.get("token");

  return axiosInstance.get(path, { headers })
    .then((data: AxiosResponse) => data)
    .catch((err: AxiosError) => {
      if (err.request.status === 401) {
        Cookie.remove("token");
        window.location.reload();
      }
      throw err;
    });
};

export const post = (path: string, data?: object) => {
  if (Cookie.get("token")) headers["authorization"] = Cookie.get("token");

  return axiosInstance.post(path, data, { headers })
    .then((data: AxiosResponse) => data)
    .catch((err: AxiosError) => {
      if (err.request.status === 401) {
        Cookie.remove("token");
        window.location.reload();
      }
      throw err;
    });
};

export const put = (path: string, data?: object) => {
  if (Cookie.get("token")) headers["authorization"] = Cookie.get("token");

  return axiosInstance.put(path, data, { headers })
    .then((data: AxiosResponse) => data)
    .catch((err: AxiosError) => {
      if (err.request.status === 401) {
        Cookie.remove("token");
        window.location.reload();
      }
      throw err;
    });
};
