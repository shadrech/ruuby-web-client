import * as api from "./";
import {AxiosResponse} from "axios";

export interface ApiCategory {
  urn: string;
  name: string;
  image: string;
  code: string;
}

function mapCategoryObject(data: any): ApiCategory {
  return {
    urn: data["@id"],
    name: data.name,
    image: data.image,
    code: data.code
  };
}

export async function fetchCategories(): Promise<ApiCategory[]> {
  const url = "service/category?selectAll=true";
  const response: AxiosResponse = await api.get(url);

  return response.data.result.map(cat => mapCategoryObject(cat));
}
