import {AxiosResponse} from "axios";
import * as api from "./";

export interface ApiAddress {
  urn: string;
  address1: string;
  address2: string;
  postcode: string;
  isDeleted: boolean;
}

export function mapAddressObject(data: any): ApiAddress {
  return {
    urn: data["@id"],
    address1: data.address1,
    address2: data.address2,
    postcode: data.postcode,
    isDeleted: Boolean(data.isDeleted)
  };
}

export async function createAddress(data: object): Promise<ApiAddress> {
  const url = "/customer/address";
  const response: AxiosResponse = await api.post(url, data);

  return mapAddressObject(response.data.result.address);
}
