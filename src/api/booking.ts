import {AxiosResponse} from "axios";
import * as moment from "moment";
import * as api from "./";
import { ApiAddress, mapAddressObject } from "./address";
import { ApiCard } from "./card";

export interface ApiPromo {
  code: string;
  isValid: boolean;
  totalAmount: number;
  totalDuration: number;
  treatmentPrices: {
    [urn: string]: number
  };
}

interface ApiSalon {
  urn: string;
  address: ApiAddress;
  commission: number;
  imageData: any;
  images: string[];
  isActive: boolean;
  isMobile: boolean;
  logoImages: string[];
  name: string;
  products: string;
  quote: string;
  rating: string;
  recommended: boolean;
  review: string;
  userUrn: string;
}

export interface ApiBookingTransaction {
  urn: string;
  transactionId: string;
  bookingUrn: string;
  type: string;
  amount: number;
  isDeleted: boolean;
  timeCreated: string;
  timeUpdated: string;
}

export interface ApiBookingTreatment {
  urn: string;
  bookingUrn: string;
  price: number;
  therapistUrn: string;
  timeCreated: moment.Moment;
  timeUpdated: moment.Moment;
  treatment: {
    urn: string;
    description: string;
    duration: number;
    name: string;
    price: number;
  };
}

export interface ApiBooking {
  urn: string;
  timeStarts: moment.Moment;
  timeEnds: moment.Moment;
  customerUrn: string;
  addressUrn: string;
  address?: ApiAddress;
  salonUrn: string;
  salon?: ApiSalon;
  originalSalonUrn: string;
  notes: string;
  status: string;
  commission: number;
  timeCreated: moment.Moment;
  timeUpdated: moment.Moment;
  timeCancelled: moment.Moment;
  chatUrn: string;
  paymentMethods?: ApiCard[];
  bookingTreatments?: ApiBookingTreatment[];
  therapist?: {
    urn: string;
    name: string;
    salonUrn: string;
  };
  isChatAllowed?: boolean;
  cards?: ApiCard[];
  transactions?: ApiBookingTransaction[];
  completed?: boolean;
}

function mapPromoObject(data: any, code: string): ApiPromo {
  return {
    code,
    isValid: data.isValid,
    totalAmount: Number(data.totalAmount),
    totalDuration: Number(data.totalDuration),
    treatmentPrices: data.treatmentPrices
  };
}

export function mapTreatmentObject(data: any): ApiBookingTreatment {
  return {
    urn: data["@id"],
    bookingUrn: data.bookingUrn,
    price: data.price,
    therapistUrn: data.therapistUrn,
    timeUpdated: moment(data.timeUpdated),
    timeCreated: moment(data.timeCreated),
    treatment: {
      urn: data.treatment["@id"],
      description: data.treatment.description,
      duration: data.treatment.duration,
      name: data.treatment.name,
      price: data.treatment.price
    }
  };
}

export async function verifyPromo(promoCode: string, serviceUrns: string[]): Promise<ApiPromo> {
  const url = "/customer/promocode/validate";
  const response: AxiosResponse = await api.post(url, { promoCode, serviceUrns });

  return mapPromoObject(response.data.result, promoCode);
}

export function mapBookingObject(data: any): ApiBooking {
  let booking: ApiBooking = {
    urn: data["@id"] || data.urn,
    timeStarts: moment(data.timeStarts),
    timeEnds: moment(data.timeEnds),
    customerUrn: data.customerUrn,
    addressUrn: data.addressUrn,
    salonUrn: data.salonUrn,
    originalSalonUrn: data.originalSalonUrn,
    notes: data.notes,
    status: data.status,
    commission: Number(data.commission),
    timeCreated: moment(data.timeCreated),
    timeUpdated: moment(data.timeUpdated),
    timeCancelled: moment(data.timeCancelled),
    chatUrn: data.chatUrn
  };

  if (data.therapist)
    booking.therapist = {
      urn: data.therapist["@id"],
      name: data.therapist.name,
      salonUrn: data.therapist.salonUrn
    };
  if (data.bookingTreatments)
    booking.bookingTreatments = data.bookingTreatments.map(mapTreatmentObject);
  if (data.salon)
    booking.salon = data.salon;
  if (data.address)
    booking.address = mapAddressObject(data.address);

  return booking;
}

export async function createBooking(state: any, therapistUrn: string): Promise<ApiBooking> {
  const url = "/customer/booking";
  let bookingData: any = {
    addressUrn: state.bookingState.selectedAddress.urn,
    bookingDate: state.bookingState.selectedDate,
    notes: state.bookingState.selectedNotes,
    paymentMethodToken: state.bookingState.selectedCard.token,
    serviceUrns: state.basketState.getIn(["items", therapistUrn]).toJS().map(service => service.urn),
    therapistUrn,
    origin: "WEBSITE"
  };

  if (state.bookingState.selectedPromo)
    bookingData.promoCode = state.bookingState.selectedPromo.code;

  const response: AxiosResponse = await api.post(url, bookingData);
  return mapBookingObject(response.data.result.booking);
}
