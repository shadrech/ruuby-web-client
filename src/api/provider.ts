import {AxiosResponse} from "axios";
import * as api from "./index";
import * as moment from "moment-timezone";
import {config} from "../config";

/* ---------------- PROVIDER -----------------*/
export interface ApiTherapistService {
  urn: string;
  name: string;
  price: number;
  duration: number;
  category: string;
  description: string;
}
interface ApiCategoryMappedTherapistService {
  category: string;
  services: ApiTherapistService[];
}
interface ApiTherapist {
  urn: string;
  name: string;
  services: ApiCategoryMappedTherapistService[];
}
interface ApiCategory {
  name: string;
  subCategories: string[];
}
export interface ApiSalonAddress {
  address1: string;
  address2: string;
  city: string;
  postcode: string;
}
export interface ApiSalon {
  urn: string;
  name: string;
  address: ApiSalonAddress;
  isMobile: boolean;
  images: string[];
  rating: number;
  profileImage?: string;
  insight: string;
  review: {
    body: string;
    author: string;
  };
  products: string;
  recommended: boolean;
  categories: ApiCategory[];
  isFavourite?: boolean;
  isRuubyOnDemand?: boolean;
}
export interface ApiProvider {
  therapist: ApiTherapist;
  salon: ApiSalon;
  availability: string[];
}

function mapServicesObject(services: any): ApiCategoryMappedTherapistService[] {
  const newServices: ApiTherapistService[] = services.map(service => ({
    urn: service["@id"],
    category: service.category,
    description: service.description,
    duration: service.duration,
    name: service.name,
    price: service.price
  }));

  let prevCategory: string = "";
  const mappedCategories: ApiCategoryMappedTherapistService[] = newServices.map(service => ({
    category: service.category,
    services: []
  })).filter(cat => {
    const duplicate = cat.category === prevCategory;
    prevCategory = cat.category;
    return !duplicate;
  });
  newServices.forEach(service => {
    const idx = mappedCategories.findIndex(cat => cat.category === service.category);
    mappedCategories[idx].services.push(service);
  });
  return mappedCategories;
}

export function mapProviderObject(data: any): ApiProvider {
  const {therapist, salon, isRuubyOnDemand, isFavourite} = data;
  const provider: ApiProvider = {
    therapist: {
      urn: therapist["@id"],
      name: therapist.name,
      services: mapServicesObject(therapist.services)
    },
    salon: {
      urn: salon["@id"],
      name: salon.name,
      address: salon.address,
      isMobile: salon.isMobile,
      images: salon.images,
      rating: salon.rating ? Number.parseFloat(salon.rating) : null,
      profileImage: salon.profileImage ? salon.profileImage : salon.logoImages[0],
      insight: salon.insight,
      review: salon.review,
      products: salon.products,
      recommended: salon.recommended,
      categories: salon.categories,
      isRuubyOnDemand,
      isFavourite
    },
    availability: data.availability
  };

  return provider;
}

interface SalonExtras {
  isRuubyOnDemand?: boolean;
  isFavourite?: boolean;
}
export function mapSalonObject(data: any, extras?: SalonExtras): ApiSalon {
  let salon: ApiSalon = {
    urn: data["@id"],
    name: data.name,
    address: data.address,
    isMobile: data.isMobile,
    images: data.images,
    rating: data.rating ? Number.parseFloat(data.rating) : null,
    profileImage: data.profileImage ? data.profileImage : data.logoImages[0],
    insight: data.insight,
    review: data.review,
    products: data.products,
    recommended: data.recommended,
    categories: data.categories,
  };

  if (extras.isFavourite)
    salon.isFavourite = extras.isFavourite;
  if (extras.isRuubyOnDemand)
    salon.isRuubyOnDemand = extras.isRuubyOnDemand;

  return salon;
}

export async function fetchProvider(providerUrn: string): Promise<ApiProvider> {
  const url = `/therapists/${providerUrn}`;
  const response: AxiosResponse = await api.get(url);
  return mapProviderObject(response.data.result);
}

export async function fetchProviders(category: string, slot: string, date: string, postcode: string): Promise<ApiProvider[]> {
  date = moment.tz(date, config.timezone).utc(true).toISOString();
  const queries: any = {date, postcode};
  if (category !== "All") queries.category = category;
  if (slot !== "All-Day" && slot !== "All Day") queries.slot = slot;
  const url = `/therapists?${api.createQueryParams(queries)}`;
  const response: AxiosResponse = await api.get(url);
  return response.data.result.map(provider => mapProviderObject(provider));
}

/* --------------- PROVIDER AVAILABILITY ---------------*/
export interface ApiAvailability {
  dateSlots: moment.Moment[];
  dateValue: moment.Moment;
}

function mapAvailabilityObject(result: object): ApiAvailability[] {
  let data = Object.keys(result);
  const availability: ApiAvailability[] = data.map(key => ({
    dateValue: moment(key),
    dateSlots: result[key].map(d => moment(d))
  }));

  return availability;
}

export async function fetchAvailability(providerUrn: string, serviceUrns: string[]) {
  const url = `/therapists/${providerUrn}/availability`;
  const response: AxiosResponse = await api.post(url, {serviceUrns});
  return mapAvailabilityObject(response.data.result);
}
