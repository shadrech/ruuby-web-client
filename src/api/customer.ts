import {AxiosResponse} from "axios";
import * as Cookie from "js-cookie";

import * as api from "./index";
import {config} from "../config";
import {
  LoginParams,
  RegisterParams
} from "../reducers/customer/actions";
import {
  ApiAddress,
  mapAddressObject
} from "./address";
import {
  ApiCard,
  mapCardObject
} from "./card";
import {mapProviderObject, ApiProvider} from "../api/provider";
import { ApiBooking, mapBookingObject } from "./booking";

export interface ApiCustomer {
  urn: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}
export interface ApiCustomerProfile {
  customer: ApiCustomer;
  cards: ApiCard[];
  addresses: ApiAddress[];
}
export interface ApiBookingMapping {
  [urn: string]: ApiBooking;
}

function mapCustomerObject(data: any): ApiCustomer {
  return {
    urn: data["@id"],
    email: data.email,
    firstName: data.firstName,
    lastName: data.lastName,
    phone: data.phone
  };
}
function mapCustomerProfileObject(data): ApiCustomerProfile {
  return {
    customer: mapCustomerObject(data.customer),
    addresses: data.addresses.map(addr => mapAddressObject(addr)),
    cards: data.cards.map(card => mapCardObject(card))
  };
}

interface LoginData {
  token: string;
  customer: ApiCustomer;
}

export async function submitLoginForm(data: LoginParams): Promise<LoginData> {
  const url = config.appApi.host + "/login";
  const response: AxiosResponse = await api.post(url, data);

  Cookie.set("token", response.headers.token, { expires: config.jwtExpiration });

  return {
    token: response.headers.token,
    customer: mapCustomerObject(response.data.result)
  };
}

export async function submitRegisterForm(data: RegisterParams): Promise<any> {
  const url = "/signup";
  const response: AxiosResponse = await api.post(url, data);

  Cookie.set("token", response.headers.token, { expires: config.jwtExpiration });

  return {
    token: response.headers.token,
    customer: mapCustomerObject(response.data.result)
  };
}

export async function fetchCustomer() {
  const url = "/customer";
  const response: AxiosResponse = await api.get(url);

  return {
    token: response.headers.token,
    customer: mapCustomerObject(response.data.result)
  };
}

export async function fetchCustomerProfile(): Promise<{token: string; profile: ApiCustomerProfile}> {
  const url = "/customer/profile";
  const response: AxiosResponse = await api.get(url);

  return {
    token: Cookie.get("token"),
    profile: mapCustomerProfileObject(response.data.result)
  };
}

export async function updateCustomer(params: any): Promise<AxiosResponse> {
  const url = "/customer";
  const response: AxiosResponse = await api.put(url, params);

  return response;
}

export async function fetchFavourites(): Promise<ApiProvider[]> {
  const url = "/customer/favourite";
  const response: AxiosResponse = await api.get(url);

  return response.data.result.map(data => mapProviderObject(data));
}

export async function fetchBookings(): Promise<ApiBookingMapping> {
  const url = "/customer/bookings";
  const response: AxiosResponse = await api.get(url);

  let bookings: ApiBookingMapping = {};
  response.data.result.completed.forEach(b => {
    let booking = mapBookingObject(b);
    booking.completed = true;
    bookings[booking.urn] = booking;
  });
  response.data.result.upcoming.forEach(b => {
    let booking = mapBookingObject(b);
    booking.completed = false;
    bookings[booking.urn] = booking;
  });

  return bookings;
}

export async function fetchTransactionsByBookingUrn(bookingUrn: string): Promise<{cards: ApiCard[], transactions: any}> {
  const url = `/bookings/${bookingUrn}/transactions`;

  const response: AxiosResponse = await api.get(url);
  return {
    cards: response.data.result.paymentMethods.map(mapCardObject),
    transactions: response.data.result.transactions
  };
}

export async function cancelBooking(urn: string): Promise<any> {
  const url = `/booking/${urn}/cancel`;

  return await api.put(url);
}
