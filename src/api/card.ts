import {AxiosResponse} from "axios";
import * as api from "./index";

export interface ApiCard {
  token: string;
  cardType: string;
  expirationMonth: number;
  expirationYear: number;
  last4: string;
  imageUrl: string;
}

export function mapCardObject(data: any): ApiCard {
  return {
    token: data.token,
    cardType: data.cardType,
    expirationMonth: data.expirationMonth,
    expirationYear: data.expirationYear,
    imageUrl: data.imageUrl,
    last4: data.last4
  };
}

export async function getBraintreeToken() {
  const url = "/payment/token";
  const response: AxiosResponse = await api.get(url);

  return {
    token: response.data.result.btreeClientToken
  };
}

export async function createCard(payment_method_nonce: string) {
  const url = "/payment/method";
  const response: AxiosResponse = await api.post(url, { payment_method_nonce });

  return response.data.result;
}

export async function fetchCards(): Promise<ApiCard[]> {
  const url = "/payment/method";
  const response: AxiosResponse = await api.get(url);

  return response.data.result.map(card => mapCardObject(card));
}
