import * as moment from "moment";
import querystring = require("querystring");
import "whatwg-fetch";
import * as _ from "lodash";

import {config} from "./config";

const categoryMap = {
  1: "Hair",
  5: "Nails",
  18: "Waxing",
  26: "Eyes",
  30: "Tanning",
  22: "Makeup",
  9: "Wellness",
  33: "Facial",
  14: "Massage"
};

// a pattern for dealing with errors: https://www.tjvantoll.com/2015/09/13/fetch-and-errors/
function handleErrors(response: any) {
  if (!response.ok) { // response.ok - status in the range 200-299?
    throw Error(response.statusText);
  }
  return response;
}

export function fetchSalons(category: number, categories: string, timePeriod: string) {
  const times = timePeriodParams(timePeriod);

  const params = {
    category: (categories) ? categories : category,
    start: times[0],
    end: times[1],
    page: 1,
    sort: "rating",
    sort_dir: "asc"
  };

  const url = `${config.api.host}/api/v1/salon?${querystring.stringify(params)}`;

  return fetch(url)
    .then(handleErrors)
    .then(response => response.json())
    .then(data => parseSalons(data.salons, category));
}

export function fetchSingleSalon(category: number, salonId: number): Promise<Provider[]> {
  const params = {
    category,
    salon_id: salonId,
  };

  const url = `${config.api.host}/api/v1/salon/show?${querystring.stringify(params)}`;

  return fetch(url)
    .then(handleErrors)
    .then(response => response.json())
    .then(data => parseSalons([data.salon], category));
}

export function fetchTherapists(category: number, categories: string, timePeriod: string, postcode: string): Promise<Provider[]> {
  const times = timePeriodParams(timePeriod);

  const params = {
    category: (categories) ? categories : category,
    postcode,
    start: times[0],
    end: times[1],
    page: 1,
    sort: "rating",
    sort_dir: "asc"
  };

  const url = `${config.api.host}/api/v1/therapists?${querystring.stringify(params)}`;

  return fetch(url)
    .then(handleErrors)
    .then(response => response.json())
    .then(data => parseTherapists(data.therapists, category));
}

export function fetchSingleTherapist(category: number, providerId: number, workstationId: number): Promise<Provider[]> {
  const params = {
    category,
    salon_id: providerId,
    therapist_id: workstationId
  };

  const url = `${config.api.host}/api/v1/therapists/show?${querystring.stringify(params)}`;

  return fetch(url)
    .then(handleErrors)
    .then(response => response.json())
    .then(data => parseTherapists([data.therapist], category));
}

export interface Treatment {
  id: number;
  name: string;
  duration: number;
  description: string;
  price: {
    value: number;
    formatted: string;
  };
}

export interface CategoryTreatments {
  category: string;
  treatments: Treatment[];
}

export interface Provider {
  type: string;
  id: number;
  workstationId?: number;
  isMobile: boolean;
  name: string;
  images: string[];
  review: string;
  rating: number;
  address?: string;
  treatments: Treatment[];
  categories: string[];
  otherTreatments: CategoryTreatments[];
  logo: string;
}

interface RawService {
  id: number;
  name: string;
  duration: number;
  description: string;
  price: {
    value: string;
    formatted: string;
  };
}

interface RawOtherCategory {
  name: string;
  services: RawService[];
}

interface RawSalonData {
  id: number;
  workstationId?: number;
  name: string;
  review: string;
  latitude: number;
  longitude: number;
  rating: number;
  is_faved: boolean;
  is_mobile: boolean;
  address: string;
  city: string;
  logo: string;
  images: string[];
  services: RawService[];
  other_categories: RawOtherCategory[];
}

interface RawTherapistData {
  id: number;
  name: string;
  salon: RawSalonData;
}

function parseTherapists(therapistData: RawTherapistData[], categoryId: number): Provider[] {
  return therapistData.map((therapist): Provider => {
    const treatments = therapist.salon.services.map(mapTreatment);

    therapist.salon.images.unshift(therapist.salon.logo);

    const categories = mapOtherCategories(therapist.salon.other_categories, categoryId);
    const otherTreatments = mapOtherTreatments(therapist.salon.other_categories);

    return {
      type: "home",
      id: therapist.salon.id,
      workstationId: therapist.id,
      name: therapist.name,
      images: therapist.salon.images,
      review: therapist.salon.review,
      rating: therapist.salon.rating,
      isMobile: true,
      logo: therapist.salon.logo,
      treatments,
      otherTreatments,
      categories,
    };
  });
}

function parseSalons(salonData: RawSalonData[], categoryId: number): Provider[] {
  return salonData.map((salon): Provider => {
    const treatments = salon.services.map(mapTreatment);

    salon.images.unshift(salon.logo);

    const categories = mapOtherCategories(salon.other_categories, categoryId);
    const otherTreatments = mapOtherTreatments(salon.other_categories);

    return {
      type: "salon",
      id: salon.id,
      name: salon.name,
      images: salon.images,
      review: salon.review,
      rating: salon.rating,
      address: salon.address,
      isMobile: false,
      logo: salon.logo,
      treatments,
      categories,
      otherTreatments
    };
  });
}

function mapOtherCategories(otherCategories: RawOtherCategory[], categoryId: number): string[] {
  const categories = otherCategories
    .filter(category => category.services.length > 0)
    .map(category => _.startCase(category.name.toLowerCase()));
  categories.unshift(categoryMap[categoryId]);

  return categories;
}

function mapTreatment(treatment: RawService): Treatment {
  return {
    id: treatment.id,
    name: treatment.name,
    duration: treatment.duration,
    description: treatment.description,
    price: {
      value: Number.parseInt(treatment.price.value),
      formatted: treatment.price.formatted
    }
  };
}

function mapOtherTreatments(otherCategories: RawOtherCategory[]): CategoryTreatments[] {
  return otherCategories
    .filter(category => category.services.length > 0)
    .map(category => ({
      category: category.name,
      treatments: category.services.map(mapTreatment)
    }));
}

function timePeriodParams(timePeriod: string) {
  return [
    moment(timePeriod).unix(),
    moment(timePeriod).endOf("day").unix()
  ];
}

interface RawPostCodeValidateResponse {
  result: boolean;
  status: number;
}

export function validatePostcode(postcode: string): Promise<boolean> {
  const url = `https://api.postcodes.io/postcodes/${encodeURIComponent(postcode)}/validate`;

  return fetch(url)
    .then(response => response.json())
    .then((data: RawPostCodeValidateResponse) => data.result)
    .catch(() => false);
}

export function getPostcodeFromLongitudeLatitude(lon: number, lat: number): Promise<string> {
  const url = `https://api.postcodes.io/postcodes?lon=${lon}&lat=${lat}`;

  return fetch(url)
    .then(response => response.json())
    .then(data => data.result[0].postcode);
}
