import "es6-shim";
import "whatwg-fetch";
import "./polyfills/object-values";
import { hotjar } from "react-hotjar";
import * as React from "react";
import * as ReactDOM from "react-dom";
import {browserHistory} from "react-router";
import {compose, createStore, applyMiddleware, AnyAction} from "redux";
import thunkMiddleware from "redux-thunk";

import {appReducer, RootReducer} from "./reducer";
import {Root} from "./root";
import { CUSTOMER_LOGOUT } from "./reducers/customer/types";

import "react-select/dist/react-select.css";

import "./css/grid.css";
import "./css/site.css";
import "./css/new.css";
import "./sass/index.scss";

import { initialise } from "./utils/meta-controller/index";
initialise();

const reducers = (state: RootReducer, action: AnyAction) => {
  if (action.type === CUSTOMER_LOGOUT)
    state = undefined;

  return appReducer(state, action);
};

declare const DEVELOPMENT: boolean;

let store;
if (DEVELOPMENT) {
  hotjar.initialize("750851", 6);
  const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const enhancer = composeEnhancers(
    applyMiddleware(thunkMiddleware)
  );
  store = createStore(reducers, enhancer);
} else {
  hotjar.initialize("751239", 6);
  store = createStore(
    reducers,
    applyMiddleware(thunkMiddleware)
  );
}

ReactDOM.render(
  <Root store={store} history={browserHistory} />,
  document.getElementById("root")
);
