import * as React from "react";
import {Route, IndexRoute, RedirectFunction, Redirect} from "react-router";
import * as moment from "moment";
import * as Cookie from "js-cookie";
import App from "./app";

import HomeContainer from "./components/home";
import SearchContainer from "./components/search/search-container";
import CategoryContainer from "./components/categories/categories-container";
import AboutContainer from "./components/about";
import FAQSContainer from "./components/faqs";
import PrivacyPolicyContainer from "./components/privacy-policy";
import {JoinContainer} from "./components/join";
import { Provider } from "./components/provider";
import LoginContainer from "./components/customer/login/login-container";
import RegisterContainer from "./components/customer/register/register-container";
import ProfileContainer from "./components/customer/profile/profile-container";
import PriceListContainer from "./components/price-list";

import { Checkout } from "./components/checkout";
import BookingConfirmation from "./components/checkout/booking-confirmation";

interface StringDictionary {
  [index: string]: string;
}

export const urls: StringDictionary = {
  category: "/category",
  search: "/search",
  provider: "/provider/:urn"
};

export function constructUrl(pathId: string, parts: StringDictionary = {}) {
  const url = urls[pathId];

  Object.keys(parts).forEach(key => {
    url.replace(key + ":", parts[key]);
  });

  return url;
}

function scrollTopNewRoute(_prevState: any, nextState: any) {
  if (nextState.location.action !== "POP") {
    window.scrollTo(0, 0);
  }
}

function checkAuthorization(_nextState: any, replace: RedirectFunction) {
  if (!Cookie.get("token"))
    replace("/login" as any);
}

function rerouteIfAuthenticated(_nextState: any, replace: RedirectFunction) {
  if (Cookie.get("token"))
    replace("/profile" as any);
}

function constructSearchUrl(category: string): string {
  return `/search/${category}/${moment().format("YYYY-MM-DD")}/All-Day`;
}

export const routes = (
 <Route component={App} path="/" onChange={scrollTopNewRoute}>
   <IndexRoute component={HomeContainer} />
   <Route path={urls["category"]} component={CategoryContainer} />
   <Route path="/search/:category/:date/:slot" component={SearchContainer} />
   <Route path="/search/:category/:date/:slot/:postcode" component={SearchContainer} />
   <Route path={urls["provider"]} component={Provider} />
   <Route path="/:urn/checkout" component={Checkout} />
   <Route path="/booking-confirmation" component={BookingConfirmation} />
   <Route path="/about" component={AboutContainer} />

   <Redirect from="/massage" to={constructSearchUrl("Massage")} />
   <Redirect from="/nails" to={constructSearchUrl("Nails")} />
   <Redirect from="/hair" to={constructSearchUrl("Hair")} />
   <Redirect from="/waxing" to={constructSearchUrl("Waxing")} />
   <Redirect from="/eyes" to={constructSearchUrl("Eyes")} />
   <Redirect from="/tanning" to={constructSearchUrl("Tanning")} />
   <Redirect from="/makeup" to={constructSearchUrl("Makeup")} />
   <Redirect from="/wellness" to={constructSearchUrl("Wellness")} />
   <Redirect from="/facial" to={constructSearchUrl("Facial")} />
   <Redirect from="/ruuby-men" to={constructSearchUrl("Ruuby Men")} />
   <Redirect from="/cosmetic-injectables" to={constructSearchUrl("Cosmetic Injectables")} />

   <Route path="/price-list" component={PriceListContainer} />
   <Route path="/faqs" component={FAQSContainer} />
   <Route path="/privacy-policy" component={PrivacyPolicyContainer} />
   <Route path="/join" component={JoinContainer} />
   <Route path="/login" component={LoginContainer} onEnter={rerouteIfAuthenticated} />
   <Route path="/register" component={RegisterContainer} onEnter={rerouteIfAuthenticated} />
   <Route path="/profile" component={ProfileContainer}  onEnter={checkAuthorization} />
   <Route path="/profile/:page" component={ProfileContainer} onEnter={checkAuthorization} />
   <Route path="/profile/:page/:urn" component={ProfileContainer} onEnter={checkAuthorization} />

   <Redirect from="*" to="/" />
  </Route>
);
