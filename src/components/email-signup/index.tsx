import * as React from "react";
import * as moment from "moment";
import { RuubyEmblem } from "../sub/svgs";
const cookie = require("react-cookie");
import {COOKIE_HIDE_EMAIL_SIGNUP} from "../home";
import { ModalShell } from "../sub/modal";

interface Props {
  hide: () => void;
  showSignup: boolean;
}

export default class EmailSignup extends React.Component<Props, {}> {
  handleFormSubmit = (event) => {
    event.preventDefault();
    cookie.save(COOKIE_HIDE_EMAIL_SIGNUP, true, {path: "/", expires: moment().add(1, "month").toDate()});

    event.target.submit();
  }

  renderModalContent = (): JSX.Element => {
    return (
      <div className="col-xs-12">
        <div className="logo signup-page">
          <RuubyEmblem />
        </div>
        <div className="header signup-page">
          <h2>Welcome to Ruuby</h2>
        </div>
        <p className="description">
          We do very cool emails with beauty news, offers and tips<br/><br/>Join the #Movement below and get<br/><span className="signupDiscount">£10 OFF</span><br />your first booking
        </p>
        <div className="newsletter-input homepage-signup">
          <form method="post" onSubmit={this.handleFormSubmit} action="//ruuby.us3.list-manage.com/subscribe/post?u=47d4f2f6a0d2b2debbbeeda73&amp;id=4b9e19a2f7">
            <input type="text" name="EMAIL" ref="emailInput" placeholder="Enter Email" />
            <input className="submit-btn" type="submit" value="Subscribe" name="subscribe" />
          </form>
        </div>
      </div>
    );
  }

  render() {
    const modalStyle = {
      content: {
        height: "fit-content",
        minHeight: "600px",
        left: "50%",
        right: "auto",
        transform: "translateX(-50%)",
      }
    };

    return (
      <div className="email-signup">
        <ModalShell
          style={modalStyle}
          show={this.props.showSignup}
          label="Signup"
          closeModal={this.props.hide}>
          {this.renderModalContent()}
        </ModalShell>
      </div>
    );
  }
}
