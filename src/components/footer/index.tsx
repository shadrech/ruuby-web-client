import * as React from "react";
import {Link} from "react-router";
import { RuubyEmblem, LinkedinLogo, InstagramLogo, TwitterLogo, PinterestLogo, FacebookLogo } from "../sub/svgs";

import "./styles/index.scss";

export default class Footer extends React.Component<{}, {}> {
  render() {
    return (
      <footer id="new-footer">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-4 col-md-5 signup">
              <p><b>Join the #Movement</b></p>
              <p>Sign up now to receive news, beauty tips, special offers and more!</p>
              <div className="newsletter-input">
                <form method="post" action="//ruuby.us3.list-manage.com/subscribe/post?u=47d4f2f6a0d2b2debbbeeda73&amp;id=4b9e19a2f7">
                  <input type="text" name="EMAIL" ref="emailInput" placeholder="Enter Your Email" className="email-input" />
                  <input className="submit-btn" type="submit" value="Subscribe" name="subscribe" />
                </form>
              </div>
            </div>
            <div className="col-xs-4 col-md-2 emblem">
              <RuubyEmblem />
            </div>
            <div className="col-xs-4 col-md-5 social">
              <p><b>Follow Ruuby</b></p>
              <div className="social-logos">
                <a href="https://www.facebook.com/ruubyapp/" target="_blank"><FacebookLogo /></a>
                <a href="https://twitter.com/ruubyapp" target="_blank"><TwitterLogo /></a>
                <a href="https://www.pinterest.com/ruubyapp/" target="_blank"><PinterestLogo /></a>
                <a href="https://www.instagram.com/ruubyapp/" target="_blank"><InstagramLogo /></a>
                <a href="https://www.linkedin.com/company-beta/5262281/" target="_blank"><LinkedinLogo /></a>
              </div>
            </div>
          </div>
          <div className="row sub-nav">
            <div className="links">
              <Link to="/about">About Ruuby</Link>
              <Link to="/faqs">FAQs</Link>
              <Link to="/privacy-policy">Privacy Policy</Link>
            </div>
            <div className="copyright">
              &copy;2017 Ruuby. All Rights Reserved.
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
