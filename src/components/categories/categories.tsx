import * as React from "react";
import Category from "./category";
import {ApiCategory} from "../../api/categories";
import {CategoryInfos} from "../../reducers/categories/reducer";

interface CategoriesProps {
  categories: ApiCategory[];
  categoryInfos: CategoryInfos;
  postcode: string;

  setSelectedCategory: (cat: string) => void;
}
interface CategoriesState {
  width?: number;
}

export default class Categories extends React.Component<CategoriesProps, CategoriesState> {
  constructor(props: CategoriesProps) {
    super(props);
    this.state = {
      width: screen.width
    };
  }

  componentWillMount() {
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  handleResize = () => {
    const prevW = this.state.width; // previous width
    const currW = window.innerWidth; // current width

    if (prevW >= 991 && currW < 991) this.setState({width: 990});
    else if (prevW <= 991 && currW > 991) this.setState({width: 992});
    else if (prevW >= 767 && currW < 767) this.setState({width: 760});
    else if (prevW <= 767 && currW > 767) this.setState({width: 780});
  }

  renderDesktopCategories = (categories: ApiCategory[]) => {
    return (
      <div className="categories-wrapper">
        <div className="row first justify-content-md-center">
          {categories.length !== 0 && categories.slice(0, 4).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row second justify-content-md-center">
          {categories.length !== 0 && categories.slice(4, 8).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row third justify-content-md-center">
          {categories.length !== 0 && categories.slice(8).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
      </div>
    );
  }

  renderTabletCategories(categories: ApiCategory[]) {
    return (
      <div className="categories-wrapper">
        <div className="row first">
          {categories.length !== 0 && categories.slice(0, 3).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row second">
          {categories.length !== 0 && categories.slice(3, 6).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row third">
          {categories.length !== 0 && categories.slice(6, 9).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row fourth">
          {categories.length !== 0 && categories.slice(9).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
      </div>
    );
  }

  renderMobileCategories(categories: ApiCategory[]) {
    return (
      <div className="categories-wrapper">
        <div className="row first">
          {categories.length !== 0 && categories.slice(0, 2).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row second">
          {categories.length !== 0 && categories.slice(2, 4).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row third">
          {categories.length !== 0 && categories.slice(4, 6).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row fourth">
          {categories.length !== 0 && categories.slice(6, 8).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row fifth justify-content-xs-center">
          {categories.length !== 0 && categories.slice(8, 10).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
        <hr/>
        <div className="row sixth justify-content-xs-center">
          {categories.length !== 0 && categories.slice(10).map(cat => <Category key={cat.code} category={cat} info={this.props.categoryInfos[cat.urn]} categories={this.props.categories} postcode={this.props.postcode} setSelectedCategory={this.props.setSelectedCategory} />)}
        </div>
      </div>
    );
  }

  renderCategories() {
    const {categories} = this.props;
    let {width} = this.state;

    if (width <= 767) return this.renderMobileCategories(categories);
    else if (width <= 991 && width >= 767) return this.renderTabletCategories(categories);
    else return this.renderDesktopCategories(categories);
  }

  render() {
    return (
      <div className="row categories-row">
        {this.renderCategories()}
      </div>
    );
  }
}
