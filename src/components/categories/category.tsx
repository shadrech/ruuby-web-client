import * as React from "react";
import { browserHistory } from "react-router";
import * as moment from "moment";
// import * as Modal from "react-modal";
// import { RuubyEmblem, ArrowThin } from "../sub/svgs";
import {SearchResultsQueryParams} from "../search/search-container";
import {constructUrl} from "../../routes";
import {ApiCategory} from "../../api/categories";
import {CategoryInfo} from "../../reducers/categories/reducer";

interface CategoryProps {
  categories: ApiCategory[];
  category: ApiCategory;
  info: CategoryInfo;
  postcode: string;

  setSelectedCategory: (cat: string) => void;
}
interface CategoryState {
  activeCategory: string;
  isOpen: boolean;
}

export default class Category extends React.Component<CategoryProps, CategoryState> {
  constructor(props: CategoryProps) {
    super(props);
    this.state = {
      activeCategory: props.category.urn,
      isOpen: false
    };
  }

  handleDisplayInfo = (event) => {
    event.preventDefault();
    this.setState({isOpen: true});
  }
  handleHideInfo = () => {
    this.setState({isOpen: false});
  }

  handleTreatmentSwitch = (direction: string) => {
    const urns = this.props.categories.map(cat => cat.urn);
    const idx = urns.findIndex(urn => urn === this.state.activeCategory);

    switch (direction) {
      case "next":
        if (idx === (urns.length - 1)) {
          return urns[0];
        } else {
          return urns[idx + 1];
        }
      case "previous":
        if (idx === 0) {
          return urns[urns.length - 1];
        } else {
          return urns[idx - 1];
        }
    }
  }

  handlePrevTreatment = () => {
    this.setState({activeCategory: this.handleTreatmentSwitch("previous")});
  }

  handleNextTreatment = () => {
    this.setState({activeCategory: this.handleTreatmentSwitch("next")});
  }

  navigateToCategory = (category: string): void => {
    const params: SearchResultsQueryParams = {
      category,
      postcode: this.props.postcode,
      slot: "All-Day",
      date: moment().format("YYYY-MM-DD"),
    };
    const url = `${constructUrl("search")}/${params.category}/${params.date}/${params.slot}/${params.postcode}`;

    this.props.setSelectedCategory(category);

    browserHistory.push(url);
  }

  render() {
    const cat = this.props.category;
    // const {info} = this.props;
    // const modalStyle = {
    //   content: {
    //     height: "70vh",
    //     left: "50%",
    //     transform: "translateX(-50%)",
    //     border: "10px solid #bdc1c9"
    //   }
    // };

    return (
      <div className={`col-xs-5col col-md-5col col-sm-5col ${cat.code.toLowerCase()}`}>
        <div className="category" onClick={() => this.navigateToCategory(cat.name)}>
          <img className={cat.code.toLowerCase()} src={require(`../../assets/imgs/${cat.code.toLowerCase()}@2x.png`)} alt={cat.name + " icon"} />
          <h5>{cat.name}</h5>
        </div>

        {/* <Modal
          isOpen={this.state.isOpen}
          contentLabel="Category Info"
          style={modalStyle} >
          <div className="close" onClick={this.handleHideInfo}>X</div>
          <div className="header">
            <div className="prev-arrow" onClick={this.handlePrevTreatment}><ArrowThin /></div>
            <h3>{`Ruuby ${info.name}`}</h3>
            <div className="next-arrow" onClick={this.handleNextTreatment}><ArrowThin /></div>
          </div>
          <div className="logo">
            <RuubyEmblem />
          </div>
          <p className="description">
            {info.description}
          </p>
          <div className="featured">
            <h6>Our Featured Treatment</h6>
            {info.featured.map((f, i: number) => (
              <div key={i} className="feature">
                <Link to={`/provider/${info.urn}?open=${f.treatment.id}`}>
                  {`${f.treatment.name}, book ${f.name}`}
                </Link>
                <p className="description">
                  {f.treatment.description}
                </p>
              </div>
            ))}
          </div>
        </Modal> */}
      </div>
    );
  }
}
