export default {
  "urn:ruuby:category:5": {
    urn: "urn:ruuby:category:5",
    name: "Nails",
    description: "Curabitur at lacus ac velit ornare lobortis. Duis leo. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Curabitur blandit mollis lacus.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:1": {
    urn: "urn:ruuby:category:1",
    name: "Hair",
    description: "Morbi mollis tellus ac sapien. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros..",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:18": {
    urn: "urn:ruuby:category:18",
    name: "Waxing",
    description: "Quisque rutrum. Curabitur a felis in nunc fringilla tristique. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:26": {
    urn: "urn:ruuby:category:26",
    name: "Eyes",
    description: "Praesent nonummy mi in odio.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:30": {
    urn: "urn:ruuby:category:30",
    name: "Tanning",
    description: "Book London's best beauty therapists to you door, for five star manicures, blowdries, massages and more... Book London's best beauty therapists to you.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:22": {
    urn: "urn:ruuby:category:22",
    name: "Makeup",
    description: "Hellow world Book London's best beauty therapists to you.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:9": {
    urn: "urn:ruuby:category:9",
    name: "Wellness",
    description: "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:14": {
    urn: "urn:ruuby:category:14",
    name: "Massage",
    description: "In ac felis quis tortor malesuada pretium. Praesent egestas neque eu enim.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:33": {
    urn: "urn:ruuby:category:33",
    name: "Facial",
    description: "Book London's best beauty therapists to you door, for five star manicures, blowdries, massages and more... Book London's best beauty therapists to you.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:41": {
    urn: "urn:ruuby:category:41",
    name: "Ruuby Men",
    description: "Book London's best beauty therapists to you door, for five star manicures, blowdries, massages and more... Book London's best beauty therapists to you.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  },
  "urn:ruuby:category:42": {
    urn: "urn:ruuby:category:42",
    name: "Ruuby Men",
    description: "Book London's best beauty therapists to you door, for five star manicures, blowdries, massages and more... Book London's best beauty therapists to you.",
    featured: [{
      id: "858926768",
      name: "No. 6 Mortimor Street",
      treatment: {
        id: 77,
        name: "Shellac Spa Manicure",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua.",
      }
    }, {
      id: "218528732-1",
      name: "Lashaun",
      treatment: {
        id: 91,
        name: "Lines Nail Art",
        description: "We love this treatment. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore magna aliqua."
      }
    }]
  }
};
