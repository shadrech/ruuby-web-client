import * as React from "react";
import { bindActionCreators } from "redux";
import { connect, Dispatch } from "react-redux";
import Categories from "./categories";
import { fetchCategories } from "../../reducers/categories/actions";
import {ApiCategory} from "../../api/categories";
import {CategoryInfos} from "../../reducers/categories/reducer";
import { actionCreators } from "../../reducers/booking/actions";
import { setMetadata } from "../../utils/meta-controller/index";

interface CategoriesContainerProps {
  isBusy: boolean;
  categories: ApiCategory[];
  categoryInfos: CategoryInfos;
  postcode: string;

  fetchCategories: () => void;
  setSelectedCategory: (cat: string) => void;
}

class CategoriesContainer extends React.Component<CategoriesContainerProps, null> {
  componentWillMount() {
    setMetadata("category");
  }

  componentDidMount() {
    if (!Array.isArray(this.props.categories)) this.props.fetchCategories();
  }

  render() {
    return (
      <div className="container-fluid ruuby-categories">
        <div id="top-section">
          <div className="row justify-content-center">
            <div className="col-xs-12 col-md-8">
              <h1>Select a treatment</h1>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-5 col-sm-12">
              <h2 className="header-text">Book London's best beauty therapists to your door, for five star manicures, blowdries, massages and more...</h2>
            </div>
          </div>
        </div>
        <Categories
          categories={this.props.categories}
          categoryInfos={this.props.categoryInfos}
          postcode={this.props.postcode}
          setSelectedCategory={this.props.setSelectedCategory} />
      </div>
    );
  }
}

function mapStateToProps(state: any, ownProps: any) {
  return {
    categories: state.categoriesState.get("items"),
    categoryInfos: state.categoriesState.get("info").toJS(),
    isBusy: state.categoriesState.get("isBusy"),
    postcode: ownProps.location.query["postcode"]
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    fetchCategories,
    setSelectedCategory: actionCreators.setSelectedCategory
  }, dispatch) as any;
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesContainer);
