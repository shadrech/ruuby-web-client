import * as React from "react";

interface NotesProps {
  value: string;
  onChange: (notes: string) => void;
}

export class Notes extends React.Component<NotesProps, {}> {
  render() {
    return(
      <textarea className="checkout-notes-textarea" value={this.props.value} onChange={event => this.props.onChange(event.target.value)} />
    );
  }
}
