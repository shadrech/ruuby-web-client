import * as React from "react";

import {LoginForm} from "./login-form";
import RegisterForm from "./register-form";

const LOGIN_TAB = "login";
const REGISTER_TAB = "register";

interface LoginRegisterProps {
  isBusy: boolean;
  error: string;
  code: string;

  onLoginSubmit: (email: string, password: string) => void;
  onRegisterSubmit: (registration: any, code: string) => void;
  handleCodeChange: (code: string) => void;
}

interface LoginRegisterState {
  selectedTab?: string;
}

export class LoginRegister extends React.Component<LoginRegisterProps, LoginRegisterState> {
  state: LoginRegisterState = {
    selectedTab: LOGIN_TAB
  };

  changeTab(tabId: string) {
    this.setState({selectedTab: tabId});
  }

  isLoginTabSelected() {
    return this.state.selectedTab === LOGIN_TAB;
  }

  isRegisterTabSelected() {
    return this.state.selectedTab === REGISTER_TAB;
  }

  renderTab(id: string) {
    let label = "";
    let direction = "";

    switch (id) {
      case LOGIN_TAB:
        label = "Log in";
        direction = "left";
        break;

      case REGISTER_TAB:
        label = "Register";
        direction = "right";
    }

    const classes = ["tab-panel-tab"];
    if (this.state.selectedTab !== id) classes.push(`tab-panel-tab-unselected-${direction}`);

    return (
      <div className={classes.join(" ")} onClick={() => this.changeTab(id)}>
        {label}
      </div>
    );
  }

  render() {
    return (
      <div className="tab-panel login-register">
        <div className="tab-panel-tabs">
          { this.renderTab(LOGIN_TAB) }
          { this.renderTab(REGISTER_TAB) }
        </div>

        <div className="home-search-form">
          {this.isLoginTabSelected() && <LoginForm onSubmit={this.props.onLoginSubmit} error={this.props.error} isLoggingIn={this.props.isBusy} />}
          {this.isRegisterTabSelected() && <RegisterForm onSubmit={this.props.onRegisterSubmit} onCodeChange={this.props.handleCodeChange} code={this.props.code} />}
        </div>
      </div>
    );
  }
}
