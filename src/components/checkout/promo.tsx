import * as React from "react";
import {ApiPromo} from "../../api/booking";
import LineLoader from "../sub/line-loader";

interface PromoProps {
  isBusy: boolean;
  promo: ApiPromo;
  error: any;

  onChange: (code: string) => void;
  onApply: (code: string) => void;
  onRemove: () => void;
  clearPromo: () => void;
}

export class Promo extends React.Component<PromoProps, {}> {
  componentWillUnmount() {
    this.props.clearPromo();
  }

  handleOnChange = evt => {
    this.props.onChange(evt.target.value);
  }

  renderInputComponents() {
    const { promo, isBusy } = this.props;
    return(
      <div>
        <input type="text" className="checkout-promo-input" value={promo.code} onChange={this.handleOnChange} placeholder="PROMOTIONAL CODE" />
        <button className={`ruuby-btn-dark redeem-promo-code${isBusy ? " disabled" : ""}`} onClick={() => this.props.onApply(promo.code)}>Apply</button>
        <LineLoader isBusy={isBusy} />
        {this.props.error && <p className="error">{this.props.error}</p>}
      </div>
    );
  }

  render() {
    const { promo } = this.props;
    return(
      <div>
        {!promo.isValid && this.renderInputComponents() }
        {promo.isValid && <p className="checkout-promo">SUCCESS! YOUR PROMOTION HAS BEEN APPLIED.</p> }
      </div>
    );
  }
}
