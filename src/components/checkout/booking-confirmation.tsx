import * as React from "react";
import { Dispatch } from "redux";
import {connect} from "react-redux";
import {Link} from "react-router";
import {ApiCustomer} from "../../api/customer";

import { actionCreators } from "../../reducers/booking/actions";
import { clearWholeBasket } from "../../reducers/basket/actions";

interface BookingConfirmationProps {
  customer: ApiCustomer;
  dispatch: Dispatch<{}>;
}

class BookingConfirmationComponent extends React.Component<BookingConfirmationProps, {}> {
  componentDidMount() {
    this.props.dispatch(actionCreators.clearBooking());
    this.props.dispatch(clearWholeBasket());
  }

  render() {
    return (
      <section className="hp-section-four little-book-page">
        <div className="container tb-padding-container lr-padding-container">
          <h2>You're all booked!</h2>
          <div className="booking-salon-info">
            <p>Dear {this.props.customer.firstName},</p>
            <p>We hope you are looking forward to your appointment!</p>
            <p>Any questions? Email <a href="mailto:support@ruuby.com">support@ruuby.com</a> and we'll be happy to help.</p>

            <div className="btn-wrapper">
              <Link to="/" className="checkout-submit ruuby-btn-dark make-another-appointment-button">Make another appointment</Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    customer: state.customerState.get("customer").toJS()
  };
};

export default connect(mapStateToProps)(BookingConfirmationComponent as any);
