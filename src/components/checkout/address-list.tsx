import * as React from "react";
import {ApiAddress} from "../../api/address";

interface Props {
  addresses: ApiAddress[];
  onSelect: (address: ApiAddress) => void;
}

export class AddressList extends React.Component<Props, {}> {
  render() {
    return(
      <div>
        {this.props.addresses.map(address => (
          <div className="payment-row" key={address.urn}>
            <p className="payment-row-text">{address.address1} {address.postcode}</p>
            <div className="use-address-btn ruuby-btn-dark" onClick={() => this.props.onSelect(address)}>select</div>
          </div>
        ))}
      </div>
    );
  }
}
