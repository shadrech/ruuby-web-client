import * as React from "react";
import * as braintree from "braintree-web";
import LineLoader from "../../components/sub/line-loader";

interface Props {
  token: string;
  onSubmit: (nonce: string) => void;
  isBusy: boolean;
  error: string;
  setUpdating: () => void;
  setPaymentError: (error: string) => void;
}

export class CardForm extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
    this.state = {
      error: undefined
    };
  }

  intergrateBrainTree = () => {
    const authorization = this.props.token;
    const submit = document.querySelector("#add-card");
    const form = document.querySelector("#checkout-form");
    const onSubmit = this.props.onSubmit;

    braintree.client.create({
      authorization: authorization
    }, (clientErr: any, clientInstance: any) => {
      if (clientErr) {
        // Handle error in client creation
        console.log("Error creating client", clientErr);
        return;
      }

      braintree.hostedFields.create({
        client: clientInstance,
        styles: {
          "input": {
            "font-size": "15px"
          },
          "input.invalid": {
            "color": "red"
          },
          "input.valid": {
            "color": "black"
          },
        },
        fields: {
          number: {
            selector: "#card-number",
            placeholder: "CARD NUMBER"
          },
          cvv: {
            selector: "#cvv",
            placeholder: "SECURITY NUMBER (CVV)"
          },
          expirationDate: {
            selector: "#expiration-date",
            placeholder: "EXPIRY MM/YYYY"
          }

        }
      }, (hostedFieldsErr: any, hostedFieldsInstance: any) => {
        if (hostedFieldsErr) {
          // Handle error in Hosted Fields creation
          console.log("hostedFieldsErr", hostedFieldsErr);
          return;
        }

        if (submit) submit.removeAttribute("disabled");

        form.addEventListener("submit", (event) => {
          event.preventDefault();
          this.props.setUpdating();
          submit.setAttribute("disabled", "true");

          hostedFieldsInstance.tokenize((tokenizeErr: any, payload: any) => {
            if (submit) submit.removeAttribute("disabled");

            if (tokenizeErr) {
              // Handle error in Hosted Fields tokenization
              console.log("There was an error getting the token", tokenizeErr);
              this.props.setPaymentError(tokenizeErr.message);
              return;
            }

            onSubmit(payload.nonce);

            hostedFieldsInstance.clear("number");
            hostedFieldsInstance.clear("cvv");
            hostedFieldsInstance.clear("expirationDate");
          });
        }, false);
      });
    });

  }

  componentDidMount() {
    this.intergrateBrainTree();
  }

  render() {
    return (
      <div className="content cardForm">
        <form id="checkout-form" action="/transaction-endpoint" method="post">
          <div id="error-message"></div>

          <label htmlFor="card-number">Card Number</label>
          <div className="hosted-field" id="card-number"></div>

          <label htmlFor="cvv">CVV</label>
          <div className="hosted-field" id="cvv"></div>

          <label htmlFor="expiration-date">Expiration Date</label>
          <div className="hosted-field" id="expiration-date"></div>

          <div className="tab-panel-form-field submit-form">

            <input id="add-card" className={`ruuby-btn-dark${this.props.isBusy ? " disabled" : ""}`} type="submit" value="Add Card" />
            <LineLoader isBusy={this.props.isBusy} />
          </div>
        </form>
        {this.props.error && <p className="error-message">{this.props.error}</p>}
      </div>
      );
  }
}

