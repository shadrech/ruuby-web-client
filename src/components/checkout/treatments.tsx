import * as React from "react";
import * as accounting from "accounting";

import {ApiTherapistService} from "../../api/provider";
import {TreatmentInfo} from "./treatments/treatment-info";
import {ApiPromo} from "../../api/booking";

interface TreatmentsProps {
  promo: ApiPromo;
  treatments: ApiTherapistService[];
}

export class Treatments extends React.Component<TreatmentsProps, {}> {
  render() {
    let total = this.props.treatments.reduce((total, treatment) => total + treatment.price, 0);
    const {promo} = this.props;

    let discount = null;
    if (this.props.promo.isValid) {
      discount = (
          <li>
            <div className="basket-item row">
              <div className="pointer">
                <div className="treatment-info-toggle">
                  <p className="total-item-heading">PROMOTION</p>
                  <div className="treatment-price">-{accounting.formatMoney(total - promo.totalAmount, "£")}</div>
                </div>
              </div>
            </div>
          </li>
      );
      total = promo.totalAmount;
    }

    const totalFormatted = accounting.formatMoney(total, "£");

    return(
      <div className="booking-time-container basket-items">
        <ul>
        {this.props.treatments.map((treatment, index: number) => (
          <li key={index}>
            <div className="basket-item row">
              <TreatmentInfo
                name={treatment.name}
                price={treatment.price}
              />
            </div>
          </li>
        ))}

          {discount}
          <li className="basket-total">
            <div className="basket-item row">
                <h4 className="total-item-heading">TOTAL</h4>
                <div className="treatment-price">{totalFormatted}</div>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
