import * as React from "react";

import LineLoader from "../../components/sub/line-loader";

interface LoginProps {
  isLoggingIn: boolean;
  error: string;

  onSubmit: (email: string, password: string) => any;
}

interface LoginState {
  email?: string;
  password?: string;
}

export class LoginForm extends React.Component<LoginProps, LoginState> {
  constructor(props: LoginProps) {
    super(props);

    this.state = {email: "", password: ""};
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event: any) {
    this.setState({email: event.target.value});
  }

  handlePasswordChange(event: any) {
    this.setState({password: event.target.value});
  }

  handleSubmit(event: any) {
    event.preventDefault();
    this.props.onSubmit(this.state.email, this.state.password);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="tab-panel-form">
        {this.props.error && <p className="tab-panel-form-error">{this.props.error}</p>}

        <div className="tab-panel-form-field">
          <label className="tab-panel-form-label">
            Email
          </label>
          <div className="tab-panel-form-input">
            <input type="text" value={this.state.email} onChange={this.handleEmailChange} />
          </div>
        </div>

        <div className="tab-panel-form-field">
          <label className="tab-panel-form-label">
            Password
          </label>
          <div className="tab-panel-form-input">
            <input type="password" value={this.state.password} onChange={this.handlePasswordChange} />
          </div>
        </div>

        <div className="tab-panel-form-field submit-form">
          <input className={`ruuby-btn-dark${this.props.isLoggingIn ? " disabled" : ""}`} type="submit" value="Log In" />
          <LineLoader isBusy={this.props.isLoggingIn} />
          <a href="/forgot-password" target="_blank" className="forgot">Forgot Your Password?</a>
        </div>
      </form>
      );
  }
}
