import * as React from "react";

import LineLoader from "../../components/sub/line-loader";

interface CheckoutButtonProps {
  canCheckout: boolean;
  itemCount: number;
  isBusy: boolean;
  error: string;

  onSubmit: () => void;
}

export class CheckoutButton extends React.Component<CheckoutButtonProps, {}> {
  render() {
    const isDisabled = !this.props.canCheckout || this.props.isBusy;

    return(
      <div className="checkout-submit-container">
        <button className={`checkout-submit ruuby-btn-dark${isDisabled ? " disabled" : ""}`} onClick={this.props.onSubmit}>{this.props.itemCount === 1 ? "Book this treatment" : "Book these treatments"}</button>
        <LineLoader isBusy={this.props.isBusy} />

        {this.props.error && <div className="error">{this.props.error}</div>}
      </div>
    );
  }
}
