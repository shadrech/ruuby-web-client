import * as React from "react";
import { Field, reduxForm, WrappedFieldProps } from "redux-form";
import { connect } from "react-redux";
import LineLoader from "../sub/line-loader";
import TelephoneComponent from "../sub/tele-input-component";

interface Props {
  initialValues: any;
  registerErrors: string[];
  isBusy: boolean;
  error: any;
  code: string;

  handleSubmit: (form: any) => void;
  onCodeChange: (code: string) => void;
}

interface InputFieldProps extends WrappedFieldProps<any> {
  label: string;
  type: string;
  disabled: boolean;
}

class RegisterFormComponent extends React.Component<Props, {}> {
  renderField (data: InputFieldProps) {
    return (
      <div className="tab-panel-form-field">
        <label className="tab-panel-form-label">
          {data.label}
        </label>
        <div className="tab-panel-form-input">
          <input {...data.input as any} placeholder={data.label} type={data.type} disabled={data.disabled} />
          {data.meta.touched && data.meta.error && <span className="error-message">{data.meta.error}</span>}
        </div>
      </div>
    );
  }

  renderTelephoneField = (data: InputFieldProps) => {
    return (
      <div className="tab-panel-form-field">
        <div className="tab-panel-form-input">
          <TelephoneComponent
            value={data.input.value}
            onCodeChange={this.props.onCodeChange}
            onPhoneChange={phone => data.input.onChange(phone as any)}
            code={this.props.code}
            classNames={{label: "tab-panel-form-label"}} />
          {data.meta.touched && data.meta.error && <span className="error-message">{data.meta.error}</span>}
        </div>
      </div>
    );
  }

  renderErrors() {
    const errs = this.props.registerErrors;
    if (!errs) return null;
    if (errs.length === 0) return null;

    return <ul className="register-errors">
      {errs.map((err: string, i: number) => <li key={i} className="error-message">{err}</li>)}
    </ul>;
  }

  render() {
    return (
      <div id="registerForm">
        <form onSubmit={this.props.handleSubmit} className="tab-panel-form">
          <Field name="firstName" component={this.renderField} type="text" label="First name"/>
          <Field name="lastName" component={this.renderField} type="text" label="Last name"/>
          <Field name="email" component={this.renderField} type="text" label="Email"/>
          <Field name="phone" component={this.renderTelephoneField} type="tel" label="Mobile number"/>
          <Field name="password" component={this.renderField} type="password" label="Password"/>
          <Field name="passwordConfirm" component={this.renderField} type="password" label="Confirm password"/>

          <div className="tab-panel-form-field">
            <button className={`ruuby-btn-dark${this.props.isBusy ? " disabled" : ""}`} type="submit">Submit</button>
            <LineLoader isBusy={this.props.isBusy} />
          </div>
        </form>
        {this.renderErrors()}
        {this.props.error && <p className="error-message">{this.props.error}</p>}
      </div>
    );
  }
}

interface RegisterFormValues {
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  password?: string;
  passwordConfirm?: string;
}

function validate (values: RegisterFormValues) {
  const errors: RegisterFormValues = {};

  if (!values.firstName) errors.firstName = "Required";
  if (!values.lastName) errors.lastName = "Required";
  if (!values.email) errors.email = "Required";
  if (!values.phone) errors.phone = "Required";
  if (!values.password) errors.password = "Required";
  if (!values.passwordConfirm) errors.passwordConfirm = "Required";

  if (!errors.password && ((values.password.length < 8) || (values.password.length > 15))) {
    errors.password = "Password should be 8-15 characters in length";
  }

  if (!errors.password && !errors.passwordConfirm && (values.passwordConfirm !== values.password)) {
    errors.passwordConfirm = "Password does not match";
  }

  return errors;
}

const mapStateToProps = (state: any) => ({
  error: state.customerState.get("error"),
  isBusy: state.customerState.get("isBusy")
});

const RegisterForm = connect(mapStateToProps)(RegisterFormComponent);

export default reduxForm({
  form: "register",
  validate
})(RegisterForm) as any;
