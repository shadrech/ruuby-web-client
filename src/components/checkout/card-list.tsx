import * as React from "react";
import {ApiCard} from "../../api/card";
import LineLoader from "../../components/sub/line-loader";

interface CardListProps {
  cards: ApiCard[];
  isBusy: boolean;
  onSelect: (card: ApiCard) => void;
}

export class CardList extends React.Component<CardListProps, {}> {
  render() {
    return(
      <div>
        <LineLoader isBusy={this.props.isBusy} />
        {this.props.cards.map((card, index: number) => (
          <div className="payment-row" key={index}>
            <p className="payment-row-text">{card.cardType} ...{card.last4}  Expires {card.expirationMonth}/{card.expirationYear}</p>
            <div className="use-address-btn ruuby-btn-dark" onClick={() => this.props.onSelect(card)}>select</div>
          </div>
        ))}
      </div>
    );
  }
}
