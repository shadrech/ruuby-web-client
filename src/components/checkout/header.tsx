import * as React from "react";
import * as moment from "moment";
import {ApiCard} from "../../api/card";
import {ApiAddress} from "../../api/address";
import {ApiSalonAddress} from "../../api/provider";

export interface HeaderProps {
  salonName: string;
  isMobile: boolean;
  timeStarts: moment.Moment;
  timeEnds: moment.Moment;
  address?: ApiAddress | ApiSalonAddress;
  card?: ApiCard;

  onChangeAddress?: () => void;
  onChangeCard?: () => void;
}

export class Header extends React.Component<HeaderProps, {}> {
  render() {
    const address = this.props.address ? (
      <div className="booking-summary-address">
        {this.props.address.address1} {this.props.address.postcode}
        &nbsp;{this.props.isMobile && <span className="booking-summary-change" onClick={this.props.onChangeAddress}>Change</span>}
      </div>
    ) : null;

    const card = this.props.card ? (
      <div className="booking-summary-card">
        {this.props.card.cardType} ...{this.props.card.last4}  Expires {this.props.card.expirationMonth}/{this.props.card.expirationYear}
        &nbsp;<span className="booking-summary-change" onClick={this.props.onChangeCard}>Change</span>
      </div>
    ) : null;

    return(
      <div className="booking-summary">
        <div className="booking-summary-salon-name">{this.props.salonName}</div>
          <div className="booking-summary-date-time">
            {this.props.timeStarts.format("MMM DD")} -
            {" " + this.props.timeStarts.format("LT")}-
            {this.props.timeEnds.format("LT")}
          </div>
          {address}
          {card}
      </div>
    );
  }
}
