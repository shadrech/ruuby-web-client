import * as React from "react";
import {connect} from "react-redux";
import {withRouter, browserHistory} from "react-router";
import {Dispatch, bindActionCreators} from "redux";
import * as moment from "moment";
import * as Cookies from "js-cookie";

import {Header, HeaderProps} from "./header";
import {Treatments} from "./treatments";
import {LoginRegister} from "./login-register";
import {AddressList} from "./address-list";
import AddressForm from "./address-form";
import {CardList} from "./card-list";
import {CardForm} from "./card-form";
import {Notes} from "./notes";
import {CheckoutButton} from "./checkout-button";
import {Promo} from "./promo";

import {
  LoginParams,
  submitLoginForm,
  fetchCustomer,
  updateCustomerAttempt,
  submitRegisterForm,
  RegisterParams
} from "../../reducers/customer/actions";
import {
  fetchBraintreeeToken,
  createCard,
  openNewCardForm,
  closeNewCardForm,
  setPaymentBusyness,
  setPaymentError
} from "../../reducers/payments/actions";
import {
  openNewAddressForm,
  createAddress,
} from "../../reducers/addresses/actions";
import {State as AddressState} from "../../reducers/addresses/reducer";
import {
  createBooking,
  verifyPromo,
  actionCreators
} from "../../reducers/booking/actions";
import {State as BookingState} from "../../reducers/booking/reducer";
import {
  ApiCard,
} from "../../api/card";
import {
  ApiAddress,
} from "../../api/address";
// import {ApiPromo} from "../../api/booking";
import {State as UserState} from "../../reducers/customer/reducer";
import {State as PaymentState} from "../../reducers/payments/reducer";
import {
  fetchProvider
} from "../../reducers/providers/actions";
import {
  ApiProvider,
  ApiTherapistService
} from "../../api/provider";

import {
  findApiProvider,
} from "../../utils";
import { setMetadata } from "../../utils/meta-controller/index";

interface RouteParams {
  urn: string;
}

interface CheckoutProps {
  params: RouteParams;
  basket: ApiTherapistService[];
  providers: ApiProvider[];
  bookingState: BookingState;
  addressesState: AddressState;
  userState: UserState;
  paymentState: PaymentState;

  submitLoginForm: (data: LoginParams, redirect?: boolean) => void;
  submitRegisterForm: (data: RegisterParams, redirect?: boolean) => void;
  selectAddress: (address: ApiAddress) => void;
  addAddress: (address: any) => void;
  createCard: (nonce: string) => Promise<any>;
  setPaymentError: (error: string) => void;
  selectCard: (card: ApiCard) => void;
  createBooking: (providerUrn: string) => void;
  setSelectedNotes: (notes: string) => void;
  setPromoCode: (code: string) => void;
  verifyPromo: (code: string, urn: string) => void;
  removeSelectedAddress: () => void;
  removeSelectedCard: () => void;
  clearPromoCode: () => void;
  fetchProvider: (providerUrn: string) => void;
  fetchBraintreeeToken: () => void;
  fetchCustomer: () => void;
  openNewCardForm: () => void;
  closeNewCardForm: () => void;
  openNewAddressForm: () => void;
  updateCustomerAttempt: () => void;
  createAddress: () => void;
  setPaymentBusyness: () => void;
}

interface State {
  code: string;
}

class CheckoutComponent extends React.Component<CheckoutProps, State> {
  state: State = {
    code: "+44"
  };

  componentWillMount() {
    setMetadata("checkout");
  }

  componentDidMount() {
    if (!Object.keys(this.props.basket).length)
      browserHistory.push(`/provider/${this.props.params.urn}`);

    if (Cookies.get("token") && !this.props.userState.customer) this.props.fetchCustomer();
  }

  componentWillUpdate(nextProps) {
    if (Object.keys(nextProps.basket).length === 0)
      browserHistory.push(`/provider/${nextProps.params.urn}`);
  }

  getProvider = () => {
    return findApiProvider(this.props.providers, this.props.params.urn);
  }

  timeEnds = () => {
    const duration = this.props.basket[this.props.params.urn].reduce( (sum: number, treatment: any) => sum + treatment.duration, 0);
    return moment(this.props.bookingState.selectedTime).clone().add(duration, "minutes");
  }

  isLoggedIn = (): boolean => {
    return !!this.props.userState.customer;
  }

  handleLoginSubmit = (email: string, password: string) => {
    this.props.submitLoginForm({ email, password });
  }

  getTotal = () => {
    if (this.props.bookingState.selectedPromo.isValid)
      return this.props.bookingState.selectedPromo.totalAmount;
    else
      return this.props.basket[this.props.params.urn].reduce((sum: number, item: any) => sum + item.price.value, 0);
  }

  handleChangeAddress = () => {
    this.props.removeSelectedAddress();
  }

  handleChangeCard = () => {
    this.props.removeSelectedCard();
  }

  renderAddressComponents = () => {
    if (this.canShowCardsComponents()) return null;

    let { addresses } = this.props.addressesState;

    const newAddressButton = this.props.addressesState.newAddressOpen ? null : (
      <div className="payment-row">
        <div className="add-new-card-btn ruuby-btn-dark" onClick={this.props.openNewAddressForm}>+ add new address</div>
      </div>
    );

    return (
      <div className="row">
        <div className="c-xsmall-12 c-small-12 c-medium-10 c-large-10 offset-medium-1 offset-large-1">
          <div className="booking-date-time-select">
            <h2 className="booking-day-select-header">
              Select address
            </h2>
            <div className="payment-content">
              <AddressList
                onSelect={this.props.selectAddress}
                addresses={addresses} />

              {newAddressButton}
            </div>

            {this.props.addressesState.newAddressOpen &&
              <AddressForm onSubmit={this.props.createAddress} />}
          </div>
        </div>
      </div>
    );
  }

  renderCardsComponents = () => {
    if (this.canShowCheckoutComponents()) return null;

    const cards = this.props.paymentState.cards;

    const newCardButton = this.props.paymentState.newCardOpen ? null : (
      <div className="payment-row">
        <div className="add-new-card-btn ruuby-btn-dark" onClick={this.props.openNewCardForm}>+ add new card</div>
      </div>
    );

    return (
      <div className="row">
        <div className="c-xsmall-12 c-small-12 c-medium-10 c-large-10 offset-medium-1 offset-large-1">
          <h2 className="booking-day-select-header">
            Payment
          </h2>
            <div className="payment-content">
              <CardList
                cards={cards}
                onSelect={this.props.selectCard}
                isBusy={this.props.userState.isBusy} />
              {newCardButton}
            </div>

            {this.props.paymentState.newCardOpen &&
              <CardForm
                token={this.props.paymentState.paymentGateway.token}
                onSubmit={this.props.createCard}
                isBusy={this.props.paymentState.isBusy}
                error={this.props.paymentState.error}
                setUpdating={this.props.setPaymentBusyness}
                setPaymentError={this.props.setPaymentError} />
            }
        </div>
      </div>
    );
  }

  renderNotesComponents = () => {
    return (
      <div className="row">
        <div className="c-xsmall-12 c-small-12 c-medium-10 c-large-10 offset-medium-1 offset-large-1">
          <div className="checkout-notes-container">
              <div className="c-xsmall-12 c-small-12 c-medium-12 c-large-12">
                  <h2 className="booking-day-select-header">
                    Notes for booking?
                  </h2>
                  <div className="booking-form-group">
                    <Notes value={this.props.bookingState.selectedNotes} onChange={this.props.setSelectedNotes} />
                  </div>
              </div>
          </div>
        </div>
      </div>
    );
  }

  canCheckout = (): boolean => {
    const provider = this.getProvider();

    if (this.props.basket[this.props.params.urn].length === 0 || !this.isLoggedIn() || this.props.userState.isBusy) {
      return false;
    }

    if (provider.salon.isMobile) {
      return (typeof this.props.bookingState.selectedCard.token === "string") && (typeof this.props.bookingState.selectedAddress.urn === "string");
    } else {
      return this.props.bookingState.selectedCard.token !== null;
    }
  }

  renderCheckoutComponents = () => {
    return (
      <div className={"content"}>
        <CheckoutButton
          canCheckout={this.canCheckout()}
          onSubmit={() => this.props.createBooking(this.props.params.urn)}
          isBusy={this.props.bookingState.isBusy}
          error={this.props.bookingState.error}
          itemCount={this.props.basket.length} />
      </div>
    );
  }

  // code must be given a value or this will not be a controlled form
  renderPromoComponents = () => {
    return (
      <div className="row">
        <div className="c-xsmall-12 c-small-12 c-medium-10 c-large-10 offset-medium-1 offset-large-1">
          <div className="checkout-promo-container">
            <div className="c-xsmall-12 c-small-12 c-medium-12 c-large-12">
              <h2 className="booking-day-select-header">
                Promotional code
              </h2>

              <div className="booking-form-group">
                <Promo
                  promo={this.props.bookingState.selectedPromo}
                  onApply={code => this.props.verifyPromo(code, this.props.params.urn)}
                  onRemove={this.props.clearPromoCode}
                  onChange={this.props.setPromoCode}
                  error={this.props.bookingState.promoError}
                  clearPromo={this.props.clearPromoCode}
                  isBusy={this.props.bookingState.promoIsBusy} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  canShowAddressComponents = () => {
    const canShow = this.isLoggedIn() && this.getProvider().salon.isMobile;
    return canShow;
  }

  canShowCardsComponents = () => {
    const provider = this.getProvider();
    const canShow = (this.isLoggedIn() && !provider.salon.isMobile) || (this.isLoggedIn() && provider.salon.isMobile && this.props.bookingState.selectedAddress && (typeof this.props.bookingState.selectedAddress.urn === "string"));
    return canShow;
  }

  canShowCheckoutComponents = () => {
    return this.canShowCardsComponents() && this.props.bookingState.selectedCard && (typeof this.props.bookingState.selectedCard.token === "string");
  }

  submitRegisterForm = (data: RegisterParams) => {
    data.phone = this.state.code + data.phone;
    this.props.submitRegisterForm(data);
  }

  render() {
    const provider = this.getProvider();

    // when user refreshes page display nothing until props come back
    if (!provider)
      return <div></div>;

    const isMobile = provider.salon.isMobile;
    const headerProps: HeaderProps = {
      salonName: isMobile ? provider.therapist.name : provider.salon.name,
      isMobile,
      timeStarts: moment(this.props.bookingState.selectedTime),
      timeEnds: this.timeEnds(),
    };

    if (this.canShowCardsComponents() && isMobile) {
      headerProps.address = this.props.addressesState.addresses.filter(address => address.urn === this.props.bookingState.selectedAddress.urn)[0];
      headerProps.onChangeAddress = this.handleChangeAddress;
    } else if (!isMobile) {
      headerProps.address = provider.salon.address;
    }

    if (this.canShowCheckoutComponents()) {
      headerProps.card = this.props.paymentState.cards.filter(card => card.token === this.props.bookingState.selectedCard.token)[0];
      headerProps.onChangeCard = this.handleChangeCard;
    }

    return (
      <section className="hp-section-four little-book-page">
        <div className="container tb-padding-container lr-padding-container">
          <h2>Booking</h2>

          <div className={"content"}>
            <Header {...headerProps} />

            <Treatments
              treatments={this.props.basket[this.props.params.urn]}
              promo={this.props.bookingState.selectedPromo} />
          </div>

          {
            !this.isLoggedIn() &&
            <LoginRegister
              onLoginSubmit={this.handleLoginSubmit}
              onRegisterSubmit={this.submitRegisterForm}
              isBusy={this.props.userState.isBusy}
              error={this.props.userState.error}
              handleCodeChange={code => this.setState({code})}
              code={this.state.code} />
          }
          {this.canShowAddressComponents() && this.renderAddressComponents()}
          {this.canShowCardsComponents() && this.renderCardsComponents()}
          {this.canShowCheckoutComponents() && this.renderNotesComponents()}
          {this.canShowCheckoutComponents() && this.renderPromoComponents()}
          {this.canShowCheckoutComponents() && this.renderCheckoutComponents()}
        </div>
      </section>
    );
  }
}

function mapStateToProps (state: any) {
  return {
    providers: state.providerState.get("items").toJS(),
    bookingState: state.bookingState,
    addressesState: state.addressesState.toJS(),
    basket: state.basketState.get("items").toJS(),
    userState: state.customerState.toJS(),
    paymentState: state.paymentState.toJS()
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    selectAddress: actionCreators.selectAddress,
    createAddress,
    createCard,
    selectCard: actionCreators.selectCard,
    createBooking,
    setSelectedNotes: actionCreators.setSelectedNotes,
    setPromoCode: actionCreators.setPromoCode,
    verifyPromo,
    removeSelectedAddress: actionCreators.removeSelectedAddress,
    removeSelectedCard: actionCreators.removeSelectedCard,
    submitRegisterForm,
    clearPromoCode: actionCreators.clearPromoCode,
    fetchProvider,
    submitLoginForm,
    fetchBraintreeeToken,
    fetchCustomer,
    openNewCardForm,
    closeNewCardForm,
    openNewAddressForm,
    updateCustomerAttempt,
    setPaymentBusyness,
    setPaymentError
  }, dispatch) as any;
}

const CheckoutWithRouter = withRouter(CheckoutComponent);

export const Checkout = connect(mapStateToProps, mapDispatchToProps)(CheckoutWithRouter);
