import * as React from "react";
import { Field, reduxForm, WrappedFieldProps } from "redux-form";
import { connect } from "react-redux";
import LineLoader from "../sub/line-loader";

interface AddressFormProps {
  isBusy: boolean;
  error: string;

  handleSubmit: (form: any) => void;
}

interface InputFieldProps extends WrappedFieldProps<any> {
  label: string;
  type: string;
  disabled: boolean;
}

class AddressFormComponent extends React.Component<AddressFormProps, {}> {
  renderField (data: InputFieldProps) {
    return (
      <div className="c-xsmall-12 c-small-12 c-medium-12 c-large-12">
        <div className="booking-form-group">
          <input {...data.input as any} placeholder={data.label.toUpperCase()} type={data.type} disabled={data.disabled}/>
          {data.meta.touched && data.meta.error && <span className="error">{data.meta.error}</span>}
        </div>
      </div>
    );
  }

  render() {
    const { handleSubmit, isBusy, error } = this.props;

    return (
      <div className="booking-new-card-box">
        <form onSubmit={handleSubmit}>
          <Field name="address1" component={this.renderField} type="text" label="Address line 1"/>
          <Field name="address2" component={this.renderField} type="text" label="Address line 2"/>
          <Field name="postcode" component={this.renderField} type="text" label="Postcode"/>

          <button type="submit" className={`checkout-submit ruuby-btn-dark${isBusy ? " disabled" : ""}`}>Submit</button>
          <LineLoader isBusy={isBusy} />
          {error && <div className="error">{error}</div>}
        </form>
      </div>
    );
  }
}

function validate (values: any) {
  const errors: any = {};

  if (!values.address1 || values.address1.trim() === "") {
    errors.address1 = "Required";
  }

  return errors;
}

function mapStateToProps(state: any, _ownProps: any) {
  return {
    error: state.addressesState.get("error"),
    isBusy: state.addressesState.get("isBusy")
  };
}

const AddressForm = connect(mapStateToProps)(AddressFormComponent);

export default reduxForm({form: "address", validate})(AddressForm);
