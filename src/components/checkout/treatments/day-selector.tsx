import * as React from "react";

interface DaySelectorProps {
  key: string;
  availabilityItem: any;
  isActive: boolean;

  onDayClick: (event: React.MouseEvent<HTMLDivElement>) => void;
}

interface DayAttributes {
  key: string;
  className?: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement>) => void;
}

export class DaySelector extends React.Component<DaySelectorProps, {}> {
  render() {
    const availabilityItem = this.props.availabilityItem;

    const attributes: DayAttributes = {
      key: availabilityItem.day.format(),
    };

    const classes = ["day"];
    if (availabilityItem.availabilitySlots.length > 0) {
      classes.push("day-available");
      attributes.onClick = this.props.onDayClick;
    }

    if (this.props.isActive) classes.push("day-selected");

    attributes.className = classes.join(" ");

    return (
      <div {...attributes}>
        <span className="day-slot-day-name">{availabilityItem.day.format("ddd")}</span>
        <span className="day-slot-date">{availabilityItem.day.format("D")}</span>
        <span className="day-slot-month">{availabilityItem.day.format("MMM")}</span>
      </div>
    );
  }
}
