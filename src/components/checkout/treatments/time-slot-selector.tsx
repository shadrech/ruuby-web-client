import * as React from "react";
import {DaySelector} from "./day-selector";

interface TimeSlotSelectorProps {
  availability: any[];
  selectedStartTime: any;

  onDaySelected: (day: any) => void;
  onSlotSelected: (slot: any) => void;
}

interface TimeSlotSelectorState {
  activeDay: any;
}

export class TimeSlotSelector extends React.Component<TimeSlotSelectorProps, TimeSlotSelectorState> {
  constructor(props: TimeSlotSelectorProps) {
    super(props);

    this.state = {activeDay: null};
  }

  setActiveDay(event: React.MouseEvent<any>, day: any) {
    event.stopPropagation();
    this.setState({
      activeDay: day
    });

    this.props.onDaySelected(day);
  }

  // isActiveAvailability: function(availabilityItem) {
  //   if(this.state.activeDay === null) {
  //     return false;
  //   }
  //   return this.state.activeDay === availabilityItem.day;
  // },

  render() {
    return (
      <div>
        { this.props.availability.map( availabilityItem =>
          <DaySelector
            key={availabilityItem.day.format()}
            availabilityItem={availabilityItem}
            isActive={ this.state.activeDay ===  availabilityItem.day }
            onDayClick={(event: React.MouseEvent<HTMLDivElement>) => this.setActiveDay(event, availabilityItem.day)}
          />)
        }
      </div>
    );
  }

}
