import * as React from "react";

import {DaySelector} from "./day-selector";
import {TimeSelector} from "./time-selector";
import LineLoader from "../../sub/line-loader";
import {TreatmentInfo} from "./treatment-info";

interface TreatmentProps {
  treatment: any;
  isOpen: boolean;
  availability: any;
  selectedStartTime: any;
  isBusy: boolean;
  canAddTreatmentToBasket: boolean;

  onTreatmentClicked: () => void;
  onAddTreatmentToBasket: (treatment: any) => void;
  onSlotSelected: (slot: any) => void;
  onDaySelected: (day: any) => void;
}

interface TreatmentState {
  activeDay: any;
}

export class Treatment extends React.Component<TreatmentProps, TreatmentState> {
  constructor(props: TreatmentProps) {
    super(props);

    this.state = {activeDay: null};

    this.setActiveDay = this.setActiveDay.bind(this);
    this.onProceedHandler = this.onProceedHandler.bind(this);
  }

  setActiveDay(availabilitySlot: any) {
    this.setState({
      activeDay: availabilitySlot
    });

    this.props.onDaySelected(availabilitySlot.day);
  }

  componentWillReceiveProps(nextProps: TreatmentProps) {
    if (!nextProps.isOpen) this.setState({activeDay: null});
  }

  onProceedHandler() {
    this.props.onAddTreatmentToBasket(this.props.treatment);
  }

  render() {
    let bookingDetails;

    if (this.props.isOpen) {
      let description;

      if (this.props.treatment.description) {
        description = (
          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12">
            <div className="treatment-info">
              <p>
                {this.props.treatment.description}
              </p>
            </div>
          </div>
        );
      };

      bookingDetails = (
        <div>
          {description}

          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12 day-slots">
            { this.props.availability.map((availabilityItem: any) =>
              <DaySelector
                key={availabilityItem.day.format()}
                availabilityItem={availabilityItem}
                isActive={this.state.activeDay && this.state.activeDay.day === availabilityItem.day}
                onDayClick={() => this.setActiveDay(availabilityItem)}
              />
            )}
            <LineLoader isBusy={this.props.isBusy} />
          </div>

          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12 time-slots">
            { this.state.activeDay && this.state.activeDay.availabilitySlots.map((slot: any) =>
              <TimeSelector
                time={slot.start}
                isActive={this.props.selectedStartTime === slot.start}
                onTimeClick={() => this.props.onSlotSelected(slot)}
              />
            )}
          </div>

          {
            this.props.selectedStartTime &&
            <button className="proceed-btn" onClick={this.onProceedHandler}>Proceed</button>
          }
        </div>
      );
    }

    return (
      <div className="row" key={this.props.treatment.id}>
        <TreatmentInfo
          name={this.props.treatment.name}
          price={this.props.treatment.price}
          onActionClick={this.props.onTreatmentClicked}
          actionIndicator="+"
        />

        {bookingDetails}
      </div>
    );
  }
}
