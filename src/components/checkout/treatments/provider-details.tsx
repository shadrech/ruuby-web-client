import * as React from "react";
const Carousel = require('nuka-carousel');
import * as MobileDetect from "mobile-detect";

import {Rating} from "../treatments/rating";

interface ProviderDetailsProps {
  name: string;
  images: string[];
  review: string;
  rating: number;
  address: string;
}

export class ProviderDetails extends React.Component<ProviderDetailsProps, {}> {
  renderCarousel() {
    const md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) {
      return <img src={this.props.images[0]} alt="therapist-image" className="therapists-img-box" />
    }

    const images = this.props.images.map((url: string, i: number) => {
      const style = {
        width: "100%",
        backgroundImage: `url("${url}")`,
        backgroundSize: "cover",
        backgroundPosition: "center",
      };

      return <div key={i} className="therapists-img-box" style={style}></div>;
    });

    return (
        <Carousel dragging={true} autoplay={true} decorators={Carousel.getDefaultProps().decorators}>
          {images}
        </Carousel>
    );
  }

  render() {
    const address = this.props.address ? <p>{this.props.address}</p> : null;

    return(
        <div className="therapists-service salon-service">
          <div className="row">
            <div className="c-large-5 c-medium-5 c-small-12 c-small-12">
              <div className="therapists-text-box">
                <h2>{this.props.name}</h2>

                {address}

                <div className="therapists-text-box-rating">
                  <Rating score={this.props.rating} />
                </div>

                <h4>OVERVIEW</h4>

                <p>
                  {this.props.review}
                </p>
              </div>
            </div>

            <div className="c-large-7 c-medium-7 c-small-12 c-small-12">
              {this.renderCarousel()}
            </div>
          </div>
        </div>
    );
  }
}
