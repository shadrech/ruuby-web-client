import * as React from "react";

interface DaySelectorProps {
  availabilityItem: any;
  selectedStartTime: any;
  isActive: boolean;

  onDayClicked: (evemt: React.MouseEvent<HTMLDivElement>) => void;
  onSlotSelected: (slot: any) => void;
}

export class DaySelector extends React.Component<DaySelectorProps, {}> {
  handleSlotClicked(event: React.MouseEvent<HTMLDivElement>, slot: any) {
    event.stopPropagation();
    this.props.onSlotSelected(slot);
  }

  renderTimes() {
    const availabilityItem = this.props.availabilityItem;

    return availabilityItem.availabilitySlots.map((slot: any) => {
      return (
        <div
          key={slot.start.format()}
          className={slot.start === this.props.selectedStartTime ? "time-slot-selected" : "time-slot" }
          onClick={(event) => this.handleSlotClicked(event, slot)}
        >
          {slot.start.format("HH:mm")}
        </div>
      );
    });
  }

  render() {
    const availabilityItem = this.props.availabilityItem;

    return (
          <div
            onClick={this.props.onDayClicked}
            className={ availabilityItem.availabilitySlots.length === 0 ? "day" : "day day-available" }
          >
            <span className="day-slot-day-name">{availabilityItem.day.format("ddd")}</span>
            <span className="day-slot-date">{availabilityItem.day.format("D")}</span>
            <span className="day-slot-month">{availabilityItem.day.format("MMM")}</span>
            { false && this.props.isActive && this.renderTimes() }
          </div>
    );
  }
}
