import * as React from "react";

interface TreatmentInfoProps {
  name: string;
  price: number;
  actionIndicator?: string;
  onActionClick?: () => void;
}

export class TreatmentInfo extends React.Component<TreatmentInfoProps, {}> {
  renderButton() {
    return (
        <div className="c-large-1 c-medium-2 c-small-2 c-xsmall-3 treatment-info-toggle">
          <div className="treatment-info-btn">
            <span>{this.props.actionIndicator}</span>
          </div>
        </div>
    );
  }

  infoClasses() {
    if (this.props.actionIndicator) {
      return [
        "c-large-11", "c-medium-10", "c-small-10", "c-xsmall-9"
      ];
    }
    else {
      return [];
    }
  }

  render() {
    const classes = this.infoClasses();
    classes.push("treatment-info-toggle");

    return(
        <div onClick={this.props.onActionClick} className="pointer">
          <div className={classes.join(" '")}>
            <p>{this.props.name}</p>
            <div className="treatment-price">{"£" + this.props.price}</div>
          </div>

          {this.props.actionIndicator && this.renderButton()}
        </div>
    );
  }
}
