import * as React from "react";
import * as _ from "lodash";

interface RatingProps {
  score: number;
  label?: string;
}

export const Rating = ({score, label}: RatingProps) => {
  const hearts = _.times(Math.floor(score), index => <span key={index} className="ruuby-rating-heart"></span>);

 const labelTag = label ? <span className="ruuby-rating-label">{label}</span> : null;

  return (
    <div className="ruuby-rating">
      {labelTag}
      {hearts}
    </div>
  );
};
