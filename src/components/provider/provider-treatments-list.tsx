import * as React from "react";
import {
  ApiProvider,
  ApiTherapistService
} from "../../api/provider";
import {TreatmentInfo} from "./provider-treatment-info";
import {changeToCamelCase, recordEvent} from "../../utils";
import { Basket } from ".";

interface Props {
  basket: Basket;
  provider: ApiProvider;
  selectedCategory: string;
  refs: {
    [category: string]: HTMLDivElement;
  };

  addTreatmentToBasket: (urn: string, treatment: ApiTherapistService) => void;
  removeTreatmentFromBasket: (id: string, index: number) => void;
}

export default class TreatmentsList extends React.Component<Props, null> {
  isInBasket = (treatment: ApiTherapistService): boolean => {
    let isIn = false;
    const {urn} = this.props.provider.therapist;
    if (this.props.basket[urn]) {
      const filtered = this.props.basket[urn].filter(t => t.urn === treatment.urn);
      if (filtered.length > 0) isIn = true;
    }
    return isIn;
  }

  addTreatment = (treatment: ApiTherapistService): void => {
    this.props.addTreatmentToBasket(this.props.provider.therapist.urn, treatment);
    recordEvent(["therapistScreen", "addTreatmentToBasket"]);
  }

  removeTreatment = (treatment: ApiTherapistService): void => {
    const i = this.props.basket[this.props.provider.therapist.urn].findIndex(item => item.urn === treatment.urn);
    this.props.removeTreatmentFromBasket(this.props.provider.therapist.urn, i);
    recordEvent(["therapistScreen", "removeTreatmentFromBasket"]);
  }

  renderTreatmentAddList = (treatments: ApiTherapistService[], category: string, i: number): JSX.Element => {
    return (
        <div>
          {treatments.length >= 1 &&
            <div ref={div => this.props.refs[category] = div}>
            <p className="category-name" id={category}> {changeToCamelCase(category)}</p>
              <ul>
                {treatments.map(treatment => {
                  const inBasket = this.isInBasket(treatment);
                  return (
                    <li key={treatment.name + treatment.urn + i} className={`${inBasket ? "inBasket" : ""}`}>
                      <div className="row" key={treatment.urn}>
                        <TreatmentInfo
                            name={treatment.name}
                            price={treatment.price}
                            duration={treatment.duration}
                            onActionClick={() => !inBasket ? this.addTreatment(treatment) : this.removeTreatment(treatment)}
                            isInBasket={inBasket}
                            actionIndicator="+"
                        />
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          }
        </div>
    );
  }

  renderList = (): JSX.Element[] => {
    const {selectedCategory} = this.props;
    const elements = [];

    this.props.provider.therapist.services.forEach(cat => {
      if (cat.category === selectedCategory)
        return elements.unshift(cat);

      elements.push(cat);
    });

    return elements.map((cat, i) => (
      <div key={i}>
        {this.renderTreatmentAddList(cat.services, cat.category, i)}
      </div>
    ));
  }

  render() {
    return (
      <div>
        {this.renderList()}
      </div>
    );
  }
}
