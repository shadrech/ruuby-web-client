import * as React from "react";
import {TreatmentInfo} from "./provider-treatment-info";

interface TreatmentProps {
  treatment: any;
  isOpen: boolean;
  selectedStartTime: any;
  isBusy: boolean;
  canAddTreatmentToBasket: boolean;

  onTreatmentClicked: () => void;
  onAddTreatmentToBasket: (treatment: any) => void;
  onSlotSelected: (slot: any) => void;
  onDaySelected: (day: any) => void;
}

interface TreatmentState {
  activeDay: any;
}

export class Treatment extends React.Component<TreatmentProps, TreatmentState> {
  constructor(props: TreatmentProps) {
    super(props);

    this.state = {activeDay: null};
  }

  render() {
    if (this.props.isOpen) {
      let description;

      if (this.props.treatment.description) {
        description = (
          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12">
            <div className="treatment-info">
              <p>
                {this.props.treatment.description}
              </p>
            </div>
          </div>
        );
      };
    }

    return (
      <div className="row" key={this.props.treatment.id}>
        <TreatmentInfo
          name={this.props.treatment.name}
          price={this.props.treatment.price}
          duration={this.props.treatment.duration}
          onActionClick={this.props.onTreatmentClicked}
          actionIndicator="+"
          isInBasket={false}
        />
      </div>
    );
  }
}
