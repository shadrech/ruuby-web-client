import * as React from "react";
import { Link } from "react-router";
import Icon from "react-fa";
import * as moment from "moment";
import * as $ from "jquery";
import {DaySelector} from "./provider-day-selector";
import {TimeSelector} from "./provider-time-selector";
import {ApiAvailability} from "../../api/provider";

interface Props {
  availability: ApiAvailability[];
  onDaySelected: (stuff: any) => void;
  selectedDate: moment.Moment;
  setSelectedDate: (date: moment.Moment) => void;
  selectedTime: moment.Moment;
  setSelectedTime: (time: moment.Moment) => void;
  activeDay: ApiAvailability;
  urn: string;
  fetchAvailability: (urn: string) => void;
  providerUrn: string;
  isAvailabilityBusy: boolean;
}

interface State {
  notAvailable: boolean;
}

export default class SelectDateTime extends React.Component<Props, State> {
  state: State = {
    notAvailable: false
  };

  componentDidMount() {
    this.props.fetchAvailability(this.props.providerUrn);
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.availability.length < 1 && this.props.availability.length > 0 && this.props.selectedDate) {
      const day = this.props.availability.find(a => a.dateValue.format("YY-MM-DD") === this.props.selectedDate.format("YY-MM-DD"));
      if (day) {
        this.props.onDaySelected(day);
        if ((this.props.selectedTime !== null) && !day.dateSlots.find(d => (d.format("HHmma") === this.props.selectedTime.format("HHmma"))))
          this.setState({notAvailable: true});
      } else
        this.setState({notAvailable: true});
    }
  }

  handleSelectedDateChange = (date: moment.Moment) => {
    this.props.onDaySelected(date);
  }

  handlesDayNext(event: any) {
    event.preventDefault();
    const length = (this.refs.dayScrollContainer as any).clientWidth - 30;
    this.handlesCarousel(".day-scroll-container", length);
  }

  handlesDayPrevious(event: any) {
    event.preventDefault();
    const length = (this.refs.dayScrollContainer as any).clientWidth - 30;
    this.handlesCarousel(".day-scroll-container", -length);
  }

  handlesTimeNext(event: any) {
    event.preventDefault();
    const length = (this.refs.dayScrollContainer as any).clientWidth - 30;
    this.handlesCarousel(".time-scroll-container", length);
  }

  handlesTimePrevious(event: any) {
    event.preventDefault();
    const length = (this.refs.dayScrollContainer as any).clientWidth - 30;
    this.handlesCarousel(".time-scroll-container", -length);
  }

  handlesCarousel(selector, offset) {
    $(selector).animate({
        scrollLeft: $(selector).scrollLeft() + offset
      }, 300
    );
  }

  render() {
    const {selectedDate, activeDay, availability, urn, selectedTime, setSelectedTime, onDaySelected} = this.props;

    if (availability.length !== 0) {
      return (
        <div className="day-time-selector">
          {this.state.notAvailable && !this.props.isAvailabilityBusy && <p className="time-not-available">Oh no! Your therapist doesn’t have enough time to perform the treatments you selected. Please change them, or choose another slot.</p>}
          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12 day-slots">
            <Icon className="control-carousel left-carousel" name="angle-left" onClick={e => this.handlesDayPrevious(e)} />
            <div className="belt-gradient belt-gradient-left"></div>
            <div className="day-scroll-container" ref="dayScrollContainer">
              <div className="day-slots-wrapper">
                {availability.map((availabilityItem, i) =>
                  <DaySelector
                    key={i}
                    availabilityItem={availabilityItem}
                    isActive={(selectedDate && selectedDate.format("YY-MM-DD") === availabilityItem.dateValue.format("YY-MM-DD")) || (activeDay && activeDay.dateValue.format("YY-MM-DD") === availabilityItem.dateValue.format("YY-MM-DD"))}
                    onDayClick={() => onDaySelected(availabilityItem)}
                  />
                )}
              </div>
            </div>
            <div className="belt-gradient belt-gradient-right"></div>
            <Icon className="control-carousel right-carousel" name="angle-right" onClick={e => this.handlesDayNext(e)} />
          </div>

          <div className="c-large-12 c-medium-12 c-small-12 c-xsmall-12 time-slots">
            {(activeDay || selectedDate) &&
              <div>
                <Icon className="control-carousel left-carousel" name="angle-left" onClick={(e) => this.handlesTimePrevious(e)} />
                <div className="belt-gradient belt-gradient-left"></div>
                <div className="time-scroll-container">
                  <div className="time-slots-wrapper">
                    {activeDay && activeDay.dateSlots.map((slot, i) =>
                      <TimeSelector
                        key={i}
                        time={slot}
                        isActive={selectedDate ? selectedDate.isSame(slot) && selectedTime !== null : false}
                        onTimeClick={() => setSelectedTime(slot)} />
                    )}
                  </div>
                </div>
                <div className="belt-gradient belt-gradient-right"></div>
                <Icon className="control-carousel right-carousel" name="angle-right" onClick={e => this.handlesTimeNext(e)} />
              </div>
            }
          </div>
          {activeDay && selectedTime ?
            <Link className="btn-checkout" to={`/${urn}/checkout`}>Continue to Checkout</Link>
          : <div className="btn-checkout disabled">Continue to Checkout</div>}
        </div>
      );
    } else {
      return (
        <div className="no-availability">
          Therapist has no availability at this time
        </div>
      );
    }
  }
}
