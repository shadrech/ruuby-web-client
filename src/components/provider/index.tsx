import * as React from "react";
import * as $ from "jquery";
import {connect} from "react-redux";
import {Dispatch, bindActionCreators} from "redux";
import * as moment from "moment";
import {
  ApiProvider,
  ApiTherapistService,
  ApiAvailability
} from "../../api/provider";
const TimelineMax = require("gsap/src/uncompressed/TimelineMax");

import {
  fetchProvider,
  fetchAvailability,
  clearAvailability
} from "../../reducers/providers/actions";
import {
  addTreatmentToBasket,
  removeTreatmentFromBasket,
  clearBasket,
} from "../../reducers/basket/actions";
import { actionCreators } from "../../reducers/booking/actions";
import {fetchCategories} from "../../reducers/categories/actions";
import {ApiCategory} from "../../api/categories";
import {ProviderDetails} from "./provider-details";
import TreatmentsList from "./provider-treatments-list";
import SelectDateTime from "./select-date-time";
import LineLoader from "../sub/line-loader";
import { Arrow } from "../sub/svgs";
import {
  findApiProvider
} from "../../utils";
import { ModalShell } from "../sub/modal";
import { setTitle, setDescription } from "../../utils/meta-controller/index";
import { browserHistory } from "react-router";

interface RouteParams {
  urn: string;
}

export interface Basket {
  [urn: string]: ApiTherapistService[];
}

interface Props {
  params: RouteParams;
  providers: ApiProvider[];
  selectedDate: moment.Moment;
  availability: ApiAvailability[];
  isBusy: boolean;
  isAvailabilityBusy: boolean;
  basket: Basket;
  activeDay: ApiAvailability;
  categories: ApiCategory[];
  selectedCategory: string;
  setSelectedTime: (time: moment.Moment) => void;
  selectedTime: moment.Moment;
  clearSelectedTime: () => void;

  fetchAvailability: (providerUrn: string) => void;
  setSelectedDate: (date: moment.Moment) => void;
  clearSelectedDate: () => void;
  fetchProvider: (providerUrn: string) => void;
  fetchCategories: () => void;
  removeTreatmentFromBasket: (id: string, treatmentIndex: number) => void;
  addTreatmentToBasket: (urn: string, treatment: ApiTherapistService) => void;
  clearBasket: () => void;
  clearAvailability: () => void;
}

interface State {
  isNavFixed: boolean;
  basketLeft: number;
  toggleBasket: boolean;
  showDates: boolean;
  activeDay: ApiAvailability;
  basketTopPadding: number;
  treatmentsHeight: number;
  isBasketOpen: boolean;
  activeCategory: string;
  beltWidth: number;
}

class ProviderComponent extends React.Component<Props, State> {
  private basketImg: HTMLImageElement;
  private viewBasket: HTMLDivElement;
  private windowInnerHeight: number;
  private treatmentsWrapper: HTMLDivElement;
  private beltWrapper: HTMLDivElement;
  private domCategories: {
    [category: string]: HTMLDivElement;
  } = {};
  private domMetrics: {
    [category: string]: {
      position: number;
      height: number;
      navOffsetLeft: number;
    }
  } = {};
  private isMobile: boolean = screen.width < 768;
  private catNav: {
    [category: string]: HTMLDivElement;
  } = {};

  constructor(props: Props) {
    super(props);

    this.state = {
      isNavFixed: false,
      basketLeft: 0,
      toggleBasket: false,
      showDates: false,
      activeDay: null,
      basketTopPadding: 80,
      treatmentsHeight: 100,
      isBasketOpen: false,
      activeCategory: null,
      beltWidth: 0
    };
  }

  componentWillMount() {
    window.addEventListener("scroll", this.handleScroll);
    window.addEventListener("resize", this.handleResize);

    if (this.props.categories.length === 0)
      this.props.fetchCategories();

    if (!this.getProvider()) {
      this.props.fetchProvider(this.props.params.urn);
    }

    const iOS = /iPhone|iPod/.test(navigator.userAgent) && !(window as any).MSStream;
    if (iOS) this.launchApp();
  }

  componentDidMount() {
    if (this.refs.basket) {
      this.setState({basketLeft: this.cumulativeOffsetLeft(this.refs.basket)});
    }

    const provider = this.getProvider();
    if (provider) {
      setTitle(provider.therapist.name);
      setDescription(provider.salon.insight);

      this.initDomMetrics();
      this.calcCategoryBeltWidth();
    }

    this.windowInnerHeight = window.innerHeight;
  }

  componentWillReceiveProps(nextProps: Props) {
    const urn = this.props.params.urn;

    if (nextProps.basket[urn]) {
      if (this.props.basket[urn] && this.props.basket[urn].length === 0 && nextProps.basket[urn].length === 1) {
        let offset = 150;
        if (window.outerWidth <= 767) {
          offset += 30;
        }
        $("body").animate({
          scrollTop: $(this.refs.treatment).offset().top - offset
        }, 800);
      }
    }

    if ((this.props.availability.length === 0 && nextProps.availability.length !== 0) || (this.props.isAvailabilityBusy && !nextProps.isAvailabilityBusy))
      if (this.isSelectedTimeAvailable(nextProps.availability))
        browserHistory.push(`/${urn}/checkout`);
      else
        this.setState({showDates: true});
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
    window.removeEventListener("resize", this.handleResize);

    this.props.clearAvailability();
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (!!this.state.showDates && !prevState.showDates)
      this.navigateTo({preventDefault: () => null}, "categoryNavbar", 70);

    if (this.props.providers.length !== 0 && prevProps.providers.length === 0) {
      const provider = this.getProvider();
      setTitle(provider.therapist.name);
      setDescription(provider.salon.insight);

      this.initDomMetrics();
      this.calcCategoryBeltWidth();
    }
  }

  initDomMetrics = (): void => {
    Object.keys(this.domCategories).forEach(cat => {
      this.domMetrics[cat] = {
        position: $(this.domCategories[cat]).offset().top,
        height: this.domCategories[cat].offsetHeight,
        navOffsetLeft: this.catNav[cat].offsetLeft
      };
    });
  }

  calcCategoryBeltWidth = (): void => {
    const padding = Object.values(this.catNav).reduce((amount, dom) => amount + dom.clientWidth, 0);

    this.setState({
      ...this.state,
      beltWidth: padding
    });
  }

  handleScroll = (): void => {
    const content = $(this.refs.tabContentServices).offset();
    if (content) {
      if (window.scrollY >= content.top - 180) {
        if (this.state.isNavFixed !== true)
          this.setState({isNavFixed: true});
      } else {
        if (this.state.isNavFixed !== false)
          this.setState({isNavFixed: false});
      }

      this.calcBasketLeft();
    }

    const provider = this.getProvider();
    if (provider) {
      const categories = this.getProviderCategories(this.getProvider());
      const {length} = categories;
      for (let i = 0; i < length; i++) {
        if (this.state.activeCategory !== categories[i] && this.isScrollPositionWithinCategory(categories[i])) {
          this.setState({
            ...this.state,
            activeCategory: categories[i]
          });

          if (this.isMobile) {
            $(this.beltWrapper).animate({
              scrollLeft: this.domMetrics[categories[i]].navOffsetLeft + (this.state.beltWidth / 2) - (screen.width * 0.7)
            }, 500);
          }

          break;
        }
      }
    }
  }

  isScrollPositionWithinCategory = (category: string): boolean => {
    const metric = this.domMetrics[category];
    const {scrollY} = window;

    if (scrollY > (metric.position - 150) && scrollY < ((metric.position + metric.height) - 150)) return true;
    else return false;
  }

  handleResize = (): void => {
    this.calcBasketLeft();

    // resize for mobile screens when top navbar appears
    if (this.windowInnerHeight > window.innerHeight) {
      this.setState({
        ...this.state,
        basketTopPadding: 140
      });
    } else {
      this.setState({
        ...this.state,
        basketTopPadding: 80
      });
    }
    this.windowInnerHeight = window.innerHeight;

    this.setState({
      ...this.state,
      treatmentsHeight: (screen.height - 135) - this.treatmentsWrapper.offsetTop
    });
  }

  calcBasketLeft = (): void => {
    let left = 0;
    if (this.refs.basket && this.refs.treatment) {
      left = this.cumulativeOffsetLeft(this.refs.treatment) + this.refs.treatment["clientWidth"] - this.refs.basket["clientWidth"];
      if (this.state.basketLeft !== left) {
        this.setState({basketLeft: left});
      }
    }
  }

  cumulativeOffsetLeft = (element): number => {
    let left = 0;
    do {
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return left;
  }

  navigateTo = (event, category: string, offset: number = 150): void => {
    event.preventDefault();

    if (window.outerWidth <= 767)
      offset += 30;
    $("html, body").animate({
      scrollTop: $("#" + category).offset().top - offset
    }, 800);
  }

  launchApp = (): void => {
    const [salonId, workstationId] = this.props.params.urn.split("-");

    if (workstationId) {
      window.location.replace(`ruuby://?salon_id=${salonId}&therapist_id=${workstationId}`);
    } else {
      window.location.replace(`ruuby://?salon_id=${salonId}`);
    }
  }

  getProvider = (): ApiProvider => {
    return findApiProvider(this.props.providers, this.props.params.urn);
  }

  canAddTreatmentToBasket = (): boolean => {
    return this.props.selectedDate !== undefined;
  }

  removeTreatmentFromBasketHandler = (treatmentIndex: number): void => {
    this.props.removeTreatmentFromBasket(this.props.params.urn, treatmentIndex);
    this.clearSelectedDate();
  }

  handleNextClick = (): void => {
    this.props.fetchAvailability(this.props.params.urn);
  }

  isSelectedTimeAvailable = (availability: ApiAvailability[]): boolean => {
    const {selectedTime} = this.props;
    if (selectedTime) {
      const available = availability.find(a => a.dateValue.format("YYYYMMDD") === selectedTime.format("YYYYMMDD"));
      if (available) {
        const availableTime = available.dateSlots.find(s => s.format("HH:mm") === selectedTime.format("HH:mm"));
        if (availableTime) return true;
      }
    }

    return false;
  }

  closeDateSelection = (): void => {
    this.setState({
      showDates: false
    });
  }

  getTotalPrice = (): string => {
    const {urn} = this.props.params;
    const basket = this.props.basket[urn];
    const sum = (basket && basket.length > 0) ? basket.reduce((totalPrice, treatment) => (totalPrice + treatment.price), 0) : 0;
    return "£" + sum;
  }

  flushBasket = (): void => {
    const {urn} = this.props.params;
    for (const _item in this.props.basket[urn]) {
      this.props.removeTreatmentFromBasket(urn, 0);
    }
  }

  toggleBasket = (): void => {
    this.setState({toggleBasket: !this.state.toggleBasket});
  }

  renderBasket = (): JSX.Element => {
    let { isNavFixed, basketLeft, toggleBasket } = this.state;
    const { urn } = this.props.params;
    const basketFilled = this.props.basket[urn] && this.props.basket[urn].length !== 0;
    const provider = this.getProvider();
    const {name} = provider.salon.isMobile ? provider.therapist : provider.salon;

    return (
      <div className={`basket ${!this.isMobile && isNavFixed ? "fixed " : ""} ${toggleBasket ? "toggled " : ""} ${!basketFilled ? "basketEmpty" : ""}`} ref="basket" style={this.refs.basket && !this.isMobile && isNavFixed ? {left:basketLeft + "px"} : {zIndex: 200}}>
        <div className="top-section">
          <h3>{name}</h3>
          {!provider.salon.isMobile && <p className="address">{`${provider.salon.address.address1.trim().replace(",", "")}, ${provider.salon.address.postcode.trim().replace(",", "")}`}</p>}
          <ul className="booked-list">
            {this.props.basket[urn] && this.props.basket[urn].map((treatment: ApiTherapistService, index: number) => (
            <li key={index}>
              <div className="treatment-info-toggle">
                <div>
                  <h4>{treatment.name}</h4>
                  <div className="treatment-price">{"£" + treatment.price}</div>
                </div>
                <div className="pull-right">
                  <span className="icon icon-remove" onClick={() => this.removeTreatmentFromBasketHandler(index)}>-</span>
                </div>
                <div className="booking-period">
                  <p>{treatment.duration} mins</p>
                </div>
              </div>
            </li>
            ))}
          </ul>
        </div>

        <div className="total-price">
            <span className="desktop-view">
              <span>Total</span>
              <span className="pull-right">{this.getTotalPrice()}</span>
            </span>
            {this.state.isBasketOpen ? (
              <span className="mobile-view">
                <span className="total">Total</span>
                <span className="price pull-right">{this.getTotalPrice()}</span>
              </span>
            ) : (
              <span className="mobile-view">
                <span className="name">{`${name} (${this.getTotalPrice()})`}</span>
                <span className="view-booking" onClick={this.toggleViewBasket}>View Booking</span>
              </span>
            )}
        </div>
        {
          (!basketFilled) ?
            <div className="btn-checkout disabled">Next</div>
            :
            <div className="btn-checkout-wrapper">
              <div className="btn-checkout" onClick={this.handleNextClick}>Next</div>
              <LineLoader isBusy={this.props.isAvailabilityBusy} />
            </div>
        }
      </div>
    );
  }

  toggleViewBasket = (): void => {
    const tl = new TimelineMax();
    if (this.state.isBasketOpen) {
      tl.to(this.viewBasket, 0.5, {bottom: "-100vh"});
    } else {
      tl.to(this.viewBasket, 0.5, {bottom: "0vh"});
    }

    this.setState({
      ...this.state,
      isBasketOpen: !this.state.isBasketOpen
    });
  }

  renderMobileBasket = (): JSX.Element => {
    const { urn } = this.props.params;
    const basket = this.props.basket[urn];
    const basketTotal = this.getTotalPrice();
    const basketLength = basket ? basket.length : 0;
    const provider = this.getProvider();

    return (
      <div className="mobile-view-basket" ref={div => this.viewBasket = div} style={{paddingTop: `${this.state.basketTopPadding}px`}}>
        <div className="close-icon" onClick={this.toggleViewBasket}><img src={require("../../assets/imgs/closeIcon.png")} alt="Close modal"/></div>
        <div className="img-wrapper">
          <img id="therapist-image" ref={img => this.basketImg = img} src={provider.salon.images[0]} alt="Booking Therapist" />
        </div>
        <h3>{provider.salon.name}</h3>
        <p>({basketLength}) Treatments - {basketTotal}</p>
        <hr/>
        <div className="treatments" ref={t => this.treatmentsWrapper = t} style={{height: `${this.state.treatmentsHeight}px`}}>
          {basket && basket.map((treatment, i) => this.renderBasketTreatment(treatment, i))}
        </div>
      </div>
    );
  }

  renderBasketTreatment = (treatment: ApiTherapistService, i: number): JSX.Element => {
    return (
      <div className="treatment" key={treatment.urn}>
        <div className="pull-left">
          <span className="icon icon-remove" onClick={() => this.removeTreatmentFromBasketHandler(i)}>-</span>
        </div>
        <div className="info">
          <p className="name">{treatment.name}</p>
          <p className="duration">{treatment.duration} mins</p>
        </div>
        <div className="price">
          <p>{treatment.price ? sprintf("£%0.2f", treatment.price) : sprintf("£%0.2f", treatment.price)}</p>
        </div>
      </div>
    );
  }

  setActiveDay = (availabilitySlot: ApiAvailability): void => {
    this.setState({
      activeDay: availabilitySlot,
    });
    this.props.setSelectedDate(moment(availabilitySlot.dateValue));
  }

  setSelectedDate = (date: moment.Moment): void => {
    this.props.setSelectedDate(date);
  }

  clearSelectedDate = (): void => {
    this.setState({
      activeDay: null,
      showDates: false
    });
    this.props.clearSelectedDate();
  }

  handleTreatmentCarousel = (direction: "previous" | "next"): void => {
    let offset = this.beltWrapper.clientWidth - 30;
    if (direction === "previous") offset *= -1;
    $(this.beltWrapper).animate({
      scrollLeft: $(this.beltWrapper).scrollLeft() + offset
    }, 300);
  }

  getProviderCategories = (provider: ApiProvider): string[] => {
    let categories: string[] = [];

    provider.therapist.services.map(cat => {
      if (cat.services.length > 0)
        categories.push(cat.category);
    });

    return categories;
  }

  renderNavbar = (provider: ApiProvider): JSX.Element => {
    let { isNavFixed } = this.state;
    let categories: any[] = provider ? this.getProviderCategories(provider).map(cat => ({
        link: "#" + cat,
        content: cat
     })) : [];

    return (
      <div className={`category-nav-bar ${isNavFixed ? "fixed" : ""}`} id="categoryNavbar">
        <div className="left-arrow" onClick={() => this.handleTreatmentCarousel("previous")}><Arrow /></div>
        <div className="belt-gradient-left"></div>
        <div className="category-belt-wrapper" ref={div => this.beltWrapper = div}>
          <div className="category-belt" style={this.isMobile ? {width: `${Math.max(this.state.beltWidth + 100, screen.width)}px`} : {}}>
            {categories.map((category, i) => (
              <div key={i} onClick={e => this.navigateTo(e, category.content)} className="category" ref={div => this.catNav[category.content] = div}>
                <a href={category.link} className={`category-name ${category.content === this.state.activeCategory ? "active" : ""}`}>{ category.content }</a>
              </div>
            ))}
          </div>
        </div>
        <div className="belt-gradient-right"></div>
        <div className="right-arrow" onClick={() => this.handleTreatmentCarousel("next")}><Arrow /></div>
      </div>
    );
  }

  render(): JSX.Element {
    const provider = this.getProvider();
    const isMobile = screen.width < 768;
    const {isNavFixed} = this.state;
    const modalStyle: ReactModal.Styles = {
      content: {
        width: isMobile ? "95%" : "80%",
        top: "calc(50% - 150px)",
        bottom: "auto",
        left: isMobile ? "2.5%" : "10%"
      }
    };

    return (
      <section id="provider">
        <div className="container provider-overview">
          {provider && <ProviderDetails provider={provider} />}
        </div>

        {provider && this.renderNavbar(provider)}

        <div className="treatment-list-container container" style={isMobile && isNavFixed ? {paddingTop: "90px"} : {}}>
          <LineLoader isBusy={this.props.isBusy} />
          {provider &&
            <div id="tab-content-services" className="tab-content-services" ref="tabContentServices" style={this.state.showDates ? {minHeight: "auto", overflow: "visible"} : {}}>
              <div className="treatment-list" ref="treatment">
                <TreatmentsList
                  provider={provider}
                  addTreatmentToBasket={this.props.addTreatmentToBasket}
                  removeTreatmentFromBasket={this.props.removeTreatmentFromBasket}
                  basket={this.props.basket}
                  selectedCategory={this.props.selectedCategory}
                  refs={this.domCategories} />

                <ModalShell
                  title="Select Date &amp; Time"
                  show={this.state.showDates}
                  style={modalStyle}
                  closeModal={this.closeDateSelection}
                  label="Select Date &amp; time"
                >
                  <SelectDateTime
                    availability={this.props.availability}
                    activeDay={this.state.activeDay}
                    onDaySelected={this.setActiveDay}
                    selectedDate={this.props.selectedDate}
                    setSelectedDate={this.setSelectedDate}
                    fetchAvailability={this.props.fetchAvailability}
                    isAvailabilityBusy={this.props.isAvailabilityBusy}
                    providerUrn={this.props.params.urn}
                    selectedTime={this.props.selectedTime}
                    setSelectedTime={this.props.setSelectedTime}
                    urn={this.props.params.urn} />
                </ModalShell>
              </div>
              {this.renderBasket()}
              {this.renderMobileBasket()}
            </div>
          }
        </div>
      </section>
    );
  }
}

function mapStateToProps(state: any) {
  const {selectedDate, selectedTime} = state.bookingState;
  return {
    availability: state.providerState.get("availability").toJS(),
    isBusy: state.providerState.get("isBusy"),
    isAvailabilityBusy: state.providerState.get("isAvailabilityBusy"),
    selectedDate: selectedDate ? moment(selectedDate) : null,
    selectedTime: selectedTime ? moment(selectedTime) : null,
    providers: state.providerState.get("items").toJS(),
    categories: state.categoriesState.get("items"),
    basket: state.basketState.get("items").toJS(),
    selectedCategory: state.bookingState.selectedCategory
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    fetchAvailability,
    clearAvailability,
    fetchProvider,
    fetchCategories,
    removeTreatmentFromBasket,
    addTreatmentToBasket,
    clearBasket,
    setSelectedDate: actionCreators.setSelectedDate,
    setSelectedTime: actionCreators.setSelectedTime,
    clearSelectedDate: actionCreators.clearSelectedDate,
    clearSelectedTime: actionCreators.clearSelectedTime
  }, dispatch) as any;
}

export const Provider = connect(mapStateToProps, mapDispatchToProps)(ProviderComponent);
