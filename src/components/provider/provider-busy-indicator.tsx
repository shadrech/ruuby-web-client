import * as React from "react";
import * as Spinner from "react-spinner";

import "react-spinner/react-spinner.css";

interface BusyIndicatorProps {
  display: boolean;
}

export class BusyIndicator extends React.Component<BusyIndicatorProps, {}> {
  render() {
    if (!this.props.display) return null;

    return (
      <div className="busy-indicator"><Spinner /></div>
    );
  }
}
