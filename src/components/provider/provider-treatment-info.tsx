import * as React from "react";
import { AddTreatment, TreatmentSelected } from "../sub/svgs";

interface TreatmentInfoProps {
  name: string;
  price: number;
  duration: number;
  actionIndicator?: string;
  onActionClick?: () => void;
  isInBasket: boolean;
}

export class TreatmentInfo extends React.Component<TreatmentInfoProps, {}> {
  renderButton() {
    return (
        <div className="treatment-info-btn">
          {
            this.props.isInBasket ?
              <TreatmentSelected /> :
              <AddTreatment />
          }
        </div>
    );
  }

  render() {

    return(
      <div onClick={this.props.onActionClick} className="pointer">
        <div className={`treatment-info-toggle${this.props.isInBasket ? " inBasket" : ""}`}>
          <div>
            <h4>{this.props.name}</h4>
            <div className="treatment-price">{"£" + this.props.price}</div>
          </div>
          <div className="booking-period">
            <p>{this.props.duration} mins</p>
          </div>
          {/*<div className="treatment-description">*/}
            {/*<p>Technique of applying polish (we love -it lasts). Practicing since the age of 13 (!) in Londrina</p>*/}
          {/*</div>*/}
        </div>

        {this.props.actionIndicator && this.renderButton()}
      </div>
    );
  }
}
