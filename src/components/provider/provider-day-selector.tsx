import * as React from "react";
import {ApiAvailability} from "../../api/provider";

interface DaySelectorProps {
  availabilityItem: ApiAvailability;
  isActive: boolean;

  onDayClick: (event: React.MouseEvent<HTMLDivElement>) => void;
}

interface DayAttributes {
  key: string;
  className?: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement>) => void;
}

export class DaySelector extends React.Component<DaySelectorProps, {}> {
  render() {
    const {availabilityItem} = this.props;

    const attributes: DayAttributes = {
      key: availabilityItem.dateValue.format(),
    };

    const classes = ["day"];
    if (availabilityItem.dateSlots.length > 0) {
      classes.push("day-available");
      attributes.onClick = this.props.onDayClick;
    }

    if (this.props.isActive) classes.push("day-selected");

    attributes.className = classes.join(" ");

    return (
      <div {...attributes}>
        <span className="day-slot-day-name">{availabilityItem.dateValue.format("ddd")}</span>
        <span className="day-slot-date">{availabilityItem.dateValue.format("D")}</span>
        <span className="day-slot-month">•</span>
      </div>
    );
  }
}
