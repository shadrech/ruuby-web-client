import * as React from "react";
import Icon from "react-fa";
const Carousel = require("nuka-carousel");
import {ApiProvider} from "../../api/provider";

interface ProviderDetailsProps {
  provider: ApiProvider;
}

export class ProviderDetails extends React.Component<ProviderDetailsProps, {}> {
  changeToCamelCase(str: string) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase();
  }

  renderCarousel(id: string) {
    const imgCategory = ["insight", "review", "product"];
    const { images } = this.props.provider.salon;
    // const md = new MobileDetect(window.navigator.userAgent);
    // if (md.mobile()) {
    //   return <img src={this.props.images[0]} alt="therapist-image" className="therapists-img-box" />
    // }

    const imgs = (id.split("-").length === 1) ? images.filter((_url: string, i: number) => i !== 0).map((url: string, i: number) => {
      const style = {
        width: "100%",
        backgroundImage: `url(${url})`,
        backgroundSize: "cover",
        backgroundPosition: "center"
      };

      return <div key={i} className="therapists-img-box" style={style}></div>;
    }) :
    imgCategory.map((cat: string, i: number) => {
      const style = {
        width: "100%",
        backgroundImage: `url(https://images.ruuby.com/therapist/urn:ruuby:therapist:${id}/${cat}/1.8x1-500.jpg)`,
        backgroundSize: "cover",
        backgroundPosition: "center"
      };

      return <div key={i} className="therapists-img-box" style={style}></div>;
    });

    return (
      <Carousel ref="carousel" dragging={true} autoplay={true} wrapAround={true}>
        {imgs}
      </Carousel>
    );
  }

  renderServices() {
    const { services } = this.props.provider.therapist;
    const categories = services.map(service => service.category);

    return (
      <div className="services-list">
        {categories && categories.length > 0 &&
          <ul>
            {categories.slice(0, 3).map((category, i) => {
              return <li key={i}><h2>{this.changeToCamelCase(category)}</h2></li>;
              })
            }
          </ul>
        }
      </div>
    );
  }

  renderRating() {
    return (
      <div className="rating">
        <p>{this.props.provider.salon.rating} <Icon className="icon-heart" name="heart-o"/></p>
      </div>
    );
  }

  handlesNext(event: any) {
    event.preventDefault();
    // ref="carousel" so :
    this.refs.carousel["nextSlide"]();
  }

  handlesPrevious(event: any) {
    event.preventDefault();
    // ref="carousel" so :
    this.refs.carousel["previousSlide"]();
  }

  render() {
    const { urn } = this.props.provider.therapist;
    const urnParts = urn.split(":");
    const id = urnParts[urnParts.length - 1];
    const { insight, profileImage } = this.props.provider.salon;
    const name = this.props.provider.salon.isMobile ? this.props.provider.therapist.name : this.props.provider.salon.name;

    return (
      <div className="therapists-service salon-service">
        <div className="row">
          <Icon className="control-carousel left-carousel" name="angle-left" onClick={(e) => this.handlesPrevious(e)} />
          <div className="c-large-6 c-medium-6 c-small-12 c-xsmall-12 therapists-overview">
            <div className="therapists-text-box">
              <div className="row">
                <div className="c-large-6 c-medium-6 c-small-6 c-xsmall-12 logo-container">
                  <img className="therapists-profile-img" src={profileImage} alt={name} />
                </div>
                <div className="c-large-6 c-medium-6 c-small-6 c-xsmall-12 profile-container">
                  <div>
                    <h1>{name}</h1>
                    {this.renderServices()}
                    {this.renderRating()}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="c-large-6 c-medium-6 c-small-12 c-xsmall-12 slider-container">
            {this.renderCarousel(id)}
            <h2 className="overview">
              {insight}
            </h2>
          </div>
          <Icon className="control-carousel right-carousel" name="angle-right" onClick={(e) => this.handlesNext(e)} />
        </div>
      </div>
    );
  }
}
