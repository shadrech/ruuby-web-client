import * as moment from "moment";
import * as React from "react";

interface TimeSelectorProps {
  time: moment.Moment;
  isActive: boolean;

  onTimeClick: () => void;
}

export class TimeSelector extends React.Component<TimeSelectorProps, {}> {
  render() {
    const time = this.props.time;

    const classes = ["time"];
    if (this.props.isActive) classes.push("time-selected");

    return (
      <div
        key={time.format()}
        className={classes.join(" ")}
        onClick={this.props.onTimeClick}
      >
        {time.format("HH:mm")}
      </div>
    );
  }
}
