import * as React from "react";

interface Props {
  img: string;
  title: string;
  info: string;
}

export default class CircularThing extends React.Component<Props, {}> {
  render() {
    // let imgs = [];
    // for (let i = 0; i < 33; i++) {
    //   imgs.push(<img key={i} className={`img${i}`} src={require(`../../../assets/imgs/${this.props.img}`)} />);
    // }

    return (
      <div className="col-md-6 circular-info">
        <div className="img-circle-wrapper">
          <div className="img-circle">
            <div className="header">
              <h2>{this.props.title}</h2>
              <p>Get in touch »</p>
            </div>
            {/*imgs*/}
            <img src={require(`../../assets/imgs/about/circle_${this.props.img}`)} />
          </div>
        </div>
        <div className="info">
          <p>{this.props.info}</p>
        </div>
      </div>
    );
  }
}
