import * as React from "react";
import {
  ArrowThin,
  EveningStandardLogo, GraziaLogo, StyleLogo, NewYorkTimesLogo,
  ForbesLogo, WiredLogo
} from "../../components/sub/svgs";
import AboutCarousel from "./carousel";
import AboutCarousel3D from "./carousel3d";
import CircularInfo from "./circular-info";
import { setMetadata } from "../../utils/meta-controller/index";

export default class AboutContainer extends React.Component<{}, {}> {
  componentWillMount() {
    setMetadata("about");
  }

  render() {
    return (
      <div className="container about-container">
        <div className="row about-row">
          <h1>About Ruuby</h1>
          <h2>Ruuby is London's first digital beauty concierge, offering five star beauty services to private clients, corporates, hotel groups and luxury concierge services. Calling on our network of over 200 professionally vetted, experienced beauty providers, we deliver consistently high-quality beauty services to our clients. Services can be booked at the home, office, or hotel, and we also cater to large events, weddings and VIPs.</h2>
          <ArrowThin />
        </div>

        <div className="row offer-row">
          <h1>The Offer</h1>
          <p className="order">1</p>
          <h2>Select your service, from a variety of options - from a manicure, to lashes to tanning to makeup</h2>
          <div className="icon-container">
            <div className="icon-wrapper nails"><div className="icon"></div></div>
            <div className="icon-wrapper hair"><div className="icon"></div></div>
            <div className="icon-wrapper facial"><div className="icon"></div></div>
            <div className="icon-wrapper tanning"><div className="icon"></div></div>
            <div className="icon-wrapper makeup"><div className="icon"></div></div>
            <div className="icon-wrapper wellness"><div className="icon"></div></div>
          </div>

          <p className="order">2</p>
          <h2>Choose your time and address, and have a browse through the available beauticians. You can review bios, reviews and examples of existing work.</h2>

          <AboutCarousel />

          <p className="order">3</p>
          <h2>Simply confirm, and wait for Ruuby to arrive!</h2>

          <AboutCarousel3D />
        </div>

        <div className="row info-row">
          <CircularInfo img="makeup2.png" title="WEDDINGS" info="Ruuby's team of professional hair and makeup artists are highly experienced, and offer the perfect service on wedding day. Artists and hairdressers can be booked for both a trial (before the day), and the main event, to ensure happy parties all around! We offer packages for the bride, and the wedding party." />
          <CircularInfo img="eyes.png" title="EVENTS" info="Book Ruuby's highly skilled professionals to attend events. Whether its a hen party, a fashion show, or other large event, we can curate a bespoke package to suit the event." />
        </div>

        <div className="row press-info">
          <h1>Press</h1>
          <div className="logo-container">
            <div className="logo"><GraziaLogo /></div>
            <div className="logo"><ForbesLogo /></div>
            <div className="logo"><StyleLogo /></div>
            <div className="logo"><NewYorkTimesLogo /></div>
            <div className="logo"><WiredLogo /></div>
            <div className="logo"><EveningStandardLogo /></div>
          </div>
          <div className="links-container">
            <a className="link" href="http://www.harpersbazaar.co.uk/beauty/make-up-nails/news/g23362/best-beauty-apps/" target="_blank">
              <div className="date">19 JUL 2017</div>
              <div className="title">The Best Beauty Apps</div>
              <div className="hyper">READ »</div>
            </a>
            <a className="link" href="https://www.cosmeticsbusiness.com/news/article_page/The_Estee_Lauder_Companies_and_Ruuby_open_beauty_concept_salon/129926" target="_blank">
              <div className="date">1 JUN 2017</div>
              <div className="title">The Estée Lauder Companies and Ruuby open beauty concept salon</div>
              <div className="hyper">READ »</div>
            </a>
            <a className="link" href="http://www.vogue.co.uk/gallery/affordable-spa-treatments" target="_blank">
              <div className="date">16 JUN 2017</div>
              <div className="title">Exams Over? Now It's Time To Treat Yourself</div>
              <div className="hyper">READ »</div>
            </a>
            <a className="link" href="http://lifestyle.one/grazia/fashion/shopping/get-instant-access-london-s-best-spas-salons-new-ruuby-app/" target="_blank">
              <div className="date">17 FEB 2015</div>
              <div className="title">Want Instant Access To London's Best Spas And Salons? There's An App For That</div>
              <div className="hyper">READ »</div>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
