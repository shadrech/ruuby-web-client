import * as React from "react";
const TimelineMax = require("gsap/src/uncompressed/TimelineMax");
import {ArrowThin} from "../sub/svgs";

interface Figures {
  [name: string]: {
    dom: HTMLElement;
    transform: number;
  };
}

declare const Sine: any;

export default class AboutCarousel3D extends React.Component<{}, {}> {
  // private active: number = 4;
  private imgCount: number = 7;
  private figs: Figures = {
    img1: { dom: null, transform: 4 },
    img2: { dom: null, transform: 5 },
    img3: { dom: null, transform: 6 },
    img4: { dom: null, transform: 7 },
    img5: { dom: null, transform: 8 },
    img6: { dom: null, transform: 9 },
    img7: { dom: null, transform: 10 }
  };
  private transforms: {[id: number]: any} = (screen.width < 768) ? { // mobile
    1: {rotationY: 90, xPercent: -140, y: 0, z: -175, zIndex: 14, ease: Sine.easeOut},
    2: {rotationY: 90, xPercent: -130, y: 0, z: -150, zIndex: 15, ease: Sine.easeOut},
    3: {rotationY: 80, xPercent: -110, y: 0, z: -125, zIndex: 16, ease: Sine.easeOut},
    4: {rotationY: 60, xPercent: -80, y: 0, z: -100, zIndex: 17, ease: Sine.easeOut},
    5: {rotationY: 40, xPercent: -50, y: 0, z: -75, zIndex: 18, ease: Sine.easeOut},
    6: {rotationY: 20, xPercent: -30, y: 0, z: -50, zIndex: 19, ease: Sine.easeOut},
    7: {rotationY: 0, xPercent: 0, y: 0, z: 10, zIndex: 20, ease: Sine.easeInOut},
    8: {rotationY: -20, xPercent: 30, y: 0, z: -50, zIndex: 19, ease: Sine.easeOut},
    9: {rotationY: -40, xPercent: 50, y: 0, z: -75, zIndex: 18, ease: Sine.easeOut},
    10: {rotationY: -60, xPercent: 80, y: 0, z: -100, zIndex: 17, ease: Sine.easeOut},
    11: {rotationY: -80, xPercent: 110, y: 0, z: -125, zIndex: 16, ease: Sine.easeOut},
    12: {rotationY: -90, xPercent: 130, y: 0, z: -150, zIndex: 15, ease: Sine.easeOut},
    13: {rotationY: -90, xPercent: 140, y: 0, z: -200, zIndex: 14, ease: Sine.easeOut}
  } : (screen.width < 1000) ? { // tablet
    1: {rotationY: 90, xPercent: -260, y: 0, z: -470, zIndex: 14, ease: Sine.easeOut},
    2: {rotationY: 90, xPercent: -240, y: 0, z: -450, zIndex: 15, ease: Sine.easeOut},
    3: {rotationY: 80, xPercent: -200, y: 0, z: -400, zIndex: 16, ease: Sine.easeOut},
    4: {rotationY: 60, xPercent: -160, y: 0, z: -350, zIndex: 17, ease: Sine.easeOut},
    5: {rotationY: 40, xPercent: -120, y: 0, z: -300, zIndex: 18, ease: Sine.easeOut},
    6: {rotationY: 20, xPercent: -80, y: 0, z: -200, zIndex: 19, ease: Sine.easeOut},
    7: {rotationY: 0, xPercent: 0, y: 0, z: 10, zIndex: 20, ease: Sine.easeInOut},
    8: {rotationY: -20, xPercent: 80, y: 0, z: -200, zIndex: 19, ease: Sine.easeOut},
    9: {rotationY: -40, xPercent: 120, y: 0, z: -300, zIndex: 18, ease: Sine.easeOut},
    10: {rotationY: -60, xPercent: 160, y: 0, z: -350, zIndex: 17, ease: Sine.easeOut},
    11: {rotationY: -80, xPercent: 200, y: 0, z: -400, zIndex: 16, ease: Sine.easeOut},
    12: {rotationY: -90, xPercent: 240, y: 0, z: -450, zIndex: 15, ease: Sine.easeOut},
    13: {rotationY: -90, xPercent: 260, y: 0, z: -475, zIndex: 14, ease: Sine.easeOut}
  } : { // desktop
    1: {rotationY: 90, xPercent: -260, y: 0, z: -570, zIndex: 14, ease: Sine.easeOut},
    2: {rotationY: 90, xPercent: -240, y: 0, z: -550, zIndex: 15, ease: Sine.easeOut},
    3: {rotationY: 80, xPercent: -200, y: 0, z: -500, zIndex: 16, ease: Sine.easeOut},
    4: {rotationY: 60, xPercent: -160, y: 0, z: -450, zIndex: 17, ease: Sine.easeOut},
    5: {rotationY: 40, xPercent: -120, y: 0, z: -400, zIndex: 18, ease: Sine.easeOut},
    6: {rotationY: 20, xPercent: -80, y: 0, z: -300, zIndex: 19, ease: Sine.easeOut},
    7: {rotationY: 0, xPercent: 0, y: 0, z: 10, zIndex: 20, ease: Sine.easeInOut},
    8: {rotationY: -20, xPercent: 80, y: 0, z: -300, zIndex: 19, ease: Sine.easeOut},
    9: {rotationY: -40, xPercent: 120, y: 0, z: -400, zIndex: 18, ease: Sine.easeOut},
    10: {rotationY: -60, xPercent: 160, y: 0, z: -450, zIndex: 17, ease: Sine.easeOut},
    11: {rotationY: -80, xPercent: 200, y: 0, z: -500, zIndex: 16, ease: Sine.easeOut},
    12: {rotationY: -90, xPercent: 240, y: 0, z: -550, zIndex: 15, ease: Sine.easeOut},
    13: {rotationY: -90, xPercent: 260, y: 0, z: -575, zIndex: 14, ease: Sine.easeOut}
  };

  componentDidMount() {
    this.initialize();
  }

  initialize = () => {
    const tl = new TimelineMax();
    tl.to(this.figs.img4.dom, 0.3, this.transforms[this.figs.img4.transform])
      .to(this.figs.img1.dom, 0.3, this.transforms[this.figs.img1.transform])
      .to(this.figs.img2.dom, 0.3, this.transforms[this.figs.img2.transform])
      .to(this.figs.img3.dom, 0.3, this.transforms[this.figs.img3.transform])
      .to(this.figs.img7.dom, 0.3, this.transforms[this.figs.img7.transform], "-=0.9")
      .to(this.figs.img6.dom, 0.3, this.transforms[this.figs.img6.transform])
      .to(this.figs.img5.dom, 0.3, this.transforms[this.figs.img5.transform]);
  }

  assignTransforms = (dir: "left" | "right") => {
    if (dir === "left")
      for (let i = 1; i <= this.imgCount; i++) {
        const oldIdx = this.figs[`img${i}`].transform;
        this.figs[`img${i}`].transform = oldIdx - 1;
      }
    else
      for (let i = this.imgCount; i > 0; i--) {
        const oldIdx = this.figs[`img${i}`].transform;
        this.figs[`img${i}`].transform = oldIdx + 1;
      }
  }

  slideCarousel = (direction: "left" | "right") => {
    if (direction === "left" && this.figs.img1.transform !== 1) {
      this.assignTransforms(direction);
      let tl = new TimelineMax();

      for (let i = 1; i <= this.imgCount; i++) {
        const fig = this.figs[`img${i}`];
        tl.to(fig.dom, 0.3, this.transforms[fig.transform], "-=0.2");
      }
    } else if (direction === "right" && this.figs[`img${this.imgCount}`].transform !== 13) {
      this.assignTransforms(direction);
      let tl = new TimelineMax();

      for (let i = this.imgCount; i > 0; i--) {
        const fig = this.figs[`img${i}`];
        tl.to(fig.dom, 0.3, this.transforms[fig.transform], "-=0.2");
      }
    }
  }

  render() {
    return (
      <div className="offer2carousel-container carousel-3d">
        <div className="cover-left"></div>
        <div className="gradient-left"></div>
        <div className="arrow next" onClick={() => this.slideCarousel("right")}><ArrowThin /></div>

        <div id="Slider3d">
          <figure className="img1" ref={dom => this.figs.img1.dom = dom}></figure>
          <figure className="img2" ref={dom => this.figs.img2.dom = dom}></figure>
          <figure className="img3" ref={dom => this.figs.img3.dom = dom}></figure>
          <figure className="img4" ref={dom => this.figs.img4.dom = dom}></figure>
          <figure className="img5" ref={dom => this.figs.img5.dom = dom}></figure>
          <figure className="img6" ref={dom => this.figs.img6.dom = dom}></figure>
          <figure className="img7" ref={dom => this.figs.img7.dom = dom}></figure>
        </div>

        <div className="arrow previous" onClick={() => this.slideCarousel("left")}><ArrowThin /></div>
        <div className="gradient-right"></div>
        <div className="cover-right"></div>
        </div>
    );
  }
}
