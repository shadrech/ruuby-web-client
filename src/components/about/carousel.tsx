import * as React from "react";
import Slider, {Settings} from "react-slick";
import {ArrowThin} from "../../components/sub/svgs";

import "slick-carousel/slick/slick.css";

export default class AboutCarousel extends React.Component<{}, {}> {
  renderArrow = (side: "previous" | "next") => (
    <div>
      <div className={`carousel-controller ${side}`}>
        <ArrowThin />
      </div>
    </div>
  )

  render() {
    const w = screen.width;
    const slidesToShow: number = (w < 768) ? 1 : (w < 1000) ? 2 : 3;
    const sliderSettings: Settings = {
      dots: true,
      dotsClass: "carousel-dots",
      infinite: true,
      speed: 500,
      slidesToShow,
      slidesToScroll: 1,
      autoplay: true
    };

    return (
      <div className="offer2carousel-container">
        <Slider {...sliderSettings} prevArrow={this.renderArrow("previous")} nextArrow={this.renderArrow("next")} className="Slider">
          <div className="frame"><img src={require("../../assets/imgs/about/img1@2x.jpg")} alt="Image of makeup products on shelf"/></div>
          <div className="frame"><img src={require("../../assets/imgs/about/img2@2x.jpg")} alt="Image if treatment chair with product shelf in background"/></div>
          <div className="frame"><img src={require("../../assets/imgs/about/img3@2x.jpg")} alt="Pic of therapist doing someones nails"/></div>
          <div className="frame"><img src={require("../../assets/imgs/about/img4@2x.jpg")} alt="Pic of chairs and table with beauty treatments on top"/></div>
          <div className="frame"><img src={require("../../assets/imgs/about/img5@2x.jpg")} alt="Pic of well manicured nails"/></div>
          <div className="frame"><img src={require("../../assets/imgs/about/img6@2x.jpg")} alt="Pic of of therapist doing clients eyelashes"/></div>
        </Slider>
      </div>
    );
  }
};
