import * as React from "react";
import { bindActionCreators } from "redux";
import { connect, Dispatch } from "react-redux";
import * as moment from "moment";
const cookie = require("react-cookie");

import {HomeBg} from "./home/home-bg";
import HomeOffer from "./home/home-offer";
import HomeFeaturedTreatment from "./home/home-featured-treatment";
import HomeAppStore from "./home/home-app-store";
import HomeTreatments from "./home/home-treatments";
import {HomeMagazine} from "./home/home-magazine";
import HomePublicity from "./home/home-publicity";
import HomeSocial from "./home/home-social";
import EmailSignup from "./email-signup";

import { fetchInstaData } from "../reducers/instagram/actions";
import {fetchCategories} from "../reducers/categories/actions";
import {fetchMagazinePosts} from "../reducers/magazine/actions";
import { actionCreators } from "../reducers/booking/actions";
import {ApiCategory} from "../api/categories";
import {ApiCustomer} from "../api/customer";
import {Post} from "../api/magazine";
import { slotTypes } from "../reducers/booking/reducer";
import { setMetadata } from "../utils/meta-controller/index";

export const COOKIE_HIDE_EMAIL_SIGNUP = "hideSignupOverlayb";

interface Props {
  magazinePosts: Post[];
  posts: any;
  categories: ApiCategory[];
  customer: ApiCustomer;
  isMagazineLoading: boolean;
  category: string;
  slot: slotTypes;
  postcode: string;
  date: string;

  fetchInstaData: () => void;
  fetchCategories: () => void;
  fetchMagazinePosts: () => void;
  setSelectedCategory: () => void;
}

interface State {
  showSignup: boolean;
}

class HomeContainer extends React.Component<Props, State> {
  timer: any;
  state = {
    showSignup: false
  };

  componentDidMount() {
    this.timer = setTimeout(() => {
      if (!cookie.load(COOKIE_HIDE_EMAIL_SIGNUP))
        this.setState({showSignup: true});
    }, 4000);

    if (!Array.isArray(this.props.categories)) this.props.fetchCategories();

    if (this.props.magazinePosts.length === 0)
      this.props.fetchMagazinePosts();
  }

  componentWillMount() {
    setMetadata("home");
  }

  componentWillUnmount() {
    window.clearTimeout(this.timer);
  }

  hideEmailSignup = () => {
    cookie.save(COOKIE_HIDE_EMAIL_SIGNUP, true, {path: "/", expires: moment().add(1, "month").toDate()});
    this.setState({showSignup: false});
  }

  render() {
    const location = {
      category: this.props.category,
      slot: this.props.slot,
      postcode: this.props.postcode,
      date: this.props.date
    };

    return (
      <div className="container-fluid">
        <HomeBg customer={this.props.customer} categories={this.props.categories} location={location} setSelectedCategory={this.props.setSelectedCategory} />
        <HomeTreatments setSelectedCategory={this.props.setSelectedCategory} />
        <HomeAppStore />
        <HomeOffer />
        <HomeFeaturedTreatment />
        <HomeMagazine posts={this.props.magazinePosts} isBusy={this.props.isMagazineLoading}/>
        <HomePublicity />
        <HomeSocial fetchInstaData={this.props.fetchInstaData} posts={this.props.posts} />
        {this.state.showSignup && <EmailSignup hide={this.hideEmailSignup} showSignup={this.state.showSignup} />}
      </div>
    );
  }
}

function mapStateToProps(state: any, ownProps: any) {
  return {
    magazinePosts: state.magazineState.get("posts").toJS(),
    isMagazineLoading: state.magazineState.get("isBusy"),
    posts: state.instagramState.toJS(),
    categories: state.categoriesState.get("items"),
    customer: state.customerState.get("customer"),
    category: ownProps.location.query["category"],
    postcode: ownProps.location.query["postcode"],
    date: ownProps.location.query["date"],
    slot: ownProps.location.query["slot"]
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    fetchInstaData,
    fetchCategories,
    fetchMagazinePosts,
    setSelectedCategory: actionCreators.setSelectedCategory
  }, dispatch) as any;
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
