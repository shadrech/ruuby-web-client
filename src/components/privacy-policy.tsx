import * as React from "react";
import { setMetadata } from "../utils/meta-controller/index";

export default class PrivacyPolicy extends React.Component<{}, {}> {
  componentWillMount() {
    setMetadata("privacyPolicy");
  }

  render() {
    return (
      <div className="container-fluid faqs-container hp-section-four">
        <div className="row faqs-row-wrapper privacy-row-wrapper">
          <div className="col-md-12">
            <div className="row">
              <div className="col-xs-12">
                <h2 className="page-title">Privacy Policy</h2>
                <ul>
                  <li>
                    <p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
                  </li>
                  <li>
                    <p className="header">What personal information do we collect from the people that visit our blog, website or app?</p>
                    <p>Ruuby only works with best-in class salons and beauty pWhen ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number or other details to help you with your experience.</p>
                  </li>
                  <li>
                    <p className="header">When do we collect information?</p>
                    <p>We collect information from you when you register on our site or enter information on our site.</p>
                  </li>
                  <li>
                    <p className="header">How do we use your information?</p>
                    <p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
                      <ul>
                        <li>To quickly process your transactions</li>
                        <li>To send periodic emails regarding your order or other products and services</li>
                      </ul>
                    </p>
                  </li>
                  <li>
                    <p className="header">How do we protect visitor information?</p>
                    <p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible. We use regular Malware Scanning. Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information. All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
                  </li>
                  <li>
                    <p className="header">Do we use 'cookies'?</p>
                    <p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
                  </li>
                  <li>
                    <p className="header">We use cookies to:</p>
                    <p><ul>
                      <li>Help remember and process the items in the shopping cart</li>
                      <li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf</li>
                    </ul>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.</p>
                  </li>
                  <li>
                    <p className="header">If users disable cookies in their browser:</p>
                    <p>If you disable cookies off, some features will be disabled It will turn off some of the features that make your site experience more efficient and some of our services will not function properly. However, you can still place ordersby contacting customer service.</p>
                  </li>
                  <li>
                    <p className="header">Third-party disclosure</p>
                    <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.</p>
                  </li>
                  <li>
                    <p className="header">Google</p>
                    <p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">positive experience for users</a>. We have not enabled Google AdSense on our site.</p>
                  </li>
                  <li>
                    <p className="header">We have implemented the following:</p>
                    <p>We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to analyse the performance of our site</p>
                  </li>
                  <li>
                    <p className="header">Opting out:</p>
                    <p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.</p>
                  </li>
                  <li>
                    <p className="header">How does our site handle do not track signals?</p>
                    <p>We don't honor do not track signals and do not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. We don't honor them because we have not yet implemented this functionality</p>
                  </li>
                  <li>
                    <p className="header">Does our site allow third-party behavioral tracking?</p>
                    <p>It's also important to note that we do not allow third-party behavioral tracking</p>
                  </li>
                  <li>
                    <p className="header">Fair Information Practices</p>
                    <p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
                  </li>
                  <li>
                    <p className="header">In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</p>
                    <p>
                      We will notify the users via email
                      <ul>
                        <li>Within 1 business day</li>
                      </ul>
                      We also agree to the Individual Redress Principle, which requires that individuals have a right to pursue legally enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
                      </p>
                  </li>
                  <li>
                    <p className="header">CAN SPAM Act</p>
                    <p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations. We collect your email address in order to communicate booking information and sent you newsletters.</p>
                  </li>
                  <li>
                    <p className="header">To be in accordance with CANSPAM we agree to the following:</p>
                    <p>If at any time you would like to unsubscribe from receiving future emails, you can email us at info@ruuby.com and we will promptly remove you from ALL correspondence.</p>
                  </li>
                  <li>
                    <p className="header">Contacting Us</p>
                    <p>If there are any questions regarding this privacy policy you may contact us using the information below. <br/><span className="address">Ruuby Limited<br/>Studio B<br/>11 Edith Grove London SW10 0JZ<br/></span>Last Edited on 2016-03-16</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
