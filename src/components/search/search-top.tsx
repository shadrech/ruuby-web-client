import * as React from "react";
import * as Select from "react-select";
import { ArrowThin } from "../sub/svgs";
import LineLoader from "../sub/line-loader";
import {ApiCategory} from "../../api/categories";
import * as bowser from "bowser";
import * as MobileDetect from "mobile-detect";
import * as api from "../../api";

interface SelectOption {
  value: string;
  label: string;
}

interface Props {
  handlePostcodeUpdate: (postcode: string) => void;
  handlePostcodeChange: (postcode: string) => void;
  handleCategoryChange: (categoryId: string) => void;
  setBusyness: () => void;
  setUnbusyness: () => void;
  setSelectedCategory: (category: string) => void;
  setGeoFail: Function;

  categories: ApiCategory[];
  selectedCategory: string;
  postcode: string;
  propsPostcode: string;
  geoFail: boolean;
}
interface State {
  postcode: string;
  postcodeBusy: boolean;
  inputWidth: string;
}

export default class SearchTop extends React.Component<Props, State> {
  state = {
    postcode: "",
    postcodeBusy: false,
    inputWidth: "45px"
  };

  componentDidMount() {
    if (!this.props.propsPostcode || this.props.propsPostcode === "")
      this.getGeolocation();

    this.setState({
      ...this.state,
      inputWidth: this.getSelectInputWidth()
    });
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.selectedCategory !== this.props.selectedCategory)
      this.setState({
        ...this.state,
        inputWidth: this.getSelectInputWidth()
      });
  }

  getSelectInputWidth = (): string => {
    const rect = document.querySelector(".Select-value-label");
    const width = bowser.msie ? 200 : rect ? rect.getBoundingClientRect().width : 45;
    return (width + 45) + "px";
  }

  setPostcode = (postcode: string): void => {
    this.props.handlePostcodeChange(postcode);

    this.setState({...this.state, postcodeBusy: false});
  }

  geoFail = () => {
    this.setState({...this.state, postcodeBusy: false});
    this.props.setUnbusyness();
    this.props.setGeoFail();
  }

  getGeolocation = (): void => {
    this.props.setBusyness();
    this.setState({...this.state, postcodeBusy: true});

    const errorCallback = () => this.geoFail();

    if (!navigator.geolocation)
      this.geoFail();
    else
      navigator.geolocation.getCurrentPosition(this.handleGeoSuccess, errorCallback);
  }

  handleGeoSuccess = async (position: any): Promise<void> => {
    try {
      const postcode = await api.getPostcodeFromLongitudeLatitude(position.coords.longitude, position.coords.latitude);
      if (!this.props.postcode) this.setPostcode(postcode);
    } catch (_error) {
      this.geoFail();
    }
  }

  getCategories = (): SelectOption[] => {
    const cats = Array.isArray(this.props.categories) ? this.props.categories.map(category => {
      return {value: category.name, label: category.name};
    }) : [];

    cats.unshift({value: "All", label: "All"});

    return cats;
  }

  handlePostcodeInputEnterPress = (evt: any): void => {
    if (evt.key === "Enter") {
      this.props.handlePostcodeChange(this.props.postcode);
    }
  }

  handleInputChange = (evt: any): void => {
    this.setState({...this.state, postcode: evt.target.value});
  }

  renderArrow = (render: any): JSX.Element => {
    let classes = [];
    if (render.isOpen) classes.push("open");
    return <ArrowThin classes={classes} />;
  }

  handleCategoryChange = (option: any): void => {
    this.props.setSelectedCategory(option.value);

    this.props.handleCategoryChange(option.value);
  }

  render(): JSX.Element {
    const treatment = this.props.categories.find(cat => this.props.selectedCategory && this.props.selectedCategory.toLocaleLowerCase() === cat.name.toLowerCase()) || {name: ""};
    const label = this.props.selectedCategory && this.props.selectedCategory.length !== 0 ? this.props.selectedCategory : "All";
    const value = {
      label,
      value: this.props.selectedCategory || ""
    };

    const md = new MobileDetect(window.navigator.userAgent);

    return (
      <div id="top-section" className="search-top">
        <div className="row justify-content-center">
          <div className="col-xs-12 col-md-10">
            <p>Searching for </p>
            {md.mobile() && <br/>}
            <Select
              name="treatment-select"
              onChange={(option: any) => this.handleCategoryChange(option)}
              value={value}
              options={this.getCategories()}
              clearable={false}
              arrowRenderer={this.renderArrow}
              autosize={true}
              searchable={false}
              style={{width: this.state.inputWidth}} />
            {md.mobile() && <br/>}
            <p> in </p>
            <div className="input-wrapper">
              <input id="postcodeInput" onChange={(evt: any) => this.props.handlePostcodeUpdate(evt.target.value.toUpperCase())} type="text" onKeyDown={this.handlePostcodeInputEnterPress} value={this.props.postcode} placeholder={this.props.geoFail || this.props.postcode === "" ? "Postcode" : ""} />
              <LineLoader isBusy={this.state.postcodeBusy} />
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-md-7 col-sm-12">
            <h2 className="header-text">Book London's best {treatment.name.toLowerCase()} therapists to your door, for five star manicures, blowdries, massages and more...</h2>
          </div>
        </div>
      </div>
    );
  }
}
