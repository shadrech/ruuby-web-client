import * as React from "react";
import { Link, browserHistory } from "react-router";
import * as moment from "moment";
import * as $ from "jquery";
import { Arrow } from "../../components/sub/svgs";
import {ApiProvider} from "../../api/provider";

interface Props {
  provider: ApiProvider;
  category?: string;
  setSelectedTime?: (date: moment.Moment) => void;
}
interface State {
  expand: boolean;
  beltWidth: number;
}

export default class SearchResult extends React.Component<Props, State> {
  private slots: HTMLDivElement[] = [];
  private beltWrapper: HTMLDivElement;
  state: State = {
    expand: false,
    beltWidth: 300
  };

  componentDidMount() {
    if (this.props.provider)
      this.setBeltWidth();
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.provider.availability !== prevProps.provider.availability)
      this.setBeltWidth();
  }

  setBeltWidth = (): void => {
    const width = this.slots.reduce((amount: number, div) => (amount + div.clientWidth), 0);
    this.setState({beltWidth: width + (this.slots.length * 11)});
  }

  toggleDescriptionExpand = (event: any): void => {
    event.preventDefault();

    this.setState({
      expand: !this.state.expand,
    });
  }

  handleTimeSelect = (time: moment.Moment, to: string): void => {
    this.props.setSelectedTime(time);
    browserHistory.push(to);
  }

  handleTimeArrow = (evt: any, direction: string): void => {
    evt.preventDefault();
    evt.stopPropagation();

    let offset = this.beltWrapper.clientWidth - 50;
    if (direction === "left") offset *= -1;
    $(this.beltWrapper).animate({
      scrollLeft: $(this.beltWrapper).scrollLeft() + offset
    }, 300);
  }

  render(): JSX.Element {
    const { therapist, salon, availability } = this.props.provider;
    const cat = this.props.category;
    let categories = ((cat && cat === "") || !cat) ? salon.categories.map(cat => cat.name).join(" • ") : salon.categories.find(s => s.name === this.props.category) ? salon.categories.find(s => s.name === this.props.category).subCategories.map(service => service).join(" • ") : salon.categories.map(cat => cat.name).join(" • ");
    if (!categories.length) categories = salon.categories.map(cat => cat.name).join(" • ");
    const {name} = salon.isMobile ? therapist : salon;

    const to = `/provider/${therapist.urn}`;

    let insight = this.props.provider.salon.insight;

    if (insight && !this.state.expand) {
      const len = Math.min(insight.length, 170);
      insight = insight.substr(0, len) + "... ";
    }

    let addrStr: string;
    if (!salon.isMobile) {
      addrStr = `${salon.address.address1.replace(",", "").trim()},`;
      if (salon.address.address2 && salon.address.address2.length)
        addrStr += ` ${salon.address.address2.replace(",", "").trim()},`;
      if (salon.address.city && salon.address.city.length)
        addrStr += ` ${salon.address.city.replace(",", "").trim()},`;
      addrStr += ` ${salon.address.postcode.replace(",", "").trim()}`;
    }

    this.slots = [];

    return (
      <Link to={to} className="col-md-5 col-xs-12 search-result">
        <div className="top">
          <img src={salon.profileImage} className="img" alt={name} />
          <div className="txt">
            <p>{name}</p>
            <p className="address">{categories}</p>
            {!salon.isMobile && <p>{addrStr}</p>}
          </div>
          <div className="rating">
            {salon.rating && <p>{salon.rating}</p>}
          </div>
        </div>
        <hr/>
        <div className="middle">
          <p className="description">
            {insight}
            {(!this.state.expand) ?
              <span className="readMore" onClick={this.toggleDescriptionExpand}>Read More ››</span>
            : <span className="readMore" onClick={this.toggleDescriptionExpand}> Read Less ‹‹</span>}
          </p>
          {this.props.setSelectedTime &&
            <div className="availability-slots-wrapper">
              <div className="left-arrow" onClick={evt => this.handleTimeArrow(evt, "left")}><Arrow /></div>
              <div className="slots-gradient-left"></div>
              <div className="availability-slots" ref={div => this.beltWrapper = div}>
                <div className="belt" style={{width: `${this.state.beltWidth}px`}}>
                  {availability && availability.map(a => {
                    const time = moment(a);
                    return <div key={a} className="slot" ref={div => div !== null ? this.slots.push(div) : null} onClick={() => this.handleTimeSelect(time, to)}>
                      <p>{time.format("h:mm A")}</p>
                    </div>;
                  })}
                </div>
              </div>
              <div className="slots-gradient-right"></div>
              <div className="right-arrow" onClick={evt => this.handleTimeArrow(evt, "right")}><Arrow /></div>
            </div>
          }
        </div>
        <hr className="bottom-divider" />
        <div className="btn">
          <p>Select</p>
        </div>
      </Link>
    );
  }
}
