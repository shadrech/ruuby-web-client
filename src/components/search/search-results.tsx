import * as React from "react";
const ReactEmoji = require("react-emoji");
import * as FlipMove from "react-flip-move";
import * as moment from "moment";
import SearchResult from "./search-result";
import { ApiProvider } from "../../api/provider";

interface SearchResultsProps {
  providers: ApiProvider[];
  category: string;
  isBusy: boolean;
  isInvalidPostcode: boolean;
  setSelectedTime: (date: moment.Moment) => void;
}

class SearchResults extends React.Component<SearchResultsProps, {}> {
  renderNoResults() {
    return (
      <div className="no-results">
        <p>
          We're fully booked at this time, or we may not serve your area. Please try a different date, or another postcode! {ReactEmoji.emojify(":D")}
        </p>
        <p>
          Beauty emergency? Contact our <a href="mailto:concierge@ruuby.com">concierge team</a> and we'll do our best to find you somebody ASAP.
        </p>
      </div>
    );
  }

  renderInvalidPostcode() {
    return(
      <div className="no-results">
        <p>Please enter your postcode above to view appointments in your area...</p>
      </div>
    );
  }

  renderResults() {
    return (this.props.providers) ?
      this.props.providers.map((provider: ApiProvider, i: number) =>
          <SearchResult
            key={i}
            provider={provider}
            category={this.props.category}
            setSelectedTime={this.props.setSelectedTime} />
      ) : [];
  }

  render() {
    return (
      <div id="results-section">
        <FlipMove duration={750} easing="ease-in-out" className="row results-row justify-content-between">
          {this.props.isInvalidPostcode && !this.props.isBusy && this.renderInvalidPostcode()}
          {!this.props.isInvalidPostcode && this.renderResults()}
          {!this.props.isInvalidPostcode && !this.props.isBusy && (this.props.providers.length === 0) && !this.props.isBusy && this.renderNoResults()}
        </FlipMove>
      </div>
    );
  }
}

export default SearchResults;
