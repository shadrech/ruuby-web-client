import * as React from "react";
import * as moment from "moment";
import * as _ from "lodash";
import { ArrowThin } from "../sub/svgs";
import { slotTypes } from "../../reducers/booking/reducer";

interface Props {
  category: string;
  date: string;
  slot: slotTypes;
  handleSlotChange: (slot: slotTypes) => void;
  handleDateChange: (date: moment.Moment) => void;
  handleCategoryChange: (category: string) => void;
  handlePostcodeChange: (postcode: string) => void;
}
interface State {
  active: string;
}

export default class SearchFilter extends React.Component<Props, State> {
  private belt: HTMLDivElement;
  state = {
    active: ""
  };

  handleTreatmentCarouselNext() {
    const length = this.belt.clientWidth - 30;
    this.handleTreatmentCarousel(this.belt, length);
  }

  handleTreatmentCarouselPrevious() {
    const length = this.belt.clientWidth - 30;
    this.handleTreatmentCarousel(this.belt, -length);
  }

  handleTreatmentCarousel(selector, offset) {
    $(selector).animate({
          scrollLeft: $(selector).scrollLeft() + offset
        }, 300
    );
  }

  renderDates() {
    const today = moment();
    const dates = [today];
    _.range(1, 28).forEach(index => {
      dates.push(today.clone().add(index, "day"));
    });

    let active = moment(this.props.date);

    const dateComps = dates.map((date: moment.Moment, i: number) =>
      <div key={i} className={`date ${(active.format("DMY") === date.format("DMY") ? " active" : "")}`} onClick={() => this.props.handleDateChange(date)}>
        <figure className="month">{ (date.date() === 1 || i === 0 || date.day() === 1) ? date.format("MMM") : ""}</figure>
        <p className="day-string">{date.format("ddd")}</p>
        <p className="day-number">{date.format("D")}</p>
        <p>•</p>
      </div>
    );

    return (
      <div className="col-xs-12 dates-selection">
        <div className="selection-wrapper">
          <div className="left-arrow" onClick={() => this.handleTreatmentCarouselPrevious()}><ArrowThin /></div>
          <div className="right-arrow" onClick={() => this.handleTreatmentCarouselNext()}><ArrowThin /></div>
          <div className="selection-belt-wrapper" ref={div => this.belt = div}>
            <div className="belt-gradient-left"></div>
            <div className="belt-gradient-right"></div>
            <div className="selection-belt">
              {dateComps}
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderTimes = (): JSX.Element => {
    const handleTimeClick = (time: any) => {
      this.props.handleSlotChange(time === "All Day" ? "All-Day" : time);
    };
    const timeComps = ["Morning", "Afternoon", "Evening", "All Day"].map((time: any, i: number) => {
      const active = time === this.props.slot || (this.props.slot === "All-Day" && time === "All Day");
      return (
        <div key={i} className={`slot ${active ? "selected" : ""}`} onClick={() => handleTimeClick(time)}>
          <p>{time}</p>
        </div>
      );
    });

    return (
      <div className="col-xs-12 slots-selection">
        <div className="slots">
          {timeComps}
        </div>
      </div>
    );
  }

  assignActive = (active: "slot" | "date"): void => {
    if (active === this.state.active) {
      this.setState({active: ""});
      return;
    }
    this.setState({active});
  }

  render() {
    const {active} = this.state;

    let renderedActive;
    if (active === "slot") renderedActive = this.renderTimes();
    else if (active === "date") renderedActive = this.renderDates();
    else renderedActive = "";

    const date = moment(this.props.date);

    const slot = this.props.slot === "All-Day" ? "All Day" : this.props.slot;

    return (
      <div id="filter-section">
        <div className="row top">
          <div className={`col-md-6 col-xs-6 col-time ${(this.state.active === "slot") ? "active" : ""}`} onClick={() => this.assignActive("slot")}>
            <div className="col-wrapper">
              <div className="text">
                <p>Time</p>
                <p>{slot}</p>
              </div>
              <div className="icon">
                <img src={require("../../assets/imgs/clock.png")} alt="Clock Icon" />
              </div>
            </div>
          </div>
          <div className={`col-md-6 col-xs-6 col-date ${(this.state.active === "date") ? "active" : ""}`} onClick={() => this.assignActive("date")}>
            <div className="col-wrapper">
              <div className="text">
                <p>Date</p>
                <p>{date.format("ddd\, MMM\ D")}</p>
              </div>
              <div className="icon">
                <img src={require("../../assets/imgs/calendar.png")} alt="Calendar Icon" />
              </div>
            </div>
          </div>
        </div>
        <div className="row bottom">
          {renderedActive}
        </div>
      </div>
    );
  }
}
