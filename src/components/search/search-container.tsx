import * as React from "react";
import { bindActionCreators } from "redux";
import { connect, Dispatch } from "react-redux";
import * as moment from "moment";

import LineLoader from "../sub/line-loader";
import SearchTop from "./search-top";
import SearchFilter from "./search-filter";
import SearchResults from "./search-results";
import { browserHistory } from "react-router";
import { constructUrl } from "../../routes";
import {validatePostcode} from "../../api";
import {fetchCategories} from "../../reducers/categories/actions";
import {fetchProviders, setBusyness, setUnbusyness} from "../../reducers/providers/actions";
import { actionCreators } from "../../reducers/booking/actions";
import {ApiCategory} from "../../api/categories";
import {ApiProvider} from "../../api/provider";
import { slotTypes } from "../../reducers/booking/reducer";
import { setTitleByUrl } from "../../utils/meta-controller";

export interface SearchResultsQueryParams {
  slot: slotTypes;
  category: string;
  date: string;
  postcode?: string;
  clear?: boolean;
}

interface Props {
  isBusy: boolean;
  location: Location;
  providers: ApiProvider[];
  category: string;
  date: string;
  slot: slotTypes;
  postcode: string;
  categories: ApiCategory[];
  selectedDate: moment.Moment;
  selectedSlot: slotTypes;
  selectedPostcode: string;
  fetchCategories: () => void;
  fetchProviders: (category: string, slot: string, date: string, postcode: string) => void;
  setBusyness: () => void;
  setUnbusyness: () => void;
  setSelectedCategory: (cat: string) => void;
  setSelectedSlot: (slot: slotTypes) => void;
  setSelectedDate: (date: moment.Moment) => void;
  setSelectedTime: (time: moment.Moment) => void;
  clearSelectedPostcode: () => void;
  clearSelectedTime: () => void;
}

interface State {
  postcode?: string;
  isInvalidPostcode?: boolean;
  geoFail?: boolean;
}

class SearchResultsContainer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      postcode: "",
      isInvalidPostcode: (props.postcode) ? false : true,
      geoFail: false
    };
  }

  componentWillMount() {
    if (this.props.categories.length === 0) this.props.fetchCategories();

    this.fetchProviders(
      this.props.category,
      this.props.slot,
      this.props.date,
      this.props.postcode
    );

    setTitleByUrl(window.location.pathname);
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.selectedPostcode && nextProps.selectedPostcode !== null)
      this.setState({
        postcode: this.props.postcode
      });

    if (
      (nextProps.slot !== this.props.slot) ||
      (nextProps.category !== this.props.category) ||
      (nextProps.date !== this.props.date) ||
      (nextProps.postcode !== this.props.postcode)
    ) {
      this.fetchProviders(
        nextProps.category,
        nextProps.slot,
        nextProps.date,
        nextProps.postcode
      );
    }

    setTitleByUrl(window.location.pathname);
  }

  componentDidMount() {
    if (!Array.isArray(this.props.categories))
      this.props.fetchCategories();

    if (this.props.slot)
      this.props.setSelectedSlot(this.props.slot);
    if (this.props.date)
      this.props.setSelectedDate(moment(this.props.date));

    this.props.clearSelectedTime();
  }

  fetchProviders(category: string, slot: string, date: string, postcode: string) {
    if (postcode) {
      this.props.fetchProviders(category, slot, date, postcode);
    }
  }

  handleDateChange = (date: moment.Moment) => {
    this.props.setSelectedDate(date);
    this.setNewUrl("date", date.format("YYYY-MM-DD"));
  }

  handleCategoryChange = (category: string) => {
    this.setNewUrl("category", category);
  }

  handleSlotChange = (slot: slotTypes) => {
    this.props.setSelectedSlot(slot);
    this.setNewUrl("slot", slot);
  }

  updatePostcode = (postcode: string) => {
    this.setState({postcode});
  }

  handlePostcodeChange = (postcode: string) => {
    if (postcode.length === 0) {
      this.props.clearSelectedPostcode();
      this.setState({
        isInvalidPostcode: true
      });
      this.setNewUrl("postcode", "");
      return;
    }

    validatePostcode(postcode)
      .then(valid => {
        if (valid) {
          this.setState({
            postcode,
            isInvalidPostcode: false
          });
          this.setNewUrl("postcode", postcode);
        } else {
          this.setState({isInvalidPostcode: true});
        }
      });
  }

  setNewUrl = (newKey: string, newValue: string) => {
    const params: SearchResultsQueryParams = {
      category: this.props.category.toString(),
      slot: this.props.slot,
      date: this.props.date,
    };

    params[newKey] = newValue;

    let url = `${constructUrl("search")}/${params.category}/${params.date}/${params.slot}`;

    if (newKey === "postcode")
      url += `/${newValue}`;
    else if (this.props.postcode)
      url += `/${this.props.postcode}`;
    else if (this.props.selectedPostcode)
      url += `/${this.props.selectedPostcode}`;

    browserHistory.push(url);
  }

  setGeoFail = () => {
    this.setState({
      geoFail: true,
      isInvalidPostcode: true
    });
  }

  render() {
    return (
      <div className="container-fluid ruuby-search">
        <SearchTop
          handleCategoryChange={this.handleCategoryChange}
          handlePostcodeChange={this.handlePostcodeChange}
          categories={this.props.categories}
          selectedCategory={this.props.category}
          postcode={this.state.postcode}
          propsPostcode={this.props.postcode}
          handlePostcodeUpdate={this.updatePostcode}
          setBusyness={this.props.setBusyness}
          setUnbusyness={this.props.setUnbusyness}
          setSelectedCategory={this.props.setSelectedCategory}
          geoFail={this.state.geoFail}
          setGeoFail={this.setGeoFail}
        />
        <SearchFilter
          handlePostcodeChange={this.handlePostcodeChange}
          handleSlotChange={this.handleSlotChange}
          handleDateChange={this.handleDateChange}
          handleCategoryChange={this.handleCategoryChange}
          category={this.props.category}
          date={this.props.date}
          slot={this.props.slot}
        />
        <LineLoader isBusy={this.props.isBusy} />
        <SearchResults
          category={this.props.category}
          providers={this.props.providers}
          isBusy={this.props.isBusy}
          isInvalidPostcode={this.state.isInvalidPostcode}
          setSelectedTime={this.props.setSelectedTime}
        />
      </div>
    );
  }
}

function mapStateToProps(state: any, ownProps: any) {
  return {
    categories: state.categoriesState.get("items"),
    providers: state.providerState.get("items").toJS(),
    isBusy: state.providerState.get("isBusy"),
    category: ownProps.params.category,
    postcode: ownProps.params.postcode,
    date: ownProps.params.date,
    slot: ownProps.params.slot,
    selectedDate: state.bookingState.selectedDate,
    selectedSlot: state.bookingState.selectedSlot,
    selectedPostcode: state.bookingState.selectedPostcode
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>) {
  return bindActionCreators({
    fetchProviders,
    fetchCategories,
    setBusyness,
    setUnbusyness,
    setSelectedCategory: actionCreators.setSelectedCategory,
    setSelectedDate: actionCreators.setSelectedDate,
    setSelectedSlot: actionCreators.setSelectedSlot,
    setSelectedTime: actionCreators.setSelectedTime,
    clearSelectedPostcode: actionCreators.clearSelectedPostcode,
    clearSelectedTime: actionCreators.clearSelectedTime
  }, dispatch) as any;
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultsContainer);
