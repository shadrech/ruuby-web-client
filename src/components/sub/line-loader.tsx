import * as React from "react";

interface Props {
  isBusy: boolean;
}

const LineLoader: React.SFC<Props> = props => {
  return (props.isBusy) ? (
    <div id="load-bar">
      <div className="bar"></div>
      <div className="bar"></div>
      <div className="bar"></div>
    </div>
  ) : null;
};

export default LineLoader;