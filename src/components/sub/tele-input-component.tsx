import * as React from "react";
const ReactFlagsSelect = require("react-flags-select");
import "react-flags-select/css/react-flags-select.css";

interface Code {
  name: string;
  dial_code: string;
  code: string;
};
const codes: Code[] = require("../../../country-telephone-codes.json");
const countries = codes.map(c => c.code);
const customLabels = {};
codes.forEach(c => {
  customLabels[c.code] = c.dial_code;
});

interface Props {
  code: string;
  value: string;
  classNames?: {
    label: string;
  };
  onPhoneChange: (tele: string) => void;
  onCodeChange: (code: string) => void;
}

interface State {
  inputPadding: number;
}

export default class TeleInputComponent extends React.Component<Props, State> {
  private flagSelect: any;
  state: State = {
    inputPadding: 80
  };

  componentDidUpdate(prevProps: Props) {
    if (prevProps.code !== this.props.code) {
      this.setState({inputPadding: this.flagSelect.refs.selectedFlag.clientWidth});
    }
  }

  handleCodeSelect = (countryCode: string): void => {
    this.props.onCodeChange(codes.find(c => c.code === countryCode).dial_code);
  }

  render() {
    return (
      <div className="input-wrapper" id="tele-component">
        <label className={this.props.classNames ? this.props.classNames.label : ""}>Phone Number</label>
        <div className="tele-input-wrapper">
          <ReactFlagsSelect
            ref={select => this.flagSelect = select}
            defaultCountry="GB"
            countries={countries}
            customLabels={customLabels}
            onSelect={this.handleCodeSelect} />
          <input type="tel" name="phone" value={this.props.value} onChange={evt => this.props.onPhoneChange(evt.target.value)} style={{paddingLeft: `${this.state.inputPadding}px`}} />
        </div>
      </div>
    );
  }
}
