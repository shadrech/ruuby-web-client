import * as React from "react";
import * as Modal from "react-modal";
import * as Immutable from "immutable";

const style: Modal.Styles = {
  content: {
    border: "10px solid #d9dfdf",
    borderRadius: 0
  }
};

interface Props extends React.Props<{}> {
  style?: Modal.Styles;
  show: boolean;
  label: string;
  title?: string;
  closeModal: () => void;
}

export const ModalShell: React.SFC<Props> = (props: Props) => {
  const styles = props.style ? Immutable.fromJS(props.style).mergeDeep(Immutable.fromJS(style)).toJS() : style;
  return (
    <Modal
      isOpen={props.show}
      contentLabel={props.label}
      style={styles}
      onRequestClose={props.closeModal} >
      <div className="modal-close">
        <div className="row top-row">
          <div className="col-xs-12">
            {props.title && <h3>{props.title}</h3>}
            <div className="close-icon" onClick={props.closeModal}><img src={require("../../assets/imgs/closeIcon.png")} alt="Close modal"/></div>
          </div>
        </div>
        <div className="row child-row">
          {props.children}
        </div>
      </div>
    </Modal>
  );
};
