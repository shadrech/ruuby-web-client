import * as React from "react";
import {Phone } from "../sub/svgs";

import "./styles/index.scss";
import { setMetadata } from "../../utils/meta-controller/index";

const EMAIL_LINK = "mailto:partners@ruuby.com?subject=Application to join Team Ruuby&body=Please include your phone number and attach your CV and cover letter";

export class JoinContainer extends React.Component<{}, {}> {
  componentWillMount() {
    setMetadata("join");
  }

  openEmail() {
    window.location.replace(EMAIL_LINK);
  }

  render() {
    const button = (<button onClick={this.openEmail} className="apply-button">Apply now</button>);

    return (
      <div className="container-fluid join-container">
        <div className="header"></div>
        <div className="main">
          <div className="content">
            <h1>Join the Ruuby team</h1>

            <div className="quote">
              &#8220;Join London’s Leading Beauty Concierge and take charge of your freelance schedule – work where you want, when you want.&#8221;
            </div>

            <blockquote className="technician-quote">
              <p>“I've been working with Ruuby from May 2016 onwards and since then have built brilliant client relationships through the use of the app! My Ruuby clients always speak highly of the service and the quality of the treatments and it is always a pleasure visiting them time and again.”</p><p>- Lashaun, Nail Technician</p>
            </blockquote>

            {button}

            <h2>Join Ruuby Today</h2>

            <div className="as-a-section">
              <h3>Work when you want</h3>
              <p>Ruuby allows you to work when you want - you set your own hours</p>
            </div>
            <div className="as-a-section">
              <h3>Work with the best</h3>
              <p>
              We love our clients and we are sure you will too. Celebrities, VIPs, influencers and other clientele keep booking!
              </p>
            </div>
            <div className="as-a-section">
              <h3>Work quickly</h3>
              <p>
              Our app makes bookings slick and easy – you receive a summary of all your bookings including notifications, treatment details, customer info and location
              </p>
            </div>
            <div className="as-a-section">
              <h3>Work to get paid</h3>
              <p>
              Maximise earnings with Ruuby  - £1000 + per month on a part time basis and £4000+ per month if you work fulltime
              </p>
            </div>
            <div className="as-a-section">
              <h3>Work where you want</h3>
              <p>
              Choose the areas where you would like to work
              </p>
            </div>
            <div className="as-a-section">
              <h3>Work in style</h3>
              <p>
              Ruuby branded uniforms, towels, bags and other merchandise are provided
              </p>
            </div>

            <h2>Trainings &amp; Perks - The Ruuby Academy</h2>

            <ul className="horizontal-list perks">
              <li>Attend training hosted by our partner brands, including Bumble &amp; bumble, Bobbi Brown, MAC Cosmetics, Darphin, and BY TERRY</li>
              <li>Receive discounted rates for approved products</li>
            </ul>

            <h2>What you’ll need</h2>
            <div className="as-a-section">
              <p>
              We only work with the best. Our clients are highly discerning, and standards are very strict.
              </p>
              <p>
                Applications are always welcomed, and please supply evidence of the below, so we can consider your application.
              </p>
            </div>
            <ul className="horizontal-list">
              <li>Right to work in the country</li>
              <li>Accredited treatment qualification(s)</li>
              <li>Valid insurance for freelance &amp; mobile work</li>
              <li>Kit with Ruuby approved products</li>
              <li>iOS or Android smartphone</li>
            </ul>

            {button}

            <div className="contact">
              <a href={EMAIL_LINK}>partners@ruuby.com</a>&nbsp;&nbsp;•&nbsp;&nbsp;<div className="icon"><Phone /></div><a className="phone" href="tel:+4402074608319"> 0207 460 8319</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
