import * as React from "react";
import { Link, browserHistory } from "react-router";
import * as MobileDetect from "mobile-detect";
import * as Cookie from "js-cookie";
import * as moment from "moment";
const TimelineMax = require("gsap/src/uncompressed/TimelineMax");
require("gsap/src/uncompressed/TweenMax");
import {ApiCategory} from "../api/categories";
import {
  RuubyLogo,
  Phone,
  Profile,
  Arrow
} from "./sub/svgs";
import {ApiCustomer} from "../api/customer";
import {SearchResultsQueryParams} from "../components/search/search-container";
import {constructUrl} from "../routes";
import { slotTypes } from "../reducers/booking/reducer";

declare const Sine: any;
declare const Power2: any;

interface Props {
  customer: ApiCustomer;
  categories: ApiCategory[];
  location: {
    category: string;
    date: string;
    slot: slotTypes;
    postcode: string;
  };

  fetchCategories: () => void;
  setSelectedCategory: (cat: string) => void;
}

interface State {
  findIsOpen?: boolean;
  navIsOpen?: boolean;
}

export default class Header extends React.Component<Props, State> {
  private domObjs: any = {}; // DOM objects
  private md: MobileDetect = new MobileDetect(window.navigator.userAgent);

  constructor(props: Props) {
    super(props);
    this.state = {
      navIsOpen: false,
      findIsOpen: false
    };

    this.domObjs.catSubs = [];
  }

  componentDidMount() {
    if (!this.props.categories.length)
      this.props.fetchCategories();
  }

  componentDidUpdate(_prevProps: Props, prevState: State) {
    if (this.md.mobile()) {
      browserHistory.listen(() => {
        if (prevState.navIsOpen)
          this.closeNav();
      });
    }
  }

  setNewUrl = (category: string): void => {
    this.props.setSelectedCategory(category);

    const params: SearchResultsQueryParams = {
      category,
      slot: this.props.location.slot || "All-Day",
      date: this.props.location.date || moment().format("YYYY-MM-DD")
    };

    let url = `${constructUrl("search")}/${params.category}/${params.date}/${params.slot}`;
    if (this.props.location.postcode)
      url += `/${this.props.location.postcode}`;

    browserHistory.push(url);
  }

  handleFindToggle = (): void => {
    this.setState({findIsOpen: !this.state.findIsOpen});
  }

  closeNav = (): void => {
    const tl = new TimelineMax();
    tl.to(this.domObjs.navbar, 0.2, {height: "60px", ease: Sine.easeInOut})
      .to(this.domObjs.nav7, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav6, 0.15, {opacity: 0}, "-=0.2");

    if (this.md.mobile()) {
      tl.to(this.domObjs.catSubmenu, 0.15, {height: 0}, "-=0.4");
      if (this.isLoggedIn()) {
        tl.to(this.domObjs.accSubmenu, 0.15, {height: 0}, "-=0.4");
      }
    }

    tl.to(this.domObjs.nav9, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav8, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav7, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav6, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav5, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav4, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav3, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav2, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav1, 0.15, {opacity: 0}, "-=0.2")
      .to(this.domObjs.nav1, 0, {display: "block"})
      .to(this.domObjs.nav2, 0, {display: "block"})
      .to(this.domObjs.nav3, 0, {display: "block"})
      .to(this.domObjs.nav4, 0, {display: "block"})
      .to(this.domObjs.nav5, 0, {display: "block"})
      .to(this.domObjs.nav6, 0, {display: "block"})
      .to(this.domObjs.nav7, 0, {display: "block"})
      .to(this.domObjs.nav8, 0, {display: "block"})
      .to(this.domObjs.nav9, 0, {display: "block"})
      .to(this.domObjs.burger1, .1, {transform: "rotate(0)", ease: Power2.easeOut}, "-1")
      .to(this.domObjs.burger3, .1, {transform: "rotate(0)", ease: Power2.easeOut})
      .to(this.domObjs.burger2, 0, {display: "initial"})
      .to(this.domObjs.burger1, .1, {top: "2px", ease: Power2.easeOut})
      .to(this.domObjs.burger3, .1, {top: "18px", ease: Power2.easeOut});
  }

  toggleOpenMenu = (): void => {
    const tl = new TimelineMax();
    if (!this.state.navIsOpen) {
      tl.to(this.domObjs.burger1, .1, {top: "10px", ease: Sine.easeInOut})
      .to(this.domObjs.burger3, .1, {top: "10px", ease: Sine.easeInOut}, "-=0.1")
      .to(this.domObjs.burger2, 0, {display: "none"})
      .to(this.domObjs.burger1, .1, {transform: "rotate(45deg)", ease: Sine.easeInOut})
      .to(this.domObjs.burger3, .1, {transform: "rotate(-45deg)", ease: Sine.easeInOut})
      .to(this.domObjs.navbar, .2, {height: `${window.innerHeight}px`, ease: Sine.easeIn}, "-=0.4")
      .to(this.domObjs.nav1, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav2, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav3, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav4, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav5, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav6, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav7, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav8, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav9, 0, {display: "block"}, "-=0.6")
      .to(this.domObjs.nav1, 0.5, {opacity: 1}, "-=0.5")
      .to(this.domObjs.nav2, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav3, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav4, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav5, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav6, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav7, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav8, 0.5, {opacity: 1}, "-=0.25")
      .to(this.domObjs.nav9, 0.5, {opacity: 1}, "-=0.25");

      this.setState({navIsOpen: true});
    } else {
      this.closeNav();
      this.setState({navIsOpen: false});
    }
  }

  isLoggedIn = (): boolean => {
    return (this.props.customer !== null) || (Cookie.get("token") !== undefined);
  }

  openAccSubmenu = () => {
    const tl = new TimelineMax();
    tl.to(this.domObjs.accSubmenu, 0.7, {height: "12em"})
    .to(this.domObjs.sub1, 0.3, {opacity: 1}, "-=0.5")
    .to(this.domObjs.sub2, 0.3, {opacity: 1}, "-=0.4")
    .to(this.domObjs.sub3, 0.3, {opacity: 1}, "-=0.4");
  }

  openCatSubmenu = () => {
    const tl = new TimelineMax();
    tl.to(this.domObjs.catSubmenu, 0.5, {height: "36em"});
    for (let i = 0; i < this.domObjs.catSubs.length; i++)
      tl.to(this.domObjs.catSubs[i], 0.3, {opacity: 1}, "-=0.5");
  }

  render() {
    const isSmallerThanTablet = screen.width <= 991;

    return (
      <header id="new-header" className="home-navbar" ref={elem => this.domObjs.header = elem} style={{top: 0}}>
        <nav className="navbar-home black" ref={elem => this.domObjs.navbar = elem}>
          <div className="navbar-container">
            <Link className="ruuby-logo-link nav-item" to="/">
              <RuubyLogo />
            </Link>
            <div className="nav-burgar" onClick={this.toggleOpenMenu}>
              <div className="burgar-wrapper">
                <div className="top" ref={elem => this.domObjs.burger1 = elem}></div>
                <div className="middle" ref={elem => this.domObjs.burger2 = elem}></div>
                <div className="bottom" ref={elem => this.domObjs.burger3 = elem}></div>
              </div>
            </div>
            <div className="divider-horizontal" ref={elem => this.domObjs.nav1 = elem}></div>
            <div className="nav-items">
              <div className={`nav-item nav-sub lbb white`} ref={elem => this.domObjs.nav2 = elem}>
                {isSmallerThanTablet ? <div className="lbb-a" onClick={() => this.md.mobile() ? this.openCatSubmenu() : true}>Little Black Book <Arrow /></div> : <div onClick={() => this.setNewUrl("All")} className="lbb-a">Little Black Book <Arrow /></div>}
                <div className="nav-submenu" ref={elem => this.domObjs.catSubmenu = elem}>
                  {this.props.categories.map((cat, i: number) =>
                    <div key={cat.code} className="nav-item" ref={elem => this.domObjs.catSubs[i] = elem}>
                      <div className="cat-link" onClick={() => this.setNewUrl(cat.name)}>{cat.name}</div>
                    </div>
                  )}
                </div>

                <div className="sub-container">
                  {this.props.categories && this.props.categories.map((cat, i: number) => <div key={cat.code} className={`cat${i} cat-link`} onClick={() => this.setNewUrl(cat.name)}>{cat.name}</div>)}
                </div>
              </div>
              <div className="nav-item nav-sub" ref={elem => this.domObjs.nav3 = elem}>
                <a href="https://shop.ruuby.com/">Gifting</a>
              </div>
              <div className="nav-item nav-sub" ref={elem => this.domObjs.nav4 = elem}>
                <Link to="/about">About</Link>
              </div>
              <div className="nav-item nav-sub" ref={elem => this.domObjs.nav5 = elem}>
                <Link to="/price-list">Price List</Link>
              </div>
              <div className="nav-item nav-sub" ref={elem => this.domObjs.nav6 = elem}>
                <a href="/magazine">Magazine</a>
              </div>
              <div className="nav-item nav-sub apply-to-partner" ref={elem => this.domObjs.nav7 = elem}>
                <Link to="/join">Join Our Team</Link>
              </div>
              {this.isLoggedIn() ?
                <div className="nav-item nav-sub" ref={elem => this.domObjs.nav8 = elem}>
                  <div className="nav-icon">
                    <Profile />
                  </div>
                  {this.md.mobile() ? <div className="nav-profile" onClick={this.openAccSubmenu}><p>My Account </p><Arrow /></div> : <Link to="/profile">My Account</Link>}
                  <div className="nav-submenu" ref={elem => this.domObjs.accSubmenu = elem}>
                    <div className="nav-item" ref={elem => this.domObjs.sub1 = elem}>
                      <Link to="/profile">Profile</Link>
                    </div>
                    <div className="nav-item" ref={elem => this.domObjs.sub2 = elem}>
                      <Link to="/profile/favourites">Favourites</Link>
                    </div>
                    <div className="nav-item" ref={elem => this.domObjs.sub3 = elem}>
                      <Link to="/profile/bookings">Bookings</Link>
                    </div>
                  </div>
                </div>
              :
                <div className="nav-item nav-sub" ref={elem => this.domObjs.nav8 = elem}>
                  <Link to="/login">Login</Link>
                </div>
              }
              <div className="nav-item nav-sub" ref={elem => this.domObjs.nav9 = elem}>
                <div className="nav-icon">
                  <Phone />
                </div>
                <a href="tel:+4402074608319">0207 460 8319</a>
              </div>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}
