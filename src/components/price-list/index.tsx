import * as React from "react";
import {Link} from "react-router";
import { setMetadata } from "../../utils/meta-controller/index";

interface Category {
  name: string;
  treatments: {
    name: string;
    price: string;
  }[];
  info?: string;
}

const categories: Category[] = [
  {
    name: "Hair",
    treatments: [
      {name: "Blowdry", price: "£45.00"},
      {name: "Up do / Curls / Straightening", price: "£55.00"},
      {name: "Braids", price: "£40.00" },
      {name: "Braids & Blowdry", price: "£60.00"},
      {name: "Cuts & Blowdry", price: "£90.00"}
    ]
  }, {
    name: "Makeup",
    treatments: [
      {name: "Day Makeup", price: "£55.00"},
      {name: "Evening Makeup", price: "£70.00"},
      {name: "Hair & Makeup", price: "£105.00"},
      {name: "Makeup Lesson", price: "from £90.00"},
      {name: "Add on Lashes", price: "£10.00"}
    ]
  }, {
    name: "Nails",
    treatments: [
      {name: "Classic Manicure", price: "£30.00"},
      {name: "Classic Pedicure", price: "£38.00"},
      {name: "Classic Manicure & Pedicure", price: "£50.00"},
      {name: "Mini Manicure (File & Polish)", price: "£15.00"},
      {name: "Gel Manicure", price: "£35.00"},
      {name: "Gel Pedicure", price: "£43.00"},
      {name: "Gel Manicure & Gel Pedicure", price: "£80.00"},
      {name: "Gel Manicure & Classic Pedicure", price: "£68.00"},
      {name: "Classic Manicure & Gel Pedicure", price: "£70.00"},
      {name: "Gel Removal (Hands or Feet)", price: "£10.00"},
      {name: "Manicure with Nail Art", price: "£45.00"}
    ]
  }, {
    name: "Eyes",
    treatments: [
      {name: "Lash Tint", price: "£18.00"},
      {name: "Brow Tint", price: "£15.00"},
      {name: "Lash & Brow Tint", price: "£28.00"},
      {name: "Threading (One Area)", price: "£15.00"},
      {name: "Full Set Lash Extensions (Mink/Synthetic)", price: "£110.00"},
      {name: "Full Set Lash Extensions (Russian)", price: "£140.00"},
      {name: "Half Set Lash Extensions", price: "£60.00"},
      {name: "Removal of Lash Extensions", price: "£20.00"},
      {name: "Infill Lashes", price: "£50.00"},
      {name: "Cluster Lashes", price: "£55.00"},
      {name: "Lash Lift", price: "£65.00"}
    ]
  }, {
    name: "Tanning",
    treatments: [
      {name: "Spray Tan", price: "£45.00"},
      {name: "Spray Tan for Two", price: "£75.00"}
    ],
    info: "(Vita Liberata, St. Tropez, Sienna X)"
  }, {
    name: "Massage",
    treatments: [
      {name: "60 Minute Massage", price: "£65.00"},
      {name: "90 Minute Massage", price: "£75.00"},
      {name: "120 Minute Massage", price: "£95.00"},
      {name: "Back and Shoulder Massager", price: "£35.00"}
    ],
    info: "(Deep Tissue / Swedish / Aromatherapy Massage & More)"
  }, {
    name: "Facial",
    treatments: [
      {name: "45 Min Facial", price: "£45.00"},
      {name: "60 Minute Facial", price: "£55.00"},
      {name: "Darphin 60 Minute Facial", price: "£95.00"}
    ],
    info: "(Hydraskin / Lumière Essentielle / Anti-Ageing)"
  }, {
    name: "Waxing",
    treatments: [
      {name: "Brazilian Wax", price: "£45.00"},
      {name: "Hollywood Wax", price: "£50.00"},
      {name: "Bikini Wax", price: "£25.00"},
      {name: "Full Leg Wax", price: "£45.00"},
      {name: "Half Leg Wax", price: "£35.00"},
      {name: "Full Arm Wax", price: "£30.00"},
      {name: "Half Arm or Underarm Wax", price: "£15.00"},
      {name: "Eyebrow or Upperlip Wax", price: "£15.00"},
      {name: "Cheek or Chin Wax", price: "£10.00"},
      {name: "Stomach or Chest Wax", price: "£20.00"},
      {name: "Back Wax", price: "£35.00"},
      {name: "G-String Wax", price: "£35.00"}
    ]
  }, {
    name: "Wellness",
    treatments: [
      {name: "Botox/Fillers", price: "from £220.00"},
      {name: "Hangover Infusions", price: "from £74.00"},
      {name: "Beauty Infusions", price: "from £169.00"},
      {name: "Fitness Infusions", price: "from £269.00"},
      {name: "Longevity Infusions", price: "from £169.00"},
      {name: "Personal Training Session", price: "from £200.00"},
      {name: "1 Day Cleanse", price: "from £75.00"},
      {name: "3 Day Cleanse", price: "from £175.00"},
      {name: "5 Day Cleanse", price: "from £280.00"},
      {name: "Grace Belgravia Soup & Juice Cleanse", price: "£127.00"},
      {name: "Yoga/Pilates Session", price: "£75.00"}
    ]
  }
];

const Treatment: React.SFC<Category> = (props: Category) => {
  return (
    <div className="category">
      <Link to={`/${props.name}`}>{props.name.toUpperCase()}</Link>
      <table>
        {props.treatments.map((t, i: number) => (
          <tr key={i}>
            <td>{t.name}</td>
            <td>{t.price}</td>
          </tr>
        ))}
      </table>
      {props.info && <p>{props.info}</p>}
    </div>
  );
};

export default class TreatmentsContainer extends React.Component<{}, {}> {
  componentWillMount() {
    setMetadata("priceList");
  }

  render() {
    return (
      <div id="treatments-list" className="container-fluid">
        <div className="row banner">
          <div className="col-xs-12">
            <div className="header-img"></div>
          </div>
        </div>
        <div className="row title justify-content-md-center">
          <div className="col-xs-12">
            <h1>Ruuby Price List</h1>
          </div>
        </div>
        <div className="row price-lists">
          <div className="col-md-6">
            {categories.slice(0, categories.length / 2).map((cat, i:  number) => <Treatment {...cat} key={i} />)}
          </div>
          <div className="col-md-6">
            {categories.slice(categories.length / 2).map((cat, i:  number) => <Treatment {...cat} key={i} />)}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <p><span>&#42;</span> Prices vary depending on the selected therapist</p>
          </div>
        </div>
      </div>
    );
  }
}
