import * as React from "react";
import {Dispatch, bindActionCreators} from "redux";
import {connect} from "react-redux";
import {LoginTop} from "./login-top";
import LoginForm from "./login-form";
import {
  submitLoginForm,
  LoginParams
} from "../../../reducers/customer/actions";
import { setMetadata } from "../../../utils/meta-controller/index";

interface Props {
  submitLoginForm: (data: LoginParams) => void;
  errorMessage: string;
  isBusy: boolean;
}

class LoginContainer extends React.Component<Props, {}> {
  componentWillMount() {
    setMetadata("login");
  }

  render() {
    return (
      <div className="account-section">
        <LoginTop />
        <LoginForm
          submitLoginForm={this.props.submitLoginForm}
          errorMessage={this.props.errorMessage}
          isBusy={this.props.isBusy} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  errorMessage: state.customerState.get("error"),
  isBusy: state.customerState.get("isBusy")
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => (
  bindActionCreators({
    submitLoginForm
  } as any, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
