import * as React from "react";
// import {FacebookLogoWhite} from "../../sub/svgs";
import {LoginParams} from "../../../reducers/customer/actions";
import LineLoader from "../../sub/line-loader";

interface Props {
  submitLoginForm: (values: LoginParams, redirect?: boolean) => void;
  errorMessage: string;
  isBusy: boolean;
}

interface State {
  email?: string;
  password?: string;
}

export default class LoginForm extends React.Component<Props, State> {
  state = {
    email: "",
    password: ""
  };

  handleEmailChange = (evt: any) => {
    this.setState({email: evt.target.value});
  }

  handlePasswordChange = (evt: any) => {
    this.setState({password: evt.target.value});
  }

  handleSubmit = () => {
    this.props.submitLoginForm(this.state, true);
  }

  render() {
    return (
      <div className="form-wrapper">
        <form>
          <div className="input-wrapper">
            <label>Email Address</label>
            <input name="email" type="email" value={this.state.email} onChange={this.handleEmailChange} />
          </div>
          <div className="input-wrapper">
            <label>Password</label>
            <input type="password" value={this.state.password} onChange={this.handlePasswordChange} />
          </div>
          <div className="input-wrapper">
            <div className={`submit-btn${this.props.isBusy ? " disabled" : ""}`} onClick={this.handleSubmit}><p>Sign in</p></div>
            <LineLoader isBusy={this.props.isBusy} />
            {this.props.errorMessage && <label className="error">{this.props.errorMessage}</label>}
            <label className="forget"><a href="/forgot-password" target="_blank">Forgot your password?</a></label>
          </div>
          {/* <div className="input-wrapper">
            <div className="submit-btn facebook">
              <FacebookLogoWhite />
              <a>Login with Facebook</a>
            </div>
          </div> */}
        </form>
      </div>
    );
  }
}
