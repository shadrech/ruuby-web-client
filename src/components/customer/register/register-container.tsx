import * as React from "react";
import {Dispatch, bindActionCreators} from "redux";
import {connect} from "react-redux";
import {RegisterTop} from "./register-top";
import RegisterForm from "./register-form";
import {
  submitRegisterForm,
  setCustomerError
} from "../../../reducers/customer/actions";
import { setMetadata } from "../../../utils/meta-controller/index";

interface Props {
  submitRegisterForm: () => void;
  errorMessage: string;
  setCustomerError: () => void;
}

class LoginContainer extends React.Component<Props, {}> {
  componentWillMount() {
    setMetadata("register");
  }

  render() {
    return (
      <div className="account-section">
        <RegisterTop />
        <RegisterForm
          submitRegisterForm={this.props.submitRegisterForm}
          errorMessage={this.props.errorMessage}
          setCustomerError={this.props.setCustomerError} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  errorMessage: state.customerState.get("error")
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => (
  bindActionCreators({
    submitRegisterForm,
    setCustomerError
  } as any, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
