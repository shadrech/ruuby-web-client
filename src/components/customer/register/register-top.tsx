import * as React from "react";
import {Link} from "react-router";

export const RegisterTop = () => (
  <div id="top-section">
    <div className="row justify-content-center">
      <div className="col-xs-12 col-md-8 header">
        <h1>Register</h1>
      </div>
    </div>
    <div className="row justify-content-center">
      <div className="col-md-5 col-sm-12">
        <p className="header-text">Registered? <Link to="/login">Click here to login</Link></p>
      </div>
    </div>
    <hr/>
  </div>
);
