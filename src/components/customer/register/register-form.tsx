import * as React from "react";
import TelephoneComponent from "../../sub/tele-input-component";

interface Props {
  submitRegisterForm: (values: any, redirect: boolean) => void;
  errorMessage: string;
  setCustomerError: (error: string) => void;
}

interface State {
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  code: string;
  password?: string;
  confirm_password?: string;
}

export default class RegisterForm extends React.Component<Props, State> {
  state: State = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    password: "",
    confirm_password: "",
    code: "+44"
  };

  handleInputChange = (evt: any) => {
    switch (evt.target.name) {
      case "firstName":
        this.setState({firstName: evt.target.value});
        break;
      case "lastName":
        this.setState({lastName: evt.target.value});
        break;
      case "email":
        this.setState({email: evt.target.value});
        break;
      case "password":
        this.setState({password: evt.target.value});
        break;
      case "confirm_password":
        this.setState({confirm_password: evt.target.value});
        break;
      default:
        break;
    }
  }

  validate = (): string => {
    let err: string,
        {state} = this;

    if (state.password !== state.confirm_password) {
      err = "Passwords not matching";
      return err;
    }

    return err;
  }

  handleCodeChange = (code: string): void => {
    this.setState({code});
  }

  handlePhoneChange = (phone: string): void => {
    this.setState({phone});
  }

  handleSubmit = () => {
    const error = this.validate();
    if (error) {
      this.props.setCustomerError(error);
      return;
    }

    const data = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      phone: this.state.code + this.state.phone,
      password: this.state.password
    };
    this.props.submitRegisterForm(data, true);
  }

  render() {
    return (
      <div className="form-wrapper">
        <form>
          <div className="input-wrapper">
            <label>First Name</label>
            <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleInputChange} />
          </div>
          <div className="input-wrapper">
            <label>Last Name</label>
            <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleInputChange} />
          </div>
          <div className="input-wrapper">
            <label>Email Address</label>
            <input type="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
          </div>
          <TelephoneComponent
            value={this.state.phone}
            onCodeChange={this.handleCodeChange}
            onPhoneChange={this.handlePhoneChange}
            code={this.state.code} />
          <div className="input-wrapper">
            <label>Password</label>
            <input type="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
          </div>
          <div className="input-wrapper">
            <label>Password Confirm</label>
            <input type="password" name="confirm_password" value={this.state.confirm_password} onChange={this.handleInputChange} />
          </div>
          <div className="input-wrapper">
            <div className="submit-btn" onClick={this.handleSubmit}><p>Sign in</p></div>
            <label className="error">{this.props.errorMessage}</label>
          </div>
        </form>
      </div>
    );
  }
}
