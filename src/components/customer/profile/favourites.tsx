import * as React from "react";
import SearchResult from "../../search/search-result";
import {ApiProvider} from "../../../api/provider";

interface Props {
  providers: ApiProvider[];
}

export default class Profile extends React.Component<Props, {}> {
  render() {
    const favs = this.props.providers.filter(provider => provider.salon.isFavourite);

    return (
      <div id="profile" className="account-content">
        <div className="row justify-content-between">
          {favs.map((provider, i: number) =>
          <SearchResult key={i} provider={provider} />
          )}
          {favs.length === 0  && <p className="no-bookings" style={{textAlign: "center", width: "100%"}}>You have no favourited therapists</p>}
        </div>
      </div>
    );
  }
}
