import * as React from "react";
import { bindActionCreators, Dispatch } from "redux";
import { connect } from "react-redux";
import {ProfileTop} from "./profile-top";
import {ProfileNavbar} from "./profile-navbar";
import Profile from "./profile";
import Favourites from "./favourites";
import {
  updateCustomer,
  fetchCustomer,
  toggleEditingFlag,
  fetchFavourites,
  logout,
  fetchBookings
} from "../../../reducers/customer/actions";
import {
  fetchTransactions,
  cancelBooking,
  updateCustomerAttempt
} from "../../../reducers/customer/actions";
import {
  UpdatesStates,
} from "../../../reducers/customer/reducer";
import {ApiProvider} from "../../../api/provider";
import LineLoader from "../../sub/line-loader";
import { Bookings } from "./bookings";
import { Booking } from "./booking/index";
import { ApiBookingMapping, ApiCustomer } from "../../../api/customer";

interface Props {
  customer: ApiCustomer;
  page: string;
  urn: string;
  updateCustomer: () => void;
  updating: UpdatesStates;
  fetchCustomer: () => void;
  error: string;
  toggleEditingFlag: () => void;
  editing: UpdatesStates;
  favourites: ApiProvider[];
  fetchFavourites: () => void;
  logout: () => void;
  bookings: ApiBookingMapping;
  fetchBookings: () => void;
  fetchTransactions: (urn: string) => void;
  cancelBooking: (urn: string) => void;
  updateCustomerAttempt: Function;
}

class ProfileContainer extends React.Component<Props, {}> {
  componentDidMount() {
    if (!this.props.customer)
      this.props.fetchCustomer();

    if (!this.props.favourites)
      this.props.fetchFavourites();

    if (!this.props.bookings)
      this.props.fetchBookings();
  }

  renderProfileSection = (): JSX.Element => {
    switch (this.props.page) {
      case "favourites":
        return <Favourites providers={this.props.favourites} />;
      case "bookings":
        const booking = this.props.bookings[this.props.urn];
        if (this.props.urn)
          return <Booking
                    fetchTransactions={this.props.fetchTransactions}
                    urn={this.props.urn}
                    updating={this.props.updating}
                    booking={booking}
                    cancelBooking={this.props.cancelBooking} />;
        else
          return <Bookings
                    bookings={this.props.bookings} />;
      default:
        return <Profile
                  customer={this.props.customer}
                  updateCustomer={this.props.updateCustomer}
                  updating={this.props.updating}
                  errorMessage={this.props.error}
                  toggleEditingFlag={this.props.toggleEditingFlag}
                  editing={this.props.editing} />;
    }
  }

  render() {
    return (
      <div className="account-section">
        <ProfileTop
          customer={this.props.customer}
          logout={this.props.logout} />
        <ProfileNavbar
          page={this.props.page} />
        {this.props.customer && this.renderProfileSection()}
        <LineLoader isBusy={this.props.customer === null} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  customer: state.customerState.get("customer") ? state.customerState.get("customer").toJS() : null,
  favourites: state.customerState.get("favourites"),
  updating: state.customerState.get("updating").toJS(),
  page: ownProps.params.page,
  urn: ownProps.params.urn,
  error: state.customerState.get("error"),
  editing: state.customerState.get("editing").toJS(),
  providers: state.providerState.get("items").toJS(),
  bookings: state.customerState.get("bookings") ? state.customerState.get("bookings").toJS() : null
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => (
  bindActionCreators({
    updateCustomer,
    fetchCustomer,
    toggleEditingFlag,
    fetchFavourites,
    logout,
    fetchBookings,
    fetchTransactions,
    cancelBooking,
    updateCustomerAttempt
  }, dispatch) as any
);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
