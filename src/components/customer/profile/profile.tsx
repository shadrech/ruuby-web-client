import * as React from "react";
import { Tick, InputCross } from "../../sub/svgs";
import LineLoader from "../../sub/line-loader";
import {
  UpdatesStates
} from "../../../reducers/customer/reducer";
import {ApiCustomer} from "../../../api/customer";

interface Props {
  customer: ApiCustomer;
  updateCustomer: (params: object) => void;
  updating: UpdatesStates;
  errorMessage: string;
  editing: UpdatesStates;
  toggleEditingFlag: (key: string) => void;
}

interface State {
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
}

export default class Profile extends React.Component<Props, State> {
  private firstName;
  private email;
  private phone;

  constructor(props: Props) {
    super(props);

    this.state = this.getCustomerState(props);
  }

  getCustomerState = (props = this.props) => {
    const {customer} = props;
    return {
      firstName: customer.firstName,
      lastName: customer.lastName,
      email: customer.email,
      phone: customer.phone
    };
  }

  updateInputState = (evt: any, key: string) => {
    switch (key) {
      case "firstName":
        this.setState({firstName: evt.target.value});
        break;
      case "lastName":
        this.setState({lastName: evt.target.value});
        break;
      case "email":
        this.setState({email: evt.target.value});
        break;
      case "phone":
        this.setState({phone: evt.target.value});
        break;
    }
  }

  cancelEdit = (key: string) => {
    this.setState(this.getCustomerState());

    this.props.toggleEditingFlag(key);
  }

  updateCustomer = (keys: string[]) => {
    const params = {};
    for (let i = 0; i < keys.length; i++)
      params[keys[i]] = this.state[keys[i]];

    this.props.updateCustomer(params);
  }

  toggleEditingFlag = (key: string) => {
    if (!this.props.editing[key])
      (this[key === "names" ? "firstName" : key] as HTMLElement).focus();

    this.props.toggleEditingFlag(key);
  }

  render() {
    const multiplier = window.screen.width < 768 ? 11 : 13,
          fNameLength = (this.state.firstName.length || 1) * multiplier,
          lNameLength = (this.state.lastName.length || 1) * multiplier,
          emailLength = (this.state.email.length || 1) * multiplier,
          phoneLength = (this.state.phone.length || 1) * multiplier,
          customer = this.props.customer,
          abbreviation = customer.firstName[0].toUpperCase() + customer.lastName[0].toUpperCase();

    return (
      <div id="profile" className="account-content">
        <div className="row justify-content-md-center">
          <div className="col-md-6">
            <div className="profile-img">
              <h2>{abbreviation}</h2>
            </div>
            <div className="profile-data">
              <div className="animated_input name">
                <div className="input-wrapper" style={{width: `${fNameLength}px`}}>
                  <input type="text" ref={input => this.firstName = input} value={this.state.firstName} readOnly={this.props.editing.names ? false : true} onChange={evt => this.updateInputState(evt, "firstName")} />
                  {this.props.editing.names && !this.props.updating.names && <div className="underline"></div>}
                  <LineLoader isBusy={this.props.updating.names} />
                </div>
                <div className="input-wrapper" style={{width: `${lNameLength}px`}}>
                  <input type="text" value={this.state.lastName} readOnly={this.props.editing.names ? false : true} onChange={evt => this.updateInputState(evt, "lastName")} />
                  {this.props.editing.names && !this.props.updating.names && <div className="underline"></div>}
                  <LineLoader isBusy={this.props.updating.names} />
                  {this.props.errorMessage && this.props.editing.names && <label className="error">{this.props.errorMessage}</label>}
                </div>
                <div className="input-feedback">
                  {this.props.editing.names && <Tick onClick={() => this.updateCustomer(["firstName", "lastName"])} />}
                  {this.props.editing.names && <InputCross onClick={() => this.cancelEdit("names")} />}
                  {!this.props.editing.names && <img src={require("../../../assets/imgs/pen-icon.png")} onClick={() => this.toggleEditingFlag("names")} />}
                </div>
              </div>
              <div className="animated_input">
                <div className="input-wrapper" style={{width: `${emailLength}px`}}>
                  <input type="text" ref={input => this.email = input} value={this.state.email} readOnly={this.props.editing.email ? false : true} onChange={evt => this.updateInputState(evt, "email")} />
                  {this.props.editing.email && <div className="underline"></div>}
                  <LineLoader isBusy={this.props.updating.email} />
                  {this.props.errorMessage && this.props.editing.email && <label className="error">{this.props.errorMessage}</label>}
                </div>
                <div className="input-feedback">
                  {/* {this.props.editing.email && <Tick onClick={() => this.updateCustomer(["email"])} />}
                  {this.props.editing.email && <InputCross onClick={() => this.cancelEdit("email")} />}
                  {!this.props.editing.email && <img src={require("../../../../assets/imgs/pen-icon.png")} onClick={() => this.toggleEditingFlag("email")} />} */}
                </div>
              </div>
              <div className="animated_input">
                <div className="input-wrapper" style={{width: `${phoneLength}px`}}>
                  <input type="text" ref={input => this.phone = input} value={this.state.phone} readOnly={this.props.editing.phone ? false : true} onChange={evt => this.updateInputState(evt, "phone")} />
                  {this.props.editing.phone && <div className="underline"></div>}
                  <LineLoader isBusy={this.props.updating.phone} />
                  {this.props.errorMessage && this.props.editing.phone && <label className="error">{this.props.errorMessage}</label>}
                </div>
                <div className="input-feedback">
                  {this.props.editing.phone && <Tick onClick={() => this.updateCustomer(["phone"])} />}
                  {this.props.editing.phone && <InputCross onClick={() => this.cancelEdit("phone") } />}
                  {!this.props.editing.phone && <img src={require("../../../assets/imgs/pen-icon.png")} onClick={() => this.toggleEditingFlag("phone")} />}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
