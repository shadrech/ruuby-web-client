import * as React from "react";
import { Link } from "react-router";
import { ApiBooking } from "../../../api/booking";
import { ApiBookingMapping } from "../../../api/customer";

interface Props {
  bookings: ApiBookingMapping;
}

export class Bookings extends React.Component<Props, {}> {
  renderBooking = (booking: ApiBooking, i: number): JSX.Element => {
    let price = 0;
    let treatments = "";
    const {length} = booking.bookingTreatments;
    booking.bookingTreatments.forEach((t, i) => {
      price += (t.price ? t.price : t.treatment.price);
      treatments += t.treatment.name;
      if (length !== i + 1) treatments += ", ";
    });
    const address = booking.address ? `${booking.address.address1}, ${booking.address.postcode}` : `${booking.salon.address.address1}, ${booking.salon.address.postcode}`;

    return (
      <Link to={`/profile/bookings/${booking.urn}`} className="col-md-8 col-xl-7 col-booking" key={booking.urn + i}>
        <div className="booking">
          <div className="therapist-img">
            <img src={booking.salon.logoImages[0]} alt="booking therapist" />
          </div>
          <div className="info">
            <h4>{booking.salon.name}</h4>
            <p>{address}</p>
            <p>{`${booking.timeStarts.format("DD MMMM HH:mm")} - ${booking.timeEnds.format("HH:mm")}`}</p>
            <div className="bottom-info">
              <p className="treatments">{treatments}</p>
              <p className="price">{sprintf("£%0.2f", price)}</p>
            </div>
            {booking.status === "CANCELLED" && <p className="cancelled">CANCELLED</p>}
          </div>
        </div>
      </Link>
    );
  }

  render() {
    const completed = this.props.bookings ? Object.values(this.props.bookings).filter(b => b.completed) : [];
    const upcoming = this.props.bookings ? Object.values(this.props.bookings).filter(b => !b.completed) : [];

    return (
      <div id="profile" className="account-content">
        <div className="row justify-content-md-center">
          <div className="col-md-8 col-xl-7">
            <h3 className="bookings-header">Upcoming Bookings</h3>
            <hr/>
          </div>
        </div>
        <div className="row justify-content-md-center bookings-row">
          {upcoming.map(this.renderBooking)}
          {upcoming.length === 0 && <p className="no-bookings">You have no upcoming bookings</p>}
        </div>
        <div className="row justify-content-md-center">
          <div className="col-md-8 col-xl-7">
            <h3 className="bookings-header">Completed Bookings</h3>
            <hr/>
          </div>
        </div>
        <div className="row justify-content-md-center bookings-row">
          {completed.map(this.renderBooking)}
          {completed.length === 0 && <p className="no-bookings">You have no completed bookings</p>}
        </div>
      </div>
    );
  }
}
