import * as React from "react";
import { ApiCard } from "../../../../api/card";

interface Props {
  card: ApiCard;
}

export const Card: React.SFC<Props> = ({ card }) => {
  return (
    <div className="booking-card">
      <img src={card.imageUrl} alt="Card Type" />
      <div className="info">
        <p>{`${card.cardType} Ending in ${card.last4} - ${card.expirationMonth}/${card.expirationYear}`}</p>
      </div>
    </div>
  );
};
