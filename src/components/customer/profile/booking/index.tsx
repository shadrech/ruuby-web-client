import * as React from "react";
import { Link } from "react-router";
import {sprintf} from "sprintf-js";
import { ApiBooking } from "../../../../api/booking";
import { Treatment } from "./treatment";
import { Card } from "./card";
import Loader from "../../../sub/line-loader";
import { UpdatesStates } from "../../../../reducers/customer/reducer";
import { ArrowThin } from "../../../sub/svgs";

interface Props {
  booking: ApiBooking;
  urn: string;
  fetchTransactions: (urn: string) => void;
  cancelBooking: (urn: string) => void;
  updating: UpdatesStates;
}

export class Booking extends React.Component<Props, {}> {
  componentDidMount() {
    if (this.props.booking && !this.props.booking.cards && this.props.urn)
      this.props.fetchTransactions(this.props.urn);
  }

  render() {
    const { booking, updating } = this.props;
    const promos = booking.transactions ? booking.transactions.filter(t => t.type === "PROMO_CODE") : [];

    if (booking) {
      const { bookingTreatments, address, cards } = booking;
      const total = bookingTreatments.reduce((amount, t) => t.price ? amount + t.price : amount + t.treatment.price, 0);
      return (
        <div id="profile" className="account-content booking-content">
          <div className="row justify-content-md-center">
            <div className="col-xs-12 col-md-8 col-sm-9 header">
              <Link to="/profile/bookings" className="booking-back"><ArrowThin/></Link>
              {booking.status === "CANCELLED" && <p className="cancelled">CANCELLED</p>}
              <img src={booking.salon.logoImages[0]} alt="Booking Therapist" />
              <h4>{booking.salon.name}, {booking.timeStarts.format("Do MMMM")}, {booking.timeStarts.format("HH:mm")}</h4>
              <p>({bookingTreatments.length}) Treatments - £{total}</p>
            </div>
          </div>
          <div className="row">
            <hr/>
          </div>
          <div className="row justify-content-md-center profile-row">
            <div className="col-md-8 col-sm-9 treatments section">
              <h3>Treatments</h3>
              {bookingTreatments.map(treatment => <Treatment key={treatment.urn} treatment={treatment} />)}
            </div>
          </div>
          <div className="row justify-content-md-center profile-row">
            <div className="col-md-8 address section">
              <h3>Address</h3>
              <p>{`${address.address1}, ${address.postcode}`}</p>
            </div>
          </div>
          <div className="row justify-content-md-center profile-row">
            <div className="col-md-8 col-sm-9 cards section">
              <h3>Payment</h3>
              {cards && cards.length !== 0 ? cards.map((c, i) => <Card key={i} card={c} />) : <p>No transactions yet for this booking</p>}
            </div>
          </div>
          <div className="row justify-content-md-center profile-row">
            <div className="col-md-8 col-sm-9 promo-notes section">
              <div className="row">
                <div className="col-md-6">
                  <h3>Promo Codes</h3>
                  {promos.length === 0 ? <p>No promos added to booking</p> : promos.map(p => <p key={p.urn}>{`${sprintf("£%0.2f", p.amount)} OFF`}</p>)}
                </div>
                <div className="col-md-6">
                  <h3>Notes</h3>
                  <p>{booking.notes}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-md-center profile-row">
            <div className="col-md-8 col-sm-9 btns section">
              <div className="row">
                <div className="col-md-6">
                  {booking.status !== "CANCELLED" && !booking.completed && <button className={`ruuby-btn-dark cancel-btn ${updating.status ? "disabled" : ""}`} disabled={updating.status} onClick={() => this.props.cancelBooking(booking.urn)}>Cancel</button>}
                  <Loader isBusy={updating.status} />
                </div>
                <div className="col-md-6">
                  <a className="ruuby-btn-dark" href="tel:+4402074608319">Chat with Ruuby</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else
      return null;
  }
}
