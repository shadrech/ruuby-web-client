import * as React from "react";
import { ApiBookingTreatment } from "../../../../api/booking";

interface Props {
  treatment: ApiBookingTreatment;
}

export const Treatment: React.SFC<Props> = ({ treatment }) => {
  return (
    <div className="treatment">
      <div className="info">
        <p className="name">{treatment.treatment.name}</p>
        <p className="duration">{treatment.treatment.duration} mins</p>
      </div>
      <div className="price">
        <p>{treatment.price ? sprintf("£%0.2f", treatment.price) : sprintf("£%0.2f", treatment.treatment.price)}</p>
      </div>
      <hr/>
    </div>
  );
};
