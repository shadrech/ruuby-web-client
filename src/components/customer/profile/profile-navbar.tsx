import * as React from "react";
import { Link } from "react-router";

interface Props {
  page: string;
}

export const ProfileNavbar: React.SFC<Props> = ({ page }) => (
  <div id="profile-navbar">
    <div className="row nav-top">
      <div className="nav-wrapper">
        <Link to={"/profile"} className={`nav-item${!page ? " active" : ""}`}>
          <div className="nav-item-name">Profile</div>
        </Link>
        <Link to={"/profile/favourites"} className={`nav-item${page === "favourites" ? " active" : ""}`}>
          <div className="nav-item-name">Favourites</div>
        </Link>
        <Link to={"/profile/bookings"} className={`nav-item${page === "bookings" ? " active" : ""}`}>
          <div className="nav-item-name">Bookings</div>
        </Link>
      </div>
    </div>
  </div>
);
