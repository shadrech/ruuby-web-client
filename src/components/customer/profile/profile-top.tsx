import * as React from "react";
import {ApiCustomer} from "../../../api/customer";

interface Props {
  customer: ApiCustomer;
  logout: () => void;
}

export const ProfileTop: React.SFC<Props> = (props: Props) => {
  const logout = () => {
    props.logout();
  };

  return (
    <div id="top-section">
      <div className="row justify-content-center">
        <div className="col-xs-12 col-md-8 header">
          <h2>My account</h2>
          {props.customer && <h6>{`${props.customer.firstName} ${props.customer.lastName}`}</h6>}
          <p onClick={logout}>Logout</p>
        </div>
      </div>
    </div>
  );
};
