import * as React from "react";
import { Link, Element } from "react-scroll";
import { setMetadata } from "../../utils/meta-controller/index";

export default class FAQS extends React.Component<{}, {}> {
  componentWillMount() {
    setMetadata("faqs");
  }

  render() {
    return (
      <div className="container-fluid faqs-container hp-section-four">
        <div className="row faqs-row-wrapper">
          <div className="col-md-3 table-of-contents">
            <div className="contents">
              <Link activeClass="active" to="about" smooth={true} duration={500} offset={-50}>About Ruuby</Link>
              <Link activeClass="active" to="how" smooth={true} duration={500} offset={-50}>How to use Ruuby</Link>
              <Link activeClass="active" to="therapists" smooth={true} duration={500} offset={-50}>Beauty Therapists</Link>
              <Link activeClass="active" to="bookings" smooth={true} duration={500} offset={-50}>Bookings</Link>
              <Link activeClass="active" to="payments" smooth={true} duration={500} offset={-50}>Payments / Cancellations / Refunds</Link>
              <Link activeClass="active" to="other" smooth={true} duration={500} offset={-50}>Any other questions or feedback</Link>
            </div>
          </div>
          <div className="col-md-9">
            <Element name="about" className="row">
              <div className="col-xs-12">
                <h2 className="page-title">FAQS</h2>
                <h4 className="page-title about">About Ruuby</h4>
                <ul>
                  <li>
                    <p className="question">What is Ruuby?</p>
                    <p className="answer">Ruuby is London’s digital beauty concierge offering a range of beauty services at home, in salons or at any other location</p>
                  </li>
                  <li>
                    <p className="question">What kind of beauty therapists do you work with?</p>
                    <p className="answer">Ruuby only works with best-in class salons and beauty professionals from our Ruuby Collective (VIP) or Ruuby Elite (VVIP) teams. All our salons and beauty therapists are thoroughly vetted and tested in order to ensure the treatment will always be of the highest standards. Our beauticians are highly skilled and trained, with a broad range of experiences in editorial, fashion, and celebrity shoots and events</p>
                  </li>
                  <li>
                    <p className="question">What services does Ruuby offer?</p>
                    <p className="answer">Ruuby offers a wide range of premium beauty services such as facials, tanning, manicures, pedicures, nail art, acrylics, waxing, eyelash / eyebrow threading &amp; tinting, eyelash extensions, makeup (including makeup for weddings or special events) blowdrys, braids, updos, massages, vitamin infusions, botox, fillers and other wellness services such as yoga and pilates</p>
                  </li>
                  <li>
                    <p className="question">What products do your beauty therapists use and which brands do you work with?</p>
                    <p className="answer">Our beauty therapists only work with the best and most premium products. Some of these include Darphin, CND Shellac, Essie, Dior, Illamasqua, Giorgio Armani, Charlotte Tilbury, Tom Ford, Clinique, Bobbi Brown, Neal’s Yard, Sienna X spray tan and Vita Liberata Luxury spray tan</p>
                  </li>
                  <li>
                    <p className="question">What areas do you cover?</p>
                    <p className="answer">Ruuby currently covers central and greater London. Please fill in your postcode on the app or website to see who is available in your area. Please contact concierge@ruuby.com if you can’t find what you are looking for</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="how" className="row row1">
              <div className="col-xs-12">
                <h4 className="page-title how">How to use Ruuby</h4>
                <ul>
                  <li>
                    <p className="question">How does Ruuby work?</p>
                    <p className="answer">Ruuby provides at-home beauty services to all areas in central and greater London and gives you access to London’s best salons and spas.  Download the Ruuby app from the App Store (available soon on Google Play) or have a look on the website to select your appointment with the beauty therapist or salon of your choice. Book a treatment at home, in a salon or at any other location you may desire. Payment is completely taken care of with no need for cash. You can book a beauty treatment up to two weeks in advance. Any special requests? Please contact our concierge team on concierge@ruuby.com or call 0207 460 8319</p>
                  </li>
                  <li>
                    <p className="question">How can I redeem a promo code?</p>
                    <p className="answer">You can simply add your promo code on the app or website after you have selected your preferred salon or beauty therapist and treatment and the bookings price will be automatically updated</p>
                  </li>
                  <li>
                    <p className="question">My promo code doesn’t seem to work, what do I have to do now?</p>
                    <p className="answer">Please contact support@ruuby.com if your code your code doesn’t work. Please note most promo codes can only be used once</p>
                  </li>
                  <li>
                    <p className="question">My app is not working, what can I do?</p>
                    <p className="answer">Although we aim for our app to be working correctly at all times, please do contact support@ruuby.com if you are facing any difficulties. Our team of tech experts are always on hand to help</p>
                  </li>
                  <li>
                    <p className="question">I would like a beauty service such as a special makeup look that is not specified on your app or website, what can I do?</p>
                    <p className="answer">We are a premium beauty concierge and can provide any beauty service you would like. Please get in touch with concierge@ruuby.com and we will find something that suits your needs within the hour</p>
                  </li>
                  <li>
                    <p className="question">I would like to book for a group, how can I do this?</p>
                    <p className="answer">Ruuby would love to help! Please contact concierge@ruuby.com or call 0207 460 8319 and we can discuss suitable options with you</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="therapists" className="row">
              <div className="col-xs-12">
                <h4 className="page-title therapists">Beauty Therapists</h4>
                <ul>
                  <li>
                    <p className="question">How do you vet your beauty therapists and salons?</p>
                    <p className="answer">All our beauty therapists and salons are thoroughly pre-screened, vetted and tested by Ruuby to ensure only the best can join our platform. Our vetting process is extensive, with further details available on request. In addition we thoroughly review their past experience and try every service out ourselves first (lucky us)!</p>
                  </li>
                  <li>
                    <p className="question">How can I be sure that I will get only the best beauty therapists for my treatment?</p>
                    <p className="answer">We only allow the absolute best beauty therapists on our platform, which is why we have a very thorough selection process in place. Every single beauty therapist has been tried and tested by ourselves so we can be sure that we deliver only an impeccable service. In addition we offer special training courses to our beauty therapists in cooperation with certain high-end brands. If, for any reason, you are not happy with your treatment, please do get in touch with us on support@ruuby.com</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="bookings" className="row row2">
              <div className="col-xs-12">
                <h4 className="page-title bookings">Bookings</h4>
                <ul>
                  <li>
                    <p className="question">How do I make a booking?</p>
                    <p className="answer">You can make a booking on the app itself or you can check out our website and book your treatments on there. The Ruuby app is currently only available in the App store, however soon it will be on Google Play as well. Once you have selected your desired salon or beauty therapist and treatment, we will send one of our top beauty therapist to you. A booking confirmation email will also be send over with all the necessary details</p>
                  </li>
                  <li>
                    <p className="question">I have made a booking with one of your beauty therapists, what happens next?</p>
                    <p className="answer">One of our beauty therapists from the Ruuby Collective or Ruuby Elite team will arrive at your location at the scheduled time. They will bring towels, products and a bed (if required) so all you need to do is sit back and enjoy your treatment. Payment is taken securely on the app or website at the time of booking</p>
                  </li>
                  <li>
                    <p className="question">My beauty therapist hasn’t arrived yet, what do I do?</p>
                    <p className="answer">Our beauty therapists will always try to be exactly on time as we appreciate how important this is, but sometimes things outside our control can cause delays. Where we can, we will also inform you if one of our beauty therapists is coming later than expected. If you haven’t heard from us please do email support@ruuby.com or call 0207 460 8319 to let us know your beauty therapist hasn’t arrived yet and we will look into this for you immediately</p>
                  </li>
                  <li>
                    <p className="question">I am running late for my own booking, what do I need to do?</p>
                    <p className="answer">Please email support@ruuby.com or call 0207 460 8319 as soon as possible and we will inform the beauty therapist. The beauty therapist will be able to wait for 20 minutes from the booking start time</p>
                  </li>
                  <li>
                    <p className="question">How can I contact my beauty therapist?</p>
                    <p className="answer">Please do contact support@ruuby.com or call 0207 460 8319 if you have any questions or comments for your beauty therapist and we can get in touch on your behalf</p>
                  </li>
                  <li>
                    <p className="question">Can I choose my beauty therapist?</p>
                    <p className="answer">Absolutely! We are always promoting the individual and understand how important it is to get exactly the beauty therapist you want. You can therefore book a specific beauty therapist on the app or website. If your favorite beauty therapist is not available on the app or website please email concierge@ruuby.com</p>
                  </li>
                  <li>
                    <p className="question">How can I amend my booking?</p>
                    <p className="answer">Please contact support@ruuby.com if you would like to amend your original booking, add or remove certain beauty services</p>
                  </li>
                  <li>
                    <p className="question">How can I cancel a booking?</p>
                    <p className="answer">Please contact support@ruuby.com if you decide to cancel your booking. Please do note our cancellation policy described below</p>
                  </li>
                  <li>
                    <p className="question">Can I book beauty services in advance?</p>
                    <p className="answer">Yes of course! You can book beauty services up to two weeks in advance via the app or website. If you have a special request such as for events, weddings or anything else that you would like to book further in advance please contact the Ruuby Concierge on concierge@ruuby.com</p>
                  </li>
                  <li>
                    <p className="question">Is there a minimum order value?</p>
                    <p className="answer">Most beauticians have their own minimum call out, which is detailed on their profile</p>
                  </li>
                  <li>
                    <p className="question">I just noticed that my address on the booking confirmation is wrong, how can I change this?</p>
                    <p className="answer">Please contact support@ruuby.com if you would like to amend the details of a booking that has already been confirmed. For new bookings please note that you can add an address on your account page on the app</p>
                  </li>
                  <li>
                    <p className="question">Do I need to tip the beauty therapist?</p>
                    <p className="answer">Whether you tip or not is completely up to you, but of course is always appreciated! You can tip in cash after the beauty therapist has provided the treatment. Our Ruuby Collective and Elite beauty professionals will of course receive 100% of all tips</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="payments" className="row">
              <div className="col-xs-12">
                <h4 className="page-title payments">Payments / Cancellations / Refunds</h4>
                <ul>
                  <li>
                    <p className="question">What is your cancellation policy?</p>
                    <p className="answer">You can cancel free of charge within 10 minutes of making a booking, for example if you have selected an incorrect time slot by mistake.<br/>We charge 50% of the total booking price if you cancel a booking less than 12 hours but more than 2 hours in advance of the booking in order to compensate our beauty therapists. However, you won’t be charged if you reschedule your booking with the same beauty therapist more than 2 hours in advance of the booking.<br/>We charge 100% of the total booking price if a booking is cancelled within 2 hours or less of the booking as most beauty therapists will either already be on their way or will not be able anymore to receive another treatment for that timeslot</p>
                  </li>
                  <li>
                    <p className="question">How long does it take to process a refund?</p>
                    <p className="answer">Ruuby will always process a refund within 48 hours from the time of cancellation (during working hours). Depending on your bank, it will then take between 3 - 5 working days for the refund to be fully processed into your account.</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="recruitment" className="row row3">
              <div className="col-xs-12">
                <h4 className="page-title recruitment">Recruitment</h4>
                <ul>
                  <li>
                    <p className="question">I would like to work for Ruuby as a beauty therapist, how can I apply?</p>
                    <p className="answer">We are always looking for new beauty therapists! Please send your CV and motivation to partners@ruuby.com and we will get in touch as soon as possible.</p>
                  </li>
                  <li>
                    <p className="question">I would like to have my salon or spa listed on your platform, how can I get in touch?</p>
                    <p className="answer">Please do contact partners@ruuby.com and we will get in touch to further discuss potential partnership opportunities as soon as possible. Please note we allow only the best salons and spas on our platform.</p>
                  </li>
                </ul>
              </div>
            </Element>
            <Element name="other" className="row">
              <div className="col-xs-12">
                <h4 className="page-title other">Any other questions or feedback</h4>
                <ul>
                  <li>
                    <p className="question">How can I contact Ruuby?</p>
                    <p className="answer">We are always looking for new beauty therapists! Please send your CV and motivation to partners@ruuby.com and we will get in touch as soon as possible.</p>
                  </li>
                  <li>
                    <p className="question">I would like to have my salon or spa listed on your platform, how can I get in touch?</p>
                    <p className="answer">For any questions about your booking, feedback or anything else, please contact support@ruuby.com<br/>For any instant concierge (booking) questions, please contact concierge@ruuby.com<br/>For recruitment please contact partners@ruuby.com<br/>Prefer to call? You can reach us on 0207 460 8319 from 9.00 - 22.00 Monday - Friday.</p>
                  </li>
                </ul>
              </div>
            </Element>
          </div>
        </div>
      </div>
    );
  }
}
