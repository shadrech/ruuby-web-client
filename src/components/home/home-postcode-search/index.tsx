import * as React from "react";

import "./styles/index.scss";

interface Props {
  handleAddressSearch: (postcode: string) => void;
  hasPostcodeError: boolean;
}

interface State {
  enteredPostcode?: string;
}

export class HomePostcodeSearch extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      enteredPostcode: "",
    };
  }

  handleInputKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === "Enter")
      this.props.handleAddressSearch(this.state.enteredPostcode);
  }

  handleChangePostcode = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({enteredPostcode: event.target.value.toUpperCase()});
  }

  render() {
    const error = this.props.hasPostcodeError ? (
      <div className="postcode-error">
        Please enter a valid full London postcode
      </div>
    ) : null;

    return (
      <div className="home-search-input">
        <div className="input-wrapper">
          <div id="PlacesAutocomplete__root">
            <input
              type="text"
              value={this.state.enteredPostcode}
              onChange={this.handleChangePostcode}
              placeholder="Enter your London postcode"
              onKeyDown={this.handleInputKeyDown}
            />
          </div>
          <div className="input-submit" onClick={() => this.props.handleAddressSearch(this.state.enteredPostcode)}>
            <p>Book Now</p>
          </div>
        </div>
        {error}
      </div>
    );
  }
}
