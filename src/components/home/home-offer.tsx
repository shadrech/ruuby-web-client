import * as React from "react";

export default class HomeOffer extends React.Component<{}, {}> {
  render() {
    return (
      <div className="the-offer-section" id="theOffer">
        <div className="row offer-header-row">
          <div className="col align-self-center">
            <p className="ruuby-header">How It Works</p>
          </div>
        </div>
        <div className="row offer-bottom-section">
          <div className="col-md-4">
            <img src={require("../../assets/imgs/offer1@2x.png")} alt="Hairdresser finishing blowdry" />
            <p className="order">1</p>
            <p>Browse our selection of vetted, professional beauty therapists, hairdressers and makeup artists</p>
          </div>
          <div className="col-md-4">
            <img src={require("../../assets/imgs/offer2@2x.png")} alt="Nail therapist in the middle of a manicure session" />
            <p className="order">2</p>
            <p>Choose a date and time to book. Appointments are available from 6am to 11pm</p>
          </div>
          <div className="col-md-4">
            <img src={require("../../assets/imgs/offer3@2x.png")} alt="Ruuby chairs under table with makeup equipment" />
            <p className="order">3</p>
            <p>You're booked! Simply wait for your therapist, hair stylist or makeup artist to arrive at the scheduled time and enjoy your treatment</p>
          </div>
        </div>
      </div>
    );
  }
}
