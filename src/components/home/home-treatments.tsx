import * as React from "react";
import { Arrow } from "../sub/svgs";
import * as utils from "../../utils";
import { browserHistory } from "react-router";

interface Props {
  setSelectedCategory: (cat: string) => void;
}

export default class HomeTreatments extends React.Component<Props, {}> {
  private domObjects: any = {};

  handleTreatmentCarouselNext = (event: any): void => {
    event.preventDefault();
    const length = (this.domObjects.beltWrapper as any).clientWidth - 30;
    this.handleTreatmentCarousel(this.domObjects.beltWrapper, length);
  }

  handleTreatmentCarouselPrevious = (event: any): void => {
    event.preventDefault();
    const length = (this.domObjects.beltWrapper as any).clientWidth - 30;
    this.handleTreatmentCarousel(this.domObjects.beltWrapper, -length);
  }

  handleTreatmentCarousel = (selector, offset): void => {
    $(selector).animate({
          scrollLeft: $(selector).scrollLeft() + offset
        }, 300
    );
  }

  navigateToResults = (category: string): void => {
    this.props.setSelectedCategory(category);

    browserHistory.push(utils.constructSearchUrl(category));
  }

  render() {
    return (
      <div className="container-fluid treatments-section">
        <div className="row treatment-top">
          <div className="treatments-wrapper">
            <div className="left-arrow" onClick={this.handleTreatmentCarouselPrevious}><Arrow /></div>
            <div className="belt-gradient-left"></div>
            <div className="treatments-belt-wrapper" ref={div => this.domObjects.beltWrapper = div}>
              <div className="treatments-belt">
                <div onClick={() => this.navigateToResults("Nails")} className="treatment">
                  <div className="treatment-name">Nails</div>
                </div>
                <div onClick={() => this.navigateToResults("Hair")} className="treatment">
                  <div className="treatment-name">Hair</div>
                </div>
                <div onClick={() => this.navigateToResults("Waxing")} className="treatment">
                  <div className="treatment-name">Waxing</div>
                </div>
                <div onClick={() => this.navigateToResults("Eyes")} className="treatment">
                  <div className="treatment-name">Eyes</div>
                </div>
                <div onClick={() => this.navigateToResults("Tanning")} className="treatment">
                  <div className="treatment-name">Tanning</div>
                </div>
                <div onClick={() => this.navigateToResults("Makeup")} className="treatment">
                  <div className="treatment-name">Makeup</div>
                </div>
                <div onClick={() => this.navigateToResults("Wellness")} className="treatment">
                  <div className="treatment-name">Wellness</div>
                </div>
                <div onClick={() => this.navigateToResults("Massage")} className="treatment">
                  <div className="treatment-name">Massage</div>
                </div>
                <div onClick={() => this.navigateToResults("Facial")} className="treatment">
                  <div className="treatment-name">Facial</div>
                </div>
                <div onClick={() => this.navigateToResults("Cosmetic Injectables")} className="treatment">
                  <div className="treatment-name">Cosmetic Injectables</div>
                </div>
                <div onClick={() => this.navigateToResults("Ruuby Men")} className="treatment">
                  <div className="treatment-name">Ruuby Men</div>
                </div>
              </div>
            </div>
            <div className="belt-gradient-right"></div>
            <div className="right-arrow" onClick={this.handleTreatmentCarouselNext}><Arrow /></div>
          </div>
        </div>
      </div>
    );
  }
}
