import * as React from "react";

interface HomeSocialProps {
  posts: any;
  fetchInstaData: () => void;
}

export default class HomeSocial extends React.Component<HomeSocialProps, {}> {
  componentDidMount() {
    if (!this.props.posts.data)
      this.props.fetchInstaData();
  }

  render() {
    const posts = (this.props.posts.data) ? this.props.posts.data.map((post: any, i: number) => {
      const { height, width } = post.images.low_resolution;
      const styles = (height < width) ? {
        height: "100%",
        width: "auto"
      } : {
        width: "100%",
        height: "auto"
      };

      return (
        <div key={i} className="col-md-2 col-xs-6">
          <a href={post.link} target="_blank">
            <img src={post.images.low_resolution.url} style={styles} alt={post.caption.text} />
          </a>
        </div>
      );
    }) : "";

    return (
      <div className="social-section">
        <div className="row">
          <div className="col align-self-center">
            <div className="ruuby-header">@ruubyapp</div>
          </div>
        </div>
        <div className="row posts">{posts}</div>
        <div className="row">
          <div className="col align-self-center">
            <a href="https://www.instagram.com/ruubyapp/" className="button-link">
              <div className="ruuby-btn">
                <p>Follow Ruuby</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
