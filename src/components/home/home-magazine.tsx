import * as React from "react";
import * as moment from "moment";

import {Post} from "../../api/magazine";
import LineLoader from "../../components/sub/line-loader";

interface Props {
  posts: Post[];
  isBusy: boolean;
}

export const HomeMagazine: React.SFC<Props> = props => {
  const clip = screen.width > 992 ? 200 : 100;

  const magazines = props.posts.slice(0, 3).map((post, i) => (
    <div key={i} className="col-md-4">
      <a href={post.link} className="home-magazine">
        <div className="mag-img">
          <img src={post.image} alt={post.title} />
        </div>
        <p className="mag-published">{moment(post.date).format("DD.MM.Y")}</p>
        <p className="mag-title" dangerouslySetInnerHTML={{__html: post.title}} />
        <p className="mag-text" dangerouslySetInnerHTML={{__html: post.excerpt.slice(0, clip) + " ...."}} />
      </a>
    </div>
  ));

  return (
    <div className="magazine-section">
      <div className="row">
        <div className="col align-self-center">
          <h1 className="ruuby-header">Ruuby Magazine</h1>
        </div>
      </div>
      <div className="row">
        <LineLoader isBusy={props.isBusy} />
        {magazines}
      </div>
      <div className="row">
        <div className="col align-self-center">
          <a href="/magazine/" className="button-link">
            <div className="ruuby-btn">
              <p>Explore Magazine</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};
