import * as React from "react";
import { DownloadFromApple, DownloadFromGoogle } from "../sub/svgs";
import {recordAppStoreLink, recordAppStoreAndroidLink} from "../../utils";

export default class HomeAppStore extends React.Component<{}, {}> {
    render() {
        return (
            <section className="home-app-stores">
                <div className="home-search-new">
                    <div className="home-download-icons">
                        <a href="https://itunes.apple.com/gb/app/ruuby/id951092698?mt=8" onClick={recordAppStoreLink} target="_blank"><DownloadFromApple /></a>
                        <a href="https://play.google.com/store/apps/details?id=com.ruubycustomerapp" onClick={recordAppStoreAndroidLink} target="_blank"><DownloadFromGoogle /></a>
                    </div>
                </div>
            </section>
        );
    }
}
