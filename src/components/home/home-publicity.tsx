import * as React from "react";
import { ForbesLogo, GraziaLogo, NewYorkTimesLogo, WiredLogo, EveningStandardLogo, StyleLogo } from "../sub/svgs";

interface HomePublicityState {
  active?: number;
  quotes?: {
    text: string;
    company: string;
  }[];
  available?: number[];
}

export default class HomePublicity extends React.Component<{}, HomePublicityState> {
  timer: any;

  constructor() {
    super();

  const quotes = [
    {
      text: `London’s digital beauty concierge.... the “Uber for Beauty”`,
      company: "Forbes"
    },
    {
      text: null,
      company: "Wired"
    },
    {
      text: "Ruuby app takes up the much needed mantle of beauty concierge in Central London",
      company: "Grazia"
    },
    {
      text: null,
      company: "The New York Times"
    },
    {
      text: "Best London lifestyle app",
      company: "London Evening Standard"
    },
    {
      text: null,
      company: "The Sunday Times Style"
    }
  ];

  const available = quotes.reduce((object, current, index) => {
    if (current.text !== null) object.push(index);
    return object;
  }, []);

    this.state = {
      active: 0,
      quotes,
      available,
    };
  }

  componentDidMount() {
    this.updateText();
  }

  componentWillUnmount() {
    window.clearTimeout(this.timer);
  }

  updateText() {
    this.timer = setTimeout((() => {
      let active = this.state.active + 1;
      if (active === this.state.available.length) active = 0;
      this.setState({active});
      this.updateText();
    }), 5000);
  }

  render() {
    const active = this.state.available[this.state.active];

    return (
      <div className="row publicity-section">
        <div className="col-md-6">
          <div className="row logos">
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 0) ? "active" : ""}`}><ForbesLogo /></div>
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 1) ? "active" : ""}`}><WiredLogo /></div>
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 2) ? "active" : ""}`}><GraziaLogo /></div>
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 3) ? "active" : ""}`}><NewYorkTimesLogo /></div>
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 4) ? "active" : ""}`}><EveningStandardLogo /></div>
            <div className={`col-md-4 col-sm-6 col-xs-6 ${(active === 5) ? "active" : ""}`}><StyleLogo /></div>
          </div>
        </div>
        <div className="col-md-6 statements">
          <p className="text">"{this.state.quotes[active].text}"</p>
          <p className="company">- {this.state.quotes[active].company}</p>
        </div>
      </div>
    );
  }
}
