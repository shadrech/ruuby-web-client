import * as React from "react";
import { Link, browserHistory } from "react-router";
import * as queryString from "querystring";
const TimelineMax = require("gsap/src/uncompressed/TimelineMax");
import * as Cookie from "js-cookie";
import * as moment from "moment";
import { Phone, RuubyLogoExpanded } from "../../sub/svgs";
import {constructUrl} from "../../../routes";
import {validatePostcode} from "../../../api";
import {HomePostcodeSearch} from "../home-postcode-search";
import {ApiCustomer} from "../../../api/customer";
import {ApiCategory} from "../../../api/categories";
import {Arrow} from "../../sub/svgs";
import {constructSearchUrl} from "../../../utils";
import {SearchResultsQueryParams} from "../../search/search-container";
import { slotTypes } from "../../../reducers/booking/reducer";

interface Props {
  categories: ApiCategory[];
  location: {
    category: string;
    date: string;
    slot: slotTypes;
    postcode: string;
  };

  setSelectedCategory: (category: string) => void;
}
interface State {
  hasPostcodeError: boolean;
}

interface Props {
  customer: ApiCustomer;
}

export class HomeBg extends React.Component<Props, State> {
  private logo: React.Component;
  private domObjs: {
    [name: string]: HTMLDivElement
  } = {};
  state: State = {
    hasPostcodeError: false,
  };

  componentDidMount() {
    let tl = new TimelineMax();
    const speed = 0.5;
    tl.delay(0.5)
      .fromTo(this.logo.refs.middleTxt, speed, {opacity: 0},  {opacity: 1})
      .fromTo(this.logo.refs.topTxt, speed, {opacity: 0, y: 20}, {opacity: 1, y: 5})
      .fromTo(this.logo.refs.bttmTxt, speed, {opacity: 0, y: -20}, {opacity: 1, y: -5}, "-=0.75")
      .fromTo(this.domObjs.about, speed, {opacity: 0, x: 20}, {opacity: 1, x: 0}, "-=1.25")
      .fromTo(this.domObjs.gifting, speed, {opacity: 0, x: 20}, {opacity: 1, x: 0}, "-=0.75")
      .fromTo(this.domObjs.mag, speed, {opacity: 0, x: -20}, {opacity: 1, x: 0}, "-=0.75")
      .fromTo(this.domObjs.price, speed, {opacity: 0, x: -20}, {opacity: 1, x: 0}, "-=1.25")
      .fromTo(this.domObjs.join, speed, {opacity: 0, x: -20}, {opacity: 1, x: 0}, "-=0.25")
      .fromTo(this.domObjs.book, speed, {opacity: 0, x: 20}, {opacity: 1, x: 0}, "-=0.75")
      .fromTo(this.domObjs.user, speed, {opacity: 0, x: -10}, {opacity: 1, x: 0}, "-=0");
  }

  setNewUrl = (category: string) => {
    this.props.setSelectedCategory(category);

    const params: SearchResultsQueryParams = {
      category: category.toString(),
      slot: "All-Day",
      date: this.props.location.date || moment().format("YYYY-MM-DD")
    };

    let url = `${constructUrl("search")}/${params.category}/${params.date}/${params.slot}`;
    if (this.props.location.postcode)
      url += `/${this.props.location.postcode}`;

    browserHistory.push(url);
  }

  handleAddressSearch = async (postcode: string) => {
    const valid = await validatePostcode(postcode);
    if (valid) {
      this.setState({hasPostcodeError: false});

      browserHistory.push(`${constructUrl("category")}?${queryString.stringify({postcode})}`);
    } else {
      this.setState({hasPostcodeError: true});
    }
  }

  isLoggedIn = (): boolean => {
    return (this.props.customer !== null) || (Cookie.get("token") !== undefined);
  }

  render() {
    return (
      <div>
        <section className="hp-section-one section-one-bg hp-section-one-new">
          <div className="home-navbar">
            <nav className="navbar-home black">
              <div className="navbar-container">
                <div ref={dom => this.domObjs.book = dom} className="nav-item lbb">
                  <Link to={constructSearchUrl("All")} className="lbb-a">Little Black Book <Arrow /></Link>
                  <div className="sub-container">
                    {this.props.categories && this.props.categories.map((cat, i: number) => <div key={cat.code} className={`cat${i} cat-link`} onClick={() => this.setNewUrl(cat.name)}>{cat.name}</div>)}
                  </div>
                </div>
                <div ref={dom => this.domObjs.gifting = dom} className="nav-item">
                  <a href="https://shop.ruuby.com/">Gifting</a>
                </div>
                <div ref={dom => this.domObjs.about = dom} className="nav-item">
                  <Link to="/about">About</Link>
                </div>
                <Link className="ruuby-logo-link nav-item" to="/">
                  <RuubyLogoExpanded ref={logo => this.logo = logo} />
                </Link>
                <div ref={elem => this.domObjs.price = elem} className="nav-item">
                  <Link to="/price-list">Price List</Link>
                </div>
                <div ref={elem => this.domObjs.mag = elem} className="nav-item">
                  <a href="/magazine">Magazine</a>
                </div>
                <div ref={elem => this.domObjs.join = elem} className="nav-item">
                  <Link to="/join">Join the Team</Link>
                </div>
                {this.isLoggedIn() ?
                  <div ref={elem => this.domObjs.user = elem} className="nav-item nav-item-user">
                    <Link to="/profile">My account</Link>
                  </div>
                : <div ref={elem => this.domObjs.user = elem} className="nav-item nav-item-user">
                    <Link to="/login">Login</Link>
                  </div>
                }
              </div>
            </nav>
          </div>
          <div className="home-search-new">
            <div className="home-search-text">
              <h1 className="ruuby-header">London's Digital Beauty Concierge.</h1>
              <h2 className="paragraph">Book five star beauty services to your door in ninety minutes</h2>
            </div>
            <HomePostcodeSearch handleAddressSearch={this.handleAddressSearch.bind(this)} hasPostcodeError={this.state.hasPostcodeError} />
            <p className="home-phone">
              <Phone />
              <a href="tel:+4402074608319">0207 460 8319</a>
            </p>
          </div>
        </section>
      </div>
    );
  }
}
