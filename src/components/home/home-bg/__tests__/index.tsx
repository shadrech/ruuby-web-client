import * as React from "react";
import * as ReactDOM from "react-dom";
import * as TestUtils from "react-addons-test-utils";
import {ApiCustomer} from "../../../../api/customer";

import {HomeBg} from "../";

declare const expect: any;

it("HomeBg rendered output", () => {
  // Render a checkbox with label in the document
  const home = TestUtils.renderIntoDocument(
    <HomeBg customer={{} as ApiCustomer} categories={[]} location={{} as any} setSelectedCategory={() => {}} />
  );

  const homeNode = ReactDOM.findDOMNode(home as React.ReactInstance);

  // Verify that it"s Off by default
  expect(homeNode.textContent).toEqual("Little Black Book GiftingAboutPrice ListMagazineJoin the TeamMy accountLondon's Digital Beauty Concierge.Book five star beauty services to your door in ninety minutesBook Now0207 460 8319");
});
