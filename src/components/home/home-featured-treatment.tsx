import * as React from "react";

export default class HomeFeaturedTreatments extends React.Component<{}, {}> {
  render() {
    return (
      <section className="treatments-section">
        <div className="row treatment-bottom">
          <div className="col-md-6">
            <a href="https://shop.ruuby.com/">
            <img className="featured-img" src={require("../../assets/imgs/voucher.jpg")} alt="Ruuby Vouchers" />
            </a>
          </div>
          <div className="col-md-6">
            <div className="featured-txt">
            <h1 className="ruuby-title">Ruuby Gifting</h1>
            <h2 className="ruuby-txt">
            Buy your loved one Ruuby Vouchers to use towards luxury beauty services at their home. We offer both e-vouchers and card vouchers. Vouchers can be redeemed at any time against all Ruuby treatments. The perfect gift!
            </h2>
            <a href="https://shop.ruuby.com/" className="button-link">
              <div className="ruuby-btn">
                <p>Buy Now</p>
              </div>
            </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
