import * as React from "react";
import { Provider } from "react-redux";
import {Router} from "react-router";
import * as ReactGA from "react-ga";
const Favicon = require("react-favicon");

import {routes} from "./routes";
import {config} from "./config";

ReactGA.initialize(config.gaId);
(ReactGA as any).plugin.require("ecommerce");

function logPageView() {
  const page = window.location.pathname;

  ReactGA.set({ page });
  ReactGA.pageview(page);
}

interface Props {
  store: any;
  history: any;
}

export class Root extends React.Component<Props, {}> {
  render() {
    const { store, history } = this.props;

    return (
      <Provider store={store}>
        <div>
          <Favicon url={require("./css/img/favicon.png")} />
          <Router history={history} routes={routes} onUpdate={logPageView} />
        </div>
      </Provider>
    );
  }
}
