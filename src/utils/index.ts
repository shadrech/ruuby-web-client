import * as ReactGA from "react-ga";
import * as moment from "moment";

import { constructUrl } from "../routes";
import {Provider} from "../api";
import {ApiProvider} from "../api/provider";
import { slotTypes, SLOT_MORNING, SLOT_AFTERNOON, SLOT_EVENING } from "../reducers/booking/reducer";

export const constants = {
  ERROR_TOKEN: "ERROR_TOKEN"
};

const ERROR_KEY_TO_MESSAGE_MAP = {
  RUUBY_ERROR_BOOKING_NO_STAFF_AVAILABLE: "There is no one available to perform this treatment. Check out our other listings...",
  RUUBY_ERROR_BOOKING_SALON_CLOSED: "Oh no! The salon is not open at this time. Have a look at our other listings...",
  RUUBY_ERROR_BOOKING_SLOT_TAKEN: "Oops! This time slot is no longer available...",
  RUUBY_ERROR_BOOKING_CARD_DECLINED: "Oops! Payment couldn't be processed - please check your details and try again"
};

export function convertErrorKeyToMessage(key: string) {
  return ERROR_KEY_TO_MESSAGE_MAP[key] || "Oops, something went wrong. Please try again!";
}

export function findProvider(providers: Provider[], id: string) {
  const parts = parseProviderUrn(id);

  if (parts.length === 2) {
    return providers.filter(provider => (provider.id === parts[0]) && (provider.workstationId === parts[1]))[0];
  }
  else {
    return providers.filter(provider => provider.id === parts[0])[0];
  }
}

export function findApiProvider(providers: ApiProvider[], urn: string) {
  if (providers.length === 0 || !providers) return undefined;

  const parts = parseProviderUrn(urn);

  if (parts.length === 2) {
    return providers.filter(provider => {
      const providerParts = parseProviderUrn(provider.therapist.urn);
      return (providerParts[0] === parts[0]) && (providerParts[1] === parts[1]);
    })[0];
  } else {
    return providers.filter(provider => parseProviderUrn(provider.therapist.urn)[0] === parts[0])[0];
  }
}

export function parseProviderUrn(urn: string): number[] {
  const getUrnId = (part: string) => {
    const p = part.split(":");
    return p[p.length - 1];
  };

  if (urn.indexOf("-") !== -1) {
    const parts = urn.split("-");
    const id = getUrnId(parts[0]);
    return [Number.parseInt(id), Number.parseInt(parts[1])];
  } else {
    const id = getUrnId(urn);
    return [Number.parseInt(id)];
  }
}

export function isAtHome(urn: string): boolean {
  return parseProviderUrn(urn).length === 2;
}

export function recordAppStoreLink() {
  recordEvent(["Outbound Link", "App Store", "iOS"]);
}

export function recordAppStoreAndroidLink() {
  recordEvent(["Outbound Link", "App Store", "android"]);
}

export function recordEvent(event: string[]) {
  ReactGA.event({
    category: event[0],
    action: event[1],
    label: event[2]
  });
}

export function changeToCamelCase(str: string) {
  if (str)
    return str[0].toUpperCase() + str.slice(1).toLowerCase();
  else
    return "";
}

export function getSlot(now: moment.Moment = moment()): slotTypes {
  return (() => {
    switch (now.hours()) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        return SLOT_MORNING;
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
        return SLOT_AFTERNOON;
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
        return SLOT_EVENING;
    }
  })();
}

export function constructSearchUrl(category: string = ""): string {
  const now = moment();

  return `${constructUrl("search")}/${category}/${now.format("YYYY-MM-DD")}/All-Day`;
}
