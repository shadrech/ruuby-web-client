let _title: HTMLTitleElement;
let _description: HTMLMetaElement;
import data from "./data";

export function initialise() {
  _title = document.getElementsByTagName("title")[0];
  _description = getDescriptionTag();
}

export function setTitle(text: string): void {
  if (!_title) initialise();
  _title.innerHTML = text;
}

export function setDescription(text: string): void {
  if (!_description) initialise();
  _description.content = text;
}

export function setTitleByUrl(url: string): void {
  const params = url.replace("%20", " ").split("/");

  setTitle(data[params[1]][params[2]].title);
}

export function setMetadata(page: "home"|"category"|"about"|"login"|"register"|"join"|"priceList"|"checkout"|"privacyPolicy"|"faqs"): void {
  setTitle(data[page].title);
  setDescription(data[page].description);
}

function getDescriptionTag(): HTMLMetaElement {
  const tags = document.getElementsByTagName("meta");
  for (let i = 0; i < tags.length; i++)
    if (tags[i].name && tags[i].name === "Description")
      return tags[i];
}
