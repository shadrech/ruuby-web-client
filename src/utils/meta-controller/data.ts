type CategoryTypes = "All" | "Nails" | "Hair" | "Massage" | "Waxing" | "Makeup" | "Eyes" | "Tanning" | "Facial" | "Wellness" | "Cosmetic Injectables" | "Ruuby Men";
interface Metadata {
  title: string;
  description: string;
}

const data: {
  home: Metadata;
  category: Metadata;
  about: Metadata;
  join: Metadata;
  login: Metadata;
  register: Metadata;
  priceList: Metadata;
  faqs: Metadata;
  privacyPolicy: Metadata;
  search: {
    [category in CategoryTypes]: Metadata;
  }
} = require("./data.json");

export default data;
