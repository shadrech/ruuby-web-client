import { combineReducers } from "redux";
import {reducer as formReducer} from "redux-form";

import magazineReducer, {State as magazineState} from "./reducers/magazine/reducer";
import instaReducer, {State as instaState} from "./reducers/instagram/reducer";
import searchReducer, {State as searchState} from "./reducers/search/reducer";
import userReducer, {State as userState} from "./reducers/customer/reducer";
import providerReducer, {State as providerState} from "./reducers/providers/reducer";
import bookingReducer, {State as bookingState} from "./reducers/booking/reducer";
import basketReducer, {State as basketState} from "./reducers/basket/reducer";
import paymentsReducer, {State as paymentState} from "./reducers/payments/reducer";
import addressesReducer, {State as addressState} from "./reducers/addresses/reducer";
import categoriesReducer, {State as categoriesState} from "./reducers/categories/reducer";
import {CREATE_ADDRESS_SUCCESS} from "./reducers/addresses/types";
import {CUSTOMER_ERROR} from "./reducers/customer/types";

export interface RootReducer {
  form: any;
  magazineState: magazineState;
  instagramState: instaState;
  searchState: searchState;
  customerState: userState;
  providerState: providerState;
  bookingState: bookingState;
  basketState: basketState;
  paymentState: paymentState;
  addressesState: addressState;
  categoriesState: categoriesState;
}

export const appReducer = combineReducers<RootReducer>({
  form: (formReducer as any).plugin({
    address: (state: any, action: any): any => { // this is used to clear the form data after saving an address
      switch (action.type) {
        case CREATE_ADDRESS_SUCCESS:
          return {
            ...state,
            values: {
              ...state.values,
              address1: "",
              address2: ""
            },
            fields: {
              ...state.fields,
              address: "",
              address2: ""
            }
          };

        default:
          return state;
      }
    },
    register: (state: any, action: any) => {
      if (state && state.values) {
        switch (action.type) {
          case CUSTOMER_ERROR:
            return {
              ...state,
              values: {
                 ...state.values,
                passwordConfirm: state.values.password
              }
            };

          default:
            return state;
        }
      }
    }
  }),
  magazineState: magazineReducer,
  instagramState: instaReducer,
  searchState: searchReducer,
  customerState: userReducer,
  providerState: providerReducer,
  bookingState: bookingReducer,
  basketState: basketReducer,
  paymentState: paymentsReducer,
  addressesState: addressesReducer,
  categoriesState: categoriesReducer
});
