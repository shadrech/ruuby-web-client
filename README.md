# Ruuby Web App

Web App which is the customer facing website

## Stack

* React
* React-Router
* Redux, Redux-Thunk
* Typescript
* Webpack
* ...

## Setup

To get up and running, run following commands. You need to make sure you have [yarn](https://yarnpkg.com/lang/en/docs/install/) globally installed

```bash
  yarn
  cp ./config.original.json config.json
```
Edit the contents of config.json to required settings
Now run the webpack-dev-server to serve your app on http://localhost:3000
```bash
  yarn dev
```
