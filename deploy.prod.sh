#!/bin/bash

set -e

export AWS_ACCESS_KEY_ID=${PROD_AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${PROD_AWS_SECRET_ACCESS_KEY}
DISTRIBUTION_ID=E19D5PZVRDU74X
BUCKET=ruuby-website-production
CONFIG_BUCKET=ruuby-configuration
TAG=$(git tag -l --contains HEAD)
CACHE_AGE=3600
PACKAGE_NAME=$(node -e 'const f = fs.readFileSync("package.json"); const j = JSON.parse(f); console.log(j.name)')
SEMAPHORE_PROJECT_ID="67ec5f95-04fd-467d-9c39-9a79da498eee"
SEMAPHORE_MASTER_BRANCH_ID=1339036
SEMAPHORE_STAGING_DEPLOY_ID=32221

rm -rf node_modules
yarn

# check if staging job or production and tagged
if [ $TAG ] && [ "${BRANCH_NAME}" == "master" ]; then
  # check that build was already deployed to staging
  wget -q https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
  chmod a+x jq-linux64

  PASSED_STAGING_DEPLOYS=$(
    curl -s \
    "https://semaphoreci.com/api/v1/projects/${SEMAPHORE_PROJECT_ID}/servers/${SEMAPHORE_STAGING_DEPLOY_ID}?auth_token=${SEMAPHORE_API_AUTH_TOKEN}" | \
    ./jq-linux64 -r --arg url "https://semaphoreci.com/api/v1/projects/${SEMAPHORE_PROJECT_ID}/${SEMAPHORE_MASTER_BRANCH_ID}/builds/${SEMAPHORE_BUILD_NUMBER}" \
    '[ .deploys[] | select(.build_url | startswith($url)) | select (.result == "passed")] | length'
  )

  if [ "${PASSED_STAGING_DEPLOYS}" -eq "0" ]; then
    echo "You must deploy this build to staging first"
    exit 1
  fi

  aws s3 cp s3://${CONFIG_BUCKET}/ruuby-website/config.json ./

  yarn build

  # upload
  aws s3 sync build/ s3://${BUCKET}/

  # set cache control header on index.html
  aws s3api copy-object --bucket ${BUCKET} \
    --cache-control "max-age=${CACHE_AGE}" \
    --content-type "text/html" \
    --copy-source ${BUCKET}/index.html --key index.html \
    --metadata-directive "REPLACE"

  # create cache invalidation
  aws configure set preview.cloudfront true

  aws cloudfront create-invalidation --distribution-id ${DISTRIBUTION_ID} \
    --paths / /index.html
else
  echo "You can only deploy a tagged commit from master"
  exit 1
fi
